gemeinsam.jetzt
===============

[![build status](https://gitlab.com/sgj/platform/badges/master/build.svg)](https://gitlab.com/sgj/platform/commits/master)
[![coverage](https://gitlab.com/sgj/platform/badges/master/coverage.svg)](https://gitlab.com/sgj/platform/commits/master)


## Installation

This is the basic installation procedure on a Debian buster
system. Of course there are lot of other possibilities. The platform
uses Django 1.11 based on python3!


In order to run the application you have to install an appropriate
database-server by yourself. Please refer to the [GeoDjango
documentation](https://docs.djangoproject.com/en/1.11/ref/contrib/gis/)
how to set-up a geo-database. Below we will outline the installation
process for a PostgreSQL server with the PostGIS extension. This will
be ok, as long as you have sufficient resources on your host.

Deployment must also be done by yourself though there is some
nginx/supervisor config-example.

### System requirements

First install some tools if you did not do that already

```shell
sudo apt-get install git python3-dev virtualenv curl libgdal-dev software-properties-common
```

Node packages are managed by `django_yarnpkg` thus you need to install
`yarnpkg`

```shell
sudo apt-get install yarnpkg
```



### Clone and install required python-packages
```shell
git clone git@gitlab.com:sgj/platform.git
cd platform
virtualenv -p /usr/bin/python3 env
. env/bin/activate
pip install -r requirements.txt
```

### Installation of PostgreSQL/PostGIS

This is a basic PostgreSQL/PostGIS setup. Please use it just as an example.

```shell
sudo apt install postgis
```

```shell
sudo su - postgres
createuser --pwprompt <db_username>
createdb --owner <db_username> --encoding utf-8 <db_name>
```

Create the PostGIS extension in your new database as the PostgreSQL superuser.
```shell
echo "CREATE EXTENSION postgis;" | psql <db_name>
```

### Configure
Enter the django-apps directory
```shell
cd gemeinsam
```

copy the environment template
```shell
cp .env.tmpl .env
```

and edit that file to adapt your installation

```shell
vi .env
```
Please refer to the comments in `.env`.

Now create the database-sheme by

```shell
./manage.py migrate
```

>>>
If you want to use the same (sub-)topics as on
[gemeinsam.jetzt](https://gemeinsam.jetzt) you can run the
following script from the base directory. This will populate the
database as well as the media directory with the respective icons.

```shell
./topic/load_initial_data.py
```
>>>

Now install all javascript/css dependencies by
```shell
./manage.py yarn install
```

Create a superuser account in order to manage the backend
```shell
./manage.py createsuperuser
```

Now you can start a test-server provided you have set `DEBUG = True` in
your local settings
```shell
./manage.py runserver
```

and point your browser to `http://localhost:8000`.

If your browser not running on the server (most probably) then start the
development server by `./manage.py runserver 0.0.0.0:8000` and in your broswer
use the server's ip address.



### Before you go on-line
Now you should log in with your newly created account and go to the admin
interface (click on your username in the upper right corner and choose
'Admin').

First of all you will want to change your site-name: go to `Start`, `Websites`
and edit the `example.com` standard site.

Next you should add 2 flat-pages, namely `about-us` and `participate`
because these are link targets of the fixed main menu (at the moment). You
have to use the urls `/about-us/` and `/participate/` everything else is
according to your preferences.

Furthermore you can adapt the appearance of your new site. See `Start`,
`Appearance`.
