{% load i18n %}
{% trans "Abmeldung vom gemeinsam.jetzt Newsletter" %}

{% blocktrans %}
Du hast dich erfolgreich vom gemeinsam.jetzt Newsletter abgemeldet.
Wenn du dich wieder anmelden willst, klicke bitte folgenden link:
{% endblocktrans %}

{{ resubscribe_url }}

{% trans "Dein sgj-Team" %}
