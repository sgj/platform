# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0011_auto_20160302_1959'),
    ]

    operations = [
        migrations.AlterField(
            model_name='occurrence',
            name='published',
            field=models.BooleanField(verbose_name='Ver\xf6ffentlicht'),
        ),
    ]
