# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0007_auto_20160302_1847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='occurrence',
            name='creator',
            field=models.ForeignKey(related_name='oc_creator', on_delete=models.CASCADE, verbose_name='Anglegt von', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='description',
            field=models.TextField(null=True, verbose_name='Beschreibung', blank=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='initiative',
            field=models.ForeignKey(related_name='oc_initiative', on_delete=models.CASCADE, verbose_name='Initiative', to='initiative.Initiative', null=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='location',
            field=models.ForeignKey(related_name='oc_location', on_delete=models.PROTECT, verbose_name='Veranstaltungsort', to='event.Location', null=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='published',
            field=models.BooleanField(default=False, verbose_name='Ver\xf6ffentlicht'),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='title',
            field=models.CharField(max_length=100, null=True, verbose_name='Titel', blank=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='topics',
            field=models.ManyToManyField(related_name='oc_topic', null=True, verbose_name='Themen', to='topic.SubTopic'),
        ),
    ]
