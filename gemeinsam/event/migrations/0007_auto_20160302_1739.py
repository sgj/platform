# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0006_auto_20160301_2002'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ('initiative', 'title'), 'verbose_name': 'Veranstaltungsreihe', 'verbose_name_plural': 'Veranstaltungsreihen'},
        ),
        migrations.AlterModelOptions(
            name='occurrence',
            options={'ordering': ('start_time',), 'verbose_name': 'Veranstaltung', 'verbose_name_plural': 'Veranstaltungen'},
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='event',
            field=models.ForeignKey(verbose_name='Veranstaltungsreihe', on_delete=models.CASCADE, to='event.Event'),
        ),
    ]
