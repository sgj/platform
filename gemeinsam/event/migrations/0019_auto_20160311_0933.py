# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0018_auto_20160307_0902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='initiative',
            field=models.ForeignKey(related_name='events', on_delete=models.CASCADE, verbose_name='Initiative', to='initiative.Initiative'),
        ),
    ]
