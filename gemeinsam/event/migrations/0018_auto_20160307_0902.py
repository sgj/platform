# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0017_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ('initiative', 'title'), 'verbose_name': 'Veranstaltung', 'verbose_name_plural': 'Veranstaltungen'},
        ),
        migrations.AlterModelOptions(
            name='occurrence',
            options={'ordering': ('start_time',), 'verbose_name': 'Termin', 'verbose_name_plural': 'Termine'},
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='event',
            field=models.ForeignKey(verbose_name='Veranstaltung', on_delete=models.CASCADE, to='event.Event'),
        ),
    ]
