# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('topic', '0004_auto_20151230_1100'),
        ('event', '0002_auto_20160229_2222'),
    ]

    operations = [
        migrations.CreateModel(
            name='Occurrence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_time', models.DateTimeField(verbose_name='Beginn')),
                ('end_time', models.DateTimeField(verbose_name='Ende')),
                ('title', models.CharField(max_length=100, verbose_name='Titel', blank=True)),
                ('description', models.TextField(verbose_name='Beschreibung', blank=True)),
                ('published', models.NullBooleanField(verbose_name='Ver\xf6ffentlicht')),
                ('creator', models.ForeignKey(related_name='evcreator', on_delete=models.CASCADE, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('event', models.ForeignKey(verbose_name='Veranstaltung', on_delete=models.CASCADE, to='event.Event')),
                ('location', models.ForeignKey(related_name='evlocation', on_delete=models.PROTECT, blank=True, to='event.Location', null=True)),
                ('topics', models.ManyToManyField(related_name='evtopic', to='topic.SubTopic')),
            ],
        ),
    ]
