# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0023_auto_20160229_2348'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('topic', '0004_auto_20151230_1100'),
        ('event', '0006_auto_20160301_2002'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='occurrence',
            options={'ordering': ('start_time',), 'verbose_name': 'Termin', 'verbose_name_plural': 'Termine'},
        ),
        migrations.RenameField(
            model_name='occurrence',
            old_name='oc_description',
            new_name='description',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='oc_creator',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='oc_location',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='oc_published',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='oc_title',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='oc_topics',
        ),
        migrations.AddField(
            model_name='occurrence',
            name='creator',
            field=models.ForeignKey(related_name='oc_creator', on_delete=models.CASCADE, default=1, verbose_name='Anglegt von', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='occurrence',
            name='initiative',
            field=models.ForeignKey(related_name='oc_initiative', on_delete=models.CASCADE, default=1, verbose_name='Initiative', to='initiative.Initiative'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='occurrence',
            name='location',
            field=models.ForeignKey(related_name='oc_location', on_delete=models.PROTECT, default=1, verbose_name='Veranstaltungsort', to='event.Location'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='occurrence',
            name='published',
            field=models.BooleanField(default=1, verbose_name='Ver\xf6ffentlicht'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='occurrence',
            name='title',
            field=models.CharField(default=1, max_length=100, verbose_name='Titel'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='occurrence',
            name='topics',
            field=models.ManyToManyField(related_name='oc_topic', verbose_name='Themen', to='topic.SubTopic'),
        ),
    ]
