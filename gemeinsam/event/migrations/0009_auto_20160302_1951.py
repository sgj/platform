# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0008_auto_20160302_1950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='occurrence',
            name='topics',
            field=models.ManyToManyField(related_name='oc_topic', verbose_name='Themen', to='topic.SubTopic', blank=True),
        ),
    ]
