# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0021_auto_20160130_1151'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('topic', '0004_auto_20151230_1100'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='Titel der Veranstaltung', max_length=100, verbose_name='Titel')),
                ('description', models.TextField(help_text='Beschreibung', verbose_name='Beschreibung', blank=True)),
                ('published', models.BooleanField(default=False, help_text='Veranstaltung \xf6ffentlich zug\xe4nglich', verbose_name='Ver\xf6ffentlicht', choices=[(True, 'Ja'), (False, 'Nein')])),
                ('creator', models.ForeignKey(related_name='creator+', on_delete=models.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('initiative', models.ForeignKey(related_name='initiative+', on_delete=models.CASCADE, verbose_name='Initiative', to='initiative.Initiative')),
            ],
            options={
                'ordering': ('initiative', 'title'),
                'verbose_name': 'Veranstaltung',
                'verbose_name_plural': 'Veranstaltungen',
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, verbose_name='Name des Veranstalungsorts', blank=True)),
                ('city', models.CharField(max_length=50, verbose_name='Ort', blank=True)),
                ('zipcode', models.CharField(max_length=50, verbose_name='PLZ', blank=True)),
                ('street', models.CharField(max_length=50, verbose_name='Stra\xdfe', blank=True)),
                ('house_number', models.CharField(max_length=50, verbose_name='Hausnummer', blank=True)),
                ('county', models.CharField(max_length=50, verbose_name='Bezirk', blank=True)),
                ('state', models.CharField(max_length=50, verbose_name='Bundesland', blank=True)),
                ('country', models.CharField(max_length=50, verbose_name='Land', blank=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='Geographische Koordinaten')),
            ],
            options={
                'ordering': ('city', 'street'),
                'verbose_name': 'Veranstaltungsort',
                'verbose_name_plural': 'Veranstaltungsorte',
            },
        ),
        migrations.AddField(
            model_name='event',
            name='location',
            field=models.ForeignKey(related_name='location+', on_delete=models.PROTECT, verbose_name='Veranstaltungsort', to='event.Location'),
        ),
        migrations.AddField(
            model_name='event',
            name='topics',
            field=models.ManyToManyField(related_name='_topics_+', verbose_name='Themen', to='topic.SubTopic'),
        ),
    ]
