# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='published',
            field=models.BooleanField(default=False, help_text='Veranstaltung \xf6ffentlich zug\xe4nglich', verbose_name='Ver\xf6ffentlicht'),
        ),
    ]
