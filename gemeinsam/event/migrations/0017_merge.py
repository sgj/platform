# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0014_merge'),
        ('event', '0016_auto_20160305_2045'),
    ]

    operations = [
    ]
