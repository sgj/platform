# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0014_auto_20160304_1059'),
    ]

    operations = [
        migrations.AddField(
            model_name='occurrence',
            name='published_changed',
            field=models.BooleanField(default=False, editable=False),
        ),
        migrations.AddField(
            model_name='occurrence',
            name='topics_changed',
            field=models.BooleanField(default=False, editable=False),
        ),
    ]
