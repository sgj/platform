# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0003_occurrence'),
    ]

    operations = [
        migrations.RenameField(
            model_name='occurrence',
            old_name='title',
            new_name='oc_title',
        ),
    ]
