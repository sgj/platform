# -*- coding: utf-8 -*-


from django.db import migrations, models
import django_bleach.models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0020_auto_20160910_2254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='description',
            field=django_bleach.models.BleachField(help_text='Beschreibung', verbose_name='Beschreibung', blank=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='description',
            field=django_bleach.models.BleachField(null=True, verbose_name='Beschreibung', blank=True),
        ),
    ]
