# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0013_auto_20160302_2038'),
    ]

    operations = [
        migrations.AddField(
            model_name='occurrence',
            name='description_changed',
            field=models.BooleanField(default=False, editable=False),
        ),
        migrations.AddField(
            model_name='occurrence',
            name='location_changed',
            field=models.BooleanField(default=False, editable=False),
        ),
        migrations.AddField(
            model_name='occurrence',
            name='title_changed',
            field=models.BooleanField(default=False, editable=False),
        ),
    ]
