# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-19 23:15


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0022_auto_20170119_2249'),
        ('location', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='Event',
            name='location',
            field=models.ForeignKey(to='location.Location', on_delete=models.PROTECT)
        ),
        migrations.AlterField(
            model_name='Occurrence',
            name='location',
            field=models.ForeignKey(to='location.Location', on_delete=models.PROTECT)
        )
    ]
