# -*- coding: utf-8 -*-


from django.db import migrations, models
import django_bleach.models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0019_auto_20160311_0933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='description',
            field=django_bleach.models.BleachField(help_text='Beschreibung', blank=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='description',
            field=django_bleach.models.BleachField(null=True, blank=True),
        ),
    ]
