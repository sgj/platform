# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0015_auto_20160304_1109'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='location',
            options={'ordering': ('name', 'city', 'street'), 'verbose_name': 'Veranstaltungsort', 'verbose_name_plural': 'Veranstaltungsorte'},
        ),
        migrations.RenameField(
            model_name='location',
            old_name='location',
            new_name='coords',
        ),
        migrations.AlterField(
            model_name='location',
            name='name',
            field=models.CharField(max_length=32, verbose_name='Name des Veranstaltungsorts', blank=True),
        ),
    ]
