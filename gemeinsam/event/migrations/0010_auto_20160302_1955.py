# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0009_auto_20160302_1951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='occurrence',
            name='creator',
            field=models.ForeignKey(related_name='oc_creator', on_delete=models.CASCADE, verbose_name='Anglegt von', blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='initiative',
            field=models.ForeignKey(related_name='oc_initiative', on_delete=models.CASCADE, verbose_name='Initiative', blank=True, to='initiative.Initiative'),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='location',
            field=models.ForeignKey(related_name='oc_location', on_delete=models.PROTECT, verbose_name='Veranstaltungsort', blank=True, to='event.Location'),
        ),
    ]
