# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-11-15 21:08


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0039_auto_20170521_0121'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='update',
            field=models.DateField(auto_now=True, verbose_name='Zuletzt aktualisiert'),
        ),
    ]
