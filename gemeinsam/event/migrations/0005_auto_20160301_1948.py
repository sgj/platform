# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('topic', '0004_auto_20151230_1100'),
        ('event', '0004_auto_20160301_1942'),
    ]

    operations = [
        migrations.RenameField(
            model_name='occurrence',
            old_name='description',
            new_name='oc_description',
        ),
        migrations.RenameField(
            model_name='occurrence',
            old_name='published',
            new_name='oc_published',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='creator',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='location',
        ),
        migrations.RemoveField(
            model_name='occurrence',
            name='topics',
        ),
        migrations.AddField(
            model_name='occurrence',
            name='oc_creator',
            field=models.ForeignKey(related_name='oc_creator', on_delete=models.CASCADE, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='occurrence',
            name='oc_location',
            field=models.ForeignKey(related_name='oc_location', on_delete=models.PROTECT, blank=True, to='event.Location', null=True),
        ),
        migrations.AddField(
            model_name='occurrence',
            name='oc_topics',
            field=models.ManyToManyField(related_name='oc_topic', to='topic.SubTopic', blank=True),
        ),
    ]
