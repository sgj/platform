# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0005_auto_20160301_1948'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='occurrence',
            options={'ordering': ('start_time',), 'verbose_name': 'Ereignis', 'verbose_name_plural': 'Ereignisse'},
        ),
        migrations.AlterField(
            model_name='event',
            name='creator',
            field=models.ForeignKey(related_name='creator+', on_delete=models.CASCADE, verbose_name='Angelegt von', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='oc_creator',
            field=models.ForeignKey(related_name='oc_creator', on_delete=models.CASCADE, verbose_name='Anglegt von', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='oc_location',
            field=models.ForeignKey(related_name='oc_location', on_delete=models.PROTECT, verbose_name='Veranstaltungsort', blank=True, to='event.Location', null=True),
        ),
        migrations.AlterField(
            model_name='occurrence',
            name='oc_topics',
            field=models.ManyToManyField(related_name='oc_topic', verbose_name='Themen', to='topic.SubTopic', blank=True),
        ),
    ]
