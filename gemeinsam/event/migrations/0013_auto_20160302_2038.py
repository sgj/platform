# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0012_auto_20160302_2036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='occurrence',
            name='published',
            field=models.NullBooleanField(verbose_name='Ver\xf6ffentlicht'),
        ),
    ]
