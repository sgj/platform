# Standard Library
import datetime as dt
import json

# Django stuff
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied, ViewDoesNotExist
from django.db.models import Q
from django.http import HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

# Third party
from djgeojson.views import GeoJSONLayerView

# First party
from initiative.models import Initiative
from location.apps import GEOCODE2MODELFIELD, MAP_STRINGS
from location.forms import LocationForm

# Local imports
from .forms import EventForm
from .models import Event, Occurrence


def ajax_required(f):
    """AJAX request required decorator use it in your views.

    @ajax_required
    def my_view(request):
        ....

    """

    def wrap(request, *args, **kwargs):
        if not request.headers.get("x-requested-with") == "XMLHttpRequest":
            return HttpResponseBadRequest()
        return f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


class DetailView(generic.DetailView):
    model = Event
    context_object_name = "event"
    template_name = "event/detail.html"

    def get_context_data(self, **kwargs):
        # Django stuff
        from django.conf import settings

        context = super(DetailView, self).get_context_data(**kwargs)
        context["moderation_mail"] = getattr(
            settings, "MODERATION_EMAIL", "moderation@gemeinsam.jetzt"
        )
        return context


class DetailOccurrenceView(generic.DetailView):
    model = Occurrence
    context_object_name = "occurrence"
    template_name = "event/detail.html"

    def get_context_data(self, **kwargs):
        # Django stuff
        from django.conf import settings

        context = super(DetailOccurrenceView, self).get_context_data(**kwargs)
        context["event"] = self.get_object().event
        context["moderation_mail"] = getattr(
            settings, "MODERATION_EMAIL", "moderation@gemeinsam.jetzt"
        )
        return context


@login_required
def edit(request, id=None, init=None):
    if id:
        event = get_object_or_404(Event, pk=id)
        # check permission
        if not request.user.has_perm("event.change_event", event):
            raise PermissionDenied
        initiative = event.initiative

    elif init:
        initiative = get_object_or_404(Initiative, pk=init)
        # check permission
        if not request.user.has_perm("initiative.add_event", initiative):
            raise PermissionDenied
        event = Event(initiative=initiative, creator=request.user)
    else:
        raise ViewDoesNotExist

    location_form = LocationForm(prefix="loc")

    if request.method == "POST":
        data = request.POST.copy()
        # first check if user wants to delete this event
        if request.POST.get("deletion"):
            event.delete()
            return HttpResponseRedirect("/")

        location_id = int(request.POST.get("location"))
        # remove location data for non-physical locations
        if location_id < 0:
            del data["location"]

        if location_id == 0:
            location_form = LocationForm(data=data, prefix="loc")

            if location_form.is_valid():
                new_location = location_form.save(commit=False)
                new_location.creator = request.user
                new_location.save()
                data["location"] = "{}".format(new_location.id)

        event_form = EventForm(data=data, instance=event)

        if event_form.is_valid():
            # save the event
            event_new = event_form.save(commit=False)
            if (
                initiative.published and not initiative.moderation
            ) or initiative.abbr == "n/a":
                event_new.published = True
            event_new.on_line = location_id == -1
            event_new.save()
            event_form.save_m2m()

            return HttpResponseRedirect(reverse("event:detail", args=[event_new.pk]))

    else:
        event_form = EventForm(instance=event, user=request.user)

    return render(
        request,
        "event/edit.html",
        {
            "event_form": event_form,
            "location_form": location_form,
            "map_strings": MAP_STRINGS,
            "geocode2modelfield": GEOCODE2MODELFIELD,
            "settings": {
                "MODERATION_EMAIL": settings.MODERATION_EMAIL,
                "PELIAS_API_KEY": settings.PELIAS_API_KEY,
                "PELIAS_URL": settings.PELIAS_URL,
            },
        },
    )


def calendar(request):
    return render(request, "event/calendar.html")


@csrf_exempt
@ajax_required
def events(request):
    start = request.POST.get("start", "2016-01-01")
    end = request.POST.get("end", now() + dt.timedelta(days=365))
    occurrences = Occurrence.objects.filter(
        start_time__lte=end, end_time__gte=start, event__published=True
    ).prefetch_related(
        "event", "event__topics", "event__topics__parent", "event__initiative"
    )
    ret = []

    # check filters
    topic_ids = request.POST.getlist("topics[]", [])
    county_names = request.POST.getlist("counties[]", [])
    initiative = request.POST.get("initiative", "")
    person = request.POST.get("person", "")
    accessible = json.loads(request.POST.get("accessible", "false"))
    off_line = json.loads(request.POST.get("off_line", "false"))
    non_recurring = json.loads(request.POST.get("non_recurring", "false"))

    if topic_ids:
        occurrences = occurrences.filter(event__topics__in=topic_ids).distinct()

    if county_names:
        county_query = Q(
            event__location__county__in=[x.split(":")[-1] for x in county_names]
        )
        on_line_query = Q(event__on_line=True)
        occurrences = occurrences.filter(county_query | on_line_query)

    if initiative:
        occurrences = occurrences.filter(
            event__initiative__name__icontains=initiative
        ) | occurrences.filter(event__initiative__abbr__icontains=initiative)
    if person:
        occurrences = (
            occurrences.filter(event__initiative__admin__first_name__icontains=person)
            | occurrences.filter(event__initiative__admin__last_name__icontains=person)
            | occurrences.filter(event__initiative__contact_person__icontains=person)
        )

    if accessible:
        accessible_query = Q(event__location__accessible=True) | Q(event__on_line=True)
        occurrences = occurrences.filter(accessible_query)

    if off_line:
        off_line_query = Q(event__on_line=False)
        occurrences = occurrences.filter(off_line_query)

    for occ in occurrences:
        if not non_recurring or not occ.event.recurring():
            ret.append(
                {
                    "title": occ.title,
                    "start": occ.start_time,
                    "end": occ.end_time,
                    "list_content": occ.list_content,
                    "popup_content": occ.popup_content,
                }
            )
    return JsonResponse(ret, safe=False)


@method_decorator(csrf_exempt, name="dispatch")
class EventMapView(GeoJSONLayerView):
    def post(self, request, *args, **kwargs):
        self.context = {
            "start": request.POST.get("start", now().replace(hour=0, minute=0)),
            "end": request.POST.get("end", now() + dt.timedelta(days=7)),
            "topic_ids": request.POST.getlist("topics[]", []),
            "county_names": [
                x.split(":")[-1] for x in request.POST.getlist("counties[]", [])
            ],
            "initiative": request.POST.get("initiative", ""),
            "person": request.POST.get("person", ""),
            "accessible": json.loads(request.POST.get("accessible", "false")),
        }
        return super(EventMapView, self).render_to_response(self.context)

    def get_queryset(self):
        ret = Event.objects.filter(
            published=True,
            coords__isnull=False,
            occurrence__start_time__range=(self.context["start"], self.context["end"]),
        ).prefetch_related("initiative")

        if self.context["topic_ids"]:
            ret = ret.filter(topics__in=self.context["topic_ids"]).distinct()

        if self.context["county_names"]:
            ret = ret.filter(location__county__in=self.context["county_names"])

        if self.context["initiative"]:
            ret = ret.filter(
                initiative__name__icontains=self.context["initiative"]
            ) | ret.filter(initiative__abbr__icontains=self.context["initiative"])

        if self.context["person"]:
            ret = (
                ret.filter(
                    initiative__admin__first_name__icontains=self.context["person"]
                )
                | ret.filter(
                    initiative__admin__last_name__icontains=self.context["person"]
                )
                | ret.filter(
                    initiative__contact_person__icontains=self.context["person"]
                )
            )

        if self.context["accessible"]:
            ret = ret.filter(location__accessible=True)

        return ret


def map(request):
    return render(request, "event/map.html")
