# Django stuff
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

# Third party
from watson import search as watson


class EventAppConfig(AppConfig):
    name = "event"
    verbose_name = _("Veranstaltungen")

    def ready(self):
        EventModel = self.get_model("Event")
        watson.register(
            EventModel.objects.filter(published=True),
            fields=(
                "title",
                "description",
                "location__city",
                "location__country",
                "location__name",
            ),
        )
