# -*- coding: utf-8 -*-
# Standard Library
import datetime

# Django stuff
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

# Third party
from django_bleach.models import BleachField
from recurrence.fields import RecurrenceField

# First party
from initiative.models import Initiative
from location.models import Location
from topic.models import SubTopic


class Event(models.Model):
    # general
    title = models.CharField(
        _("Titel"), max_length=100, help_text=_("Titel der Veranstaltung")
    )
    description = BleachField(
        verbose_name=_("Beschreibung"), help_text=_("Beschreibung"), blank=True
    )

    # initiative
    initiative = models.ForeignKey(
        Initiative,
        related_name="events",
        on_delete=models.CASCADE,
        verbose_name=_("Initiative"),
    )

    start_time = models.DateTimeField(_("Beginn"))
    end_time = models.DateTimeField(_("Ende"))

    # location
    location = models.ForeignKey(
        Location,
        related_name="ev_loc",
        on_delete=models.PROTECT,
        verbose_name=_("Veranstaltungsort"),
        null=True,
        blank=True,
    )
    # probably non-physical on-line meeting
    on_line = models.BooleanField(_("On-line meeting"), default=False)

    # topics
    topics = models.ManyToManyField(
        SubTopic, related_name="topic+", verbose_name=_("Themen"), blank=True
    )

    # creator of entry
    creator = models.ForeignKey(
        User,
        related_name="creator+",
        on_delete=models.CASCADE,
        verbose_name=_("Angelegt von"),
    )

    # published
    published = models.BooleanField(
        _("Veröffentlicht"),
        help_text=_("Veranstaltung öffentlich zugänglich"),
        default=False,
    )

    # sequence that get's incremented when recurrences change
    sequence = models.IntegerField("Sequenz", default=0)

    # recurrences
    recurrences = RecurrenceField(
        _("Termine"),
        blank=True,
        null=True,
        help_text=_(
            "Hier kannst du alle Zeiten eintragen, an denen die "
            "Veranstalung stattfindet oder Ausnahmen festlegen"
        ),
    )

    # we have to save the coordinates because otherwise the object is not
    # geojson-serializable
    coords = models.PointField(
        _("Geographische Koordinaten"), srid=4326, dim=2, null=True, blank=True
    )

    # remember when event is created/updated
    update = models.DateField(_("Zuletzt aktualisiert"), auto_now=True)

    class Meta:
        verbose_name = _("Veranstaltung")
        verbose_name_plural = _("Veranstaltungen")
        ordering = (
            "initiative",
            "start_time",
            "title",
        )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/event/{}".format(self.pk)

    @property
    def uuid(self):
        # Standard Library
        import uuid

        return uuid.uuid5(uuid.NAMESPACE_OID, "{}".format(self.id))

    @property
    def popup_content(self):
        return render_to_string("event/calendar-tooltip.html", {"event": self})

    def recurring(self):
        return len(self.recurrences.rrules) + len(self.recurrences.rdates) > 0

    recurring.short_description = _("Wiederkehrend")
    recurring.boolean = True

    def next_occurrence(self):
        ret = Occurrence.objects.filter(
            event=self, sequence=self.sequence, start_time__gte=timezone.now()
        )
        return ret and ret[0] or None

    def next_occurrence_datetime(self):
        next_occurrence = self.next_occurrence()
        if next_occurrence:
            return next_occurrence.start_time
        else:
            return self.start_time

    @property
    def next_occurrence_weekday(self):
        return f"{self.next_occurrence_datetime():%u}"

    def _update_occurrences(self):
        dtstart = self.start_time
        duration = self.end_time - self.start_time
        dtend = datetime.datetime.now() + datetime.timedelta(days=365)

        # if there are rdates and/or exdates update the hh:mm
        self.recurrences.rdates = [
            x.replace(hour=dtstart.hour, minute=dtstart.minute)
            for x in self.recurrences.rdates
        ]
        self.recurrences.exdates = [
            x.replace(hour=dtstart.hour, minute=dtstart.minute)
            for x in self.recurrences.exdates
        ]

        # if there is no rule at all just create the one and only occurrence
        st_list = self.recurrences.between(
            dtstart, dtend, dtstart=dtstart, inc=True
        ) or [dtstart]

        for st_date in st_list:
            st = st_date.replace(hour=dtstart.hour, minute=dtstart.minute)
            occ, created = Occurrence.objects.get_or_create(
                start_time=st,
                end_time=st + duration,
                event=self,
                sequence=self.sequence - 1,
            )
            occ.sequence += 1
            occ.save()

        # remove any older occurrence
        Occurrence.objects.filter(event=self, sequence__lt=self.sequence).delete()

    def save(self, *args, **kwargs):
        # update sequence when start/end_time and/or recurrences have changed
        # and regenerate occurrences until now + one year
        update_ini = kwargs.pop("update_ini", True)

        if self.location:
            self.on_line = False
            self.coords = self.location.coords
        else:
            self.coords = ""

        if self.pk is not None:
            orig = Event.objects.get(pk=self.pk)
            if (
                self.recurrences != orig.recurrences
                or self.start_time != orig.start_time
                or self.end_time != orig.end_time
            ):
                self.sequence += 1
                self._update_occurrences()
            super(Event, self).save(*args, **kwargs)
        else:
            super(Event, self).save(*args, **kwargs)
            self._update_occurrences()

        # save also the initiative in order to set its
        # update-field to now
        if update_ini:
            self.initiative.save()

    def delete(self, *args, **kwargs):
        # when deleting an event set the initiative's
        # update field to now
        self.initiative.save()
        super(Event, self).delete(*args, **kwargs)


def email_notify(sender, instance, created, **kwargs):
    # inform the moderation about unpublished events
    if created and not instance.published:
        # Django stuff
        from django.conf import settings
        from django.contrib.sites.models import Site
        from django.core.mail import EmailMultiAlternatives
        from django.template import loader

        site = Site.objects.get_current()
        c = {"initiative": instance.initiative, "site": site}

        subject = "[{}] {}".format(site.name, instance.initiative)
        t = loader.get_template("initiative/moderated_events.txt")
        msg = EmailMultiAlternatives(
            subject, t.render(c), settings.SERVER_EMAIL, [settings.MODERATION_EMAIL]
        )
        msg.send()


post_save.connect(email_notify, sender=Event)


class Occurrence(models.Model):
    start_time = models.DateTimeField(_("Beginn"))
    end_time = models.DateTimeField(_("Ende"))
    event = models.ForeignKey(
        Event, verbose_name=_("Veranstaltung"), on_delete=models.CASCADE
    )
    sequence = models.IntegerField(_("Sequenz"), default=0)

    class Meta:
        verbose_name = _("Termin")
        verbose_name_plural = _("Termine")
        ordering = ("start_time",)
        unique_together = (("start_time", "end_time", "sequence", "event"),)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/event/occurrence/{}".format(self.pk)

    def save(self, *args, **kwargs):
        if self.event.location:
            self.coords = self.event.location.coords
        else:
            self.coords = ""
        super(Occurrence, self).save(*args, **kwargs)

    @property
    def title(self):
        return self.event.title

    @property
    def description(self):
        return self.event.description

    @property
    def initiative(self):
        return self.event.initiative

    @property
    def location(self):
        return self.event.location

    @property
    def topics(self):
        return self.event.topics

    @property
    def published(self):
        return self.event.published

    @property
    def creator(self):
        return self.event.creator

    @property
    def list_content(self):
        return render_to_string("event/calendar-list-view.html", {"occ": self})

    @property
    def popup_content(self):
        return render_to_string("event/calendar-tooltip.html", {"event": self.event})
