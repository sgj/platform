// toggle display of date-rules
var dr = document.querySelector("#dropdown-rules");
if (dr !== null) {
    dr.addEventListener(
	"click", function(event) {
	    document.querySelector("#rules").classList.toggle("d-none");
	    event.preventDefault();
	    event.returnValue = false;
	});
}
