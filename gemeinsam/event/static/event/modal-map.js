// map in bootstrap modal
var data = document.querySelector("#map-data");
var lat = data.dataset.lat;
var lon = data.dataset.lon;
var title = data.dataset.title;
var map;
$(window).on('map:init', function(e) {
      map = e.originalEvent.detail.map;
      icon = new L.BeautifyIcon.icon({
	  icon: 'calendar-o',
	  iconShape: 'marker',
      });
      marker = L.marker([lat, lon], {icon: icon})
      marker.bindPopup(title).addTo(map);
      map.setView(L.latLng(lat, lon), 16);
  });
  $("#map-modal").on('shown.bs.modal', function() {
      setTimeout(function() {
	  map.invalidateSize();
      }, 1);
  });
