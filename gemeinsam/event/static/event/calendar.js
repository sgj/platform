(function () {
    var today, todayMoment, todayMarked, todayWeekDay, todayElement;

    today = new Date();
    todayMoment = moment(today);
    todayMarked = false;
    todayElement = false;
    todayWeekDay = today.getDay();

    $(document).ready(function () {
	var tooltip;
	tooltip = $('<div/>').qtip({
	    id: 'fullcalendar',
	    prerender: true,
	    hide: {
		fixed: true
	    },
	    content: {
		text: ' ',
		title: {
		    button: false
		}
	    },
	    hide: {
		fixed: false
	    },
	    position: {
		my: 'top center',
		at: 'bottom center',
		target: 'event',
		viewport: $('#calendar')
	    },
	    style: {
		classes: 'event-tooltip'
	    }
	}).qtip('api');

	var stored_filters = {};
	if (!_.isUndefined(localStorage['filters'])) {
	    stored_filters = JSON.parse(localStorage['filters']);
	}
	var date_range = {
	    name: 'listMonth',
	};
	if (!_.isUndefined(localStorage['date_range'])) {
	    date_range = JSON.parse(localStorage['date_range']);
	    if (date_range.updated === undefined || moment(date_range.updated).add(1, 'd').isBefore()) {
		date_range.start = new Date()
	    };
	}
	var event_source = {
	    url: '/event/events/',
	    type: 'POST',
	    data: stored_filters
	};
	var calender_ref = $('#calendar').fullCalendar({
	    timeFormat: 'HH:mm',
	    events: event_source,
	    defaultDate: date_range.start,
	    defaultView: date_range.name,
	    scrollTime: "10:00:00",
	    firstDay: 1,
	    header: {
		left: 'prev,next today',
		center: 'title',
		right: 'month,agendaWeek,agendaDay,listMonth'
	    },
	    views: {
		listMonth: {
		    listDayFormat: 'dddd',
		    listDayAltFormat: 'D. MMMM YYYY'
		},
		agendaDay: {
		    allDaySlot: false
		},
		agendaWeek: {
		    allDaySlot: false
		},
		agendaTodayPlusSix: {
		    type: 'agendaWeek',
		    buttonText: 'Woche ab Heute',
		    duration: {
			days: 7
		    }
		}
	    },
	    eventClick: function (data, event, view) {
		if (view.name != 'listMonth') {
		    tooltip.set({
			'content.text': data.popup_content
		    }).reposition(event).show(event);
		}
	    },
	    dayClick: function () {
		tooltip.hide();
	    },
	    eventResizeStart: function () {
		tooltip.hide();
	    },
	    eventDragStart: function () {
		tooltip.hide();
	    },
	    eventRender: function (event, element, view) {
		if (view.name === 'listMonth') {
		    element[0].innerHTML = event.list_content;
		    if (!todayMarked) {
			var eventEndsAfterToday = moment(event.start).isAfter(todayMoment);
			if (eventEndsAfterToday) {
			    todayElement = $(element[0]);
			    todayMarked = true;
			}
		    }
		}
		if (event.end < new Date().getTime()) {
		    element.addClass("past-event");
		}
		/* corona */
		if (event.start>new Date("2020-03-16") & event.end<new Date("2020-05-01")) {
		    element.addClass("corona");
		}
		return element;
	    },
	    viewRender: function () {
		var firstDay, newFirstDay;
		todayMarked = false;
		tooltip.hide();
		view =  $('#calendar').fullCalendar('getView');
		window.localStorage['date_range'] = JSON.stringify({
		    name: view.name,
		    start: view.intervalStart.format(),
		    end: view.intervalEnd.format(),
		    updated: new Date()
		});
		firstDay = $('#calendar').fullCalendar('option', 'firstDay');
		newFirstDay = this.name === 'agendaTodayPlusSix' ? todayWeekDay : 1;
		if (firstDay !== newFirstDay) {
		    return $('#calendar').fullCalendar('option', 'firstDay', newFirstDay);
		}
	    },
	    eventAfterAllRender: function () {
		var container = $('.fc-scroller');
		if(todayMarked){
		    var offset = todayElement.offset().top - container.offset().top;
		    container.scrollTop(offset - 250);
		}
	    }
	});

	$.subscribe('filters.update', function (evt, new_filters) {
	    event_source.data.topics = new_filters.topics;
	    event_source.data.counties = new_filters.counties;
	    event_source.data.initiative = new_filters.initiative;
	    event_source.data.person = new_filters.person;
	    event_source.data.accessible = new_filters.accessible;
	    event_source.data.off_line = new_filters.off_line;
	    event_source.data.non_recurring = new_filters.non_recurring;
	    calender_ref.fullCalendar('refetchEvents');
	    calender_ref.fullCalendar('rerenderEvents');
	})
    });

}).call(this);
