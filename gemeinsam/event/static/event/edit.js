document.addEventListener("DOMContentLoaded", (event) => {
    // load the de locale and set it globally
    tempusDominus.loadLocale(tempusDominus.locales.de);
    tempusDominus.locale(tempusDominus.locales.de.name);

    // load the moment_parse plugin (to support de locale)
    tempusDominus.extend(tempusDominus.plugins.moment_parse, 'DD.MM.YYYY HH:mm');

    options = {
	display: {
	    icons: {
		type: "icons",
		time: "fa fa-clock",
		date: "fa fa-calendar",
		up: "fa fa-arrow-up",
		down: "fa fa-arrow-down",
		next: "fa fa-chevron-right",
		previous: "fa fa-chevron-left",
		today: "fa fa-calendar-check-o",
		clear: "fa fa-trash",
		close: "fa fa-times"
	    },
	    sideBySide: true,
	    buttons: {
		today: true,
		close: true
	    },
	    components: {
		useTwentyfourHour: true
	    }
	}
    };

    const sp = document.getElementById('start-datetime');
    const ep = document.getElementById('end-datetime');
    const st = new tempusDominus.TempusDominus(sp, options);
    const et = new tempusDominus.TempusDominus(ep, options);

    // add listeners
    sp.addEventListener(tempusDominus.Namespace.events.hide, (e) => {
	newOptions = Object.assign(options, {restrictions: {minDate: e.detail.date}});
	et.updateOptions(newOptions);
	// if end date is not set already add 1:30 to start date
	if (document.getElementById('id_end_time').value == "") {
	    end_date = e.detail.date.clone;
	    end_date.manipulate(90, "minutes");
	    et.dates.setValue(end_date);
	}
    });

    ep.addEventListener(tempusDominus.Namespace.events.hide, (e) => {
	newOptions = Object.assign(options, {restrictions: {maxDate: e.detail.date}});
	st.updateOptions(newOptions);
    });
});
