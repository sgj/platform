# Django stuff
from django.urls import path

# Local imports
from . import views
from .models import Event

app_name = "event"
urlpatterns = [
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path(
        "occurrence/<int:pk>/",
        views.DetailOccurrenceView.as_view(),
        name="detail-occurrence",
    ),
    path("add/<int:init>/", views.edit, name="add-event"),
    path("edit/<int:id>/", views.edit, name="edit-event"),
    path("calendar/", views.calendar, name="calendar"),
    path("events/", views.events, name="events"),
    path("map/", views.map, name="map"),
    path(
        "data.geojson",
        views.EventMapView.as_view(
            model=Event,
            properties=("title", "topics", "next_occurrence_weekday", "popup_content"),
            geometry_field="coords",
        ),
        name="data",
    ),
]
