# -*- coding: utf-8 -*-


# Django stuff
from django.core.exceptions import ObjectDoesNotExist

# Third party
from rules import add_perm, is_group_member, predicate


@predicate
def is_initiative_admin(user, event):
    return user == event.initiative.admin


is_moderator = is_group_member("Moderation")


@predicate
def event_editor(user, event):
    try:
        event.initiative.permission_set.get(user=user, module=2)
        return True
    except ObjectDoesNotExist:
        return False


add_perm("event", is_moderator)
add_perm("event.delete_event", is_moderator | is_initiative_admin | event_editor)
add_perm("event.change_event", is_moderator | is_initiative_admin | event_editor)
add_perm("event.add_event", is_moderator)
