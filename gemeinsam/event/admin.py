# -*- coding: utf-8 -*-

# Django stuff
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

# Local imports
from .models import Event


def make_published(modeladmin, request, queryset):
    for instance in queryset:
        instance.published = True
        instance.save()


make_published.short_description = _("Ausgewählte Veranstaltungen veröffentlichen")


def make_unpublished(modeladmin, request, queryset):
    for instance in queryset:
        instance.published = False
        instance.save()


make_unpublished.short_description = _("Ausgewählte Veranstaltungen unveröffentlichen")


class InitiativeFilter(admin.SimpleListFilter):
    title = _("Initiative")
    parameter_name = "initiative"

    def lookups(self, request, model_admin):
        initiative_list = [x.initiative for x in model_admin.model.objects.all()]
        initiatives = set(initiative_list)
        return sorted([(i.id, i.name) for i in initiatives], key=lambda x: x[1])

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(initiative__id__exact=self.value())
        else:
            return queryset


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ["title", "initiative", "location", "published", "recurring"]
    search_fields = ["title", "initiative__name"]
    list_filter = [
        InitiativeFilter,
        "published",
    ]
    actions = [make_published, make_unpublished]
    readonly_fields = ["coords"]
