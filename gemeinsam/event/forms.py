# -*- coding: utf-8 -*-
"""
Event form
"""
# Django stuff
from django import forms
from django.utils.translation import gettext_lazy as _

# Third party
from django_ckeditor_5.widgets import CKEditor5Widget

# First party
from common.utils import add_link_target_blank
from location.models import locations_as_choices
from topic.models import topics_as_choices

# Local imports
from .models import Event


class EventForm(forms.ModelForm):
    deletion = forms.BooleanField(required=False, label=_("Unwiderruflich löschen"))

    class Meta:
        model = Event
        fields = [
            "title",
            "description",
            "start_time",
            "end_time",
            "location",
            "topics",
            "recurrences",
        ]

        widgets = {
            "description": CKEditor5Widget(),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super(EventForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field not in ["description", "recurrences", "deletion"]:
                self.fields[field].widget.attrs.update(
                    {
                        "class": "form-control",
                        "placeholder": self.fields[field].help_text
                        and self.fields[field].help_text
                        or self.fields[field].label,
                    }
                )

        # dealing with locations
        loc_id = self.instance.location_id or self.instance.initiative.location_id

        locs = (
            [
                [
                    _("Keine Ortsangabe"),
                    [[-100, _("Noch nicht bekannt")], [-1, _("On-line Treffen")]],
                ]
            ]
            + locations_as_choices(user=self.user, id=loc_id)
            + [[_("Neuer Ort"), [[0, _("Auf Karte wählen")]]]]
        )

        self.fields["location"].choices = locs
        self.fields["location"].widget.attrs.update(
            {
                "class": "selectpicker form-control",
                "data-live-search": "true",
            }
        )

        # set the value of location
        if self.instance.on_line:
            # to on-line
            self.initial["location"] = -1
        else:
            # or to stored location
            self.initial["location"] = self.instance.location_id

        # dates
        self.fields["start_time"].widget.attrs.update(
            {"data-td-target": "#start-datetime", "placeholder": "TT.MM.JJJJ SS:MM"}
        )
        self.fields["end_time"].widget.attrs.update(
            {"data-td-target": "#end-datetime", "placeholder": "TT.MM.JJJJ SS:MM"}
        )

        # dealing with subtopic / topic optgroups
        self.fields["topics"].choices = topics_as_choices()
        self.fields["topics"].widget.attrs.update(
            {
                "class": "selectpicker form-control",
                "multiple": "",
                "data-live-search": "true",
                "data-live-search-placeholder": _("Suche ..."),
                "data-max-options": "3",
                "data-max-options-text": _("Maximum von 3 Themen erreicht"),
                "data-title": _("Wähle bis zu 3 Themen"),
            }
        )

    def clean(self):
        cleaned_data = super(EventForm, self).clean()
        start_time = cleaned_data.get("start_time")
        end_time = cleaned_data.get("end_time")

        try:
            if start_time >= end_time:
                self.add_error("start_time", _("Beginn muss vor dem Ende liegen"))
                self.add_error("end_time", _("Ende muss nach dem Beginn liegen"))
        except TypeError:
            # start_time and/or end_time not given
            pass

    def clean_description(self):
        data = self.cleaned_data.get("description", "")
        return add_link_target_blank(data)
