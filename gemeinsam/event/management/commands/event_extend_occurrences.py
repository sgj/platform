# Standard Library
import datetime

# Django stuff
from django.core.management.base import BaseCommand

# First party
from event.models import Event, Occurrence


class Command(BaseCommand):
    help = "Extend occurrences"

    def add_arguments(self, parser):
        parser.add_argument(
            "--verbose",
            action="store_true",
            dest="verbose",
            default=False,
            help="print out summary for new occurrences",
        )
        parser.add_argument(
            "--days",
            dest="days",
            type=int,
            default=365,
            help="number of days from today that shall be considered",
        )

    def handle(self, *args, **options):
        dtend = datetime.datetime.now() + datetime.timedelta(days=options["days"])

        for event in Event.objects.all():
            count = 0
            dtstart = event.start_time
            duration = event.end_time - event.start_time

            # get list of occurrences
            st_list = event.recurrences.between(dtstart, dtend, dtstart=dtstart)

            # (re)create occurrences
            for st_date in st_list:
                st = st_date.replace(hour=dtstart.hour, minute=dtstart.minute)
                occ, created = Occurrence.objects.get_or_create(
                    start_time=st,
                    end_time=st + duration,
                    event=event,
                    sequence=event.sequence,
                )
                count += created

            if options["verbose"] and count > 0:
                self.stdout.write("{} new occurrence(s) for {}".format(count, event))
