# Django stuff
from django.contrib import admin

# Local imports
from .models import SubTopic, Topic

# Register your models here.


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ("name", "order", "icon")
    list_display_links = ("name",)

    search_fields = ("name", "description")


@admin.register(SubTopic)
class SubTopicAdmin(admin.ModelAdmin):
    list_display = ("name", "parent", "order")
    list_display_links = ("name",)
    list_filter = ("parent",)

    search_fields = ("name", "description")
