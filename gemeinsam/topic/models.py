# -*- coding: utf-8 -*-

# Django stuff
from django.db import models
from django.utils.translation import gettext_lazy as _

# Third party
from stdimage import StdImageField


class Topic(models.Model):
    name = models.CharField(
        _("Name"),
        max_length=50,
        unique=True,
        help_text=_(
            "Dies ist der Name des Themas, der überall auf "
            "der Home-page erscheinen wird"
        ),
    )
    description = models.TextField(
        _("Erklärung"), help_text=_("Text, der dieses Thema beschreibt")
    )
    icon = StdImageField(
        _("Kleines Bild zu diesem Thema"),
        upload_to="topic",
        variations={
            "small": (32, 32, True),
            "medium": (64, 64, True),
            "large": (128, 128, True),
        },
    )
    photo = StdImageField(
        _("Photo zum Thema"),
        upload_to="topic",
        null=True,
        blank=True,
        variations={"samll": (320, 213), "medium": (640, 426), "large": (1280, 852)},
    )
    order = models.SmallIntegerField(
        _("Sortierung"),
        default=0,
        help_text=_(
            "Diese Feld dient dazu, die Themen in der gewünschten "
            "Reihenfolge anzuzeigen. Je größer dieser Wert ist, desto "
            "weiter hinten erscheint dieses Thema"
        ),
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/topic/{}".format(self.pk)

    def initiative_list(self):
        """
        Returns distinct sorted list of published initiatives.
        """
        initiatives = []
        for st in self.subtopic_set.all():
            initiatives.extend(st.published_initiatives())
        initiatives = list(set(initiatives))
        return sorted(initiatives, key=lambda x: x.name.lower())

    class Meta:
        verbose_name = _("Thema")
        verbose_name_plural = _("Themen")

        ordering = ["order", "name"]


class SubTopic(models.Model):
    name = models.CharField(
        _("Name"), max_length=50, unique=True, help_text=_("Name des Subthemas")
    )
    description = models.TextField(
        _("Erklärung"), blank=True, help_text=_("Text, der dieses Subthema beschreibt")
    )
    order = models.SmallIntegerField(
        _("Sortierung"),
        default=0,
        help_text=_(
            "Diese Feld dient dazu, die Subthemen in der "
            "gewünschten Reihenfolge anzuzeigen. Je größer "
            "dieser Wert ist, desto weiter hinten erscheint "
            "dieses Subthema"
        ),
    )
    parent = models.ForeignKey(
        Topic, on_delete=models.CASCADE, verbose_name=_("Übergeordnetes Thema")
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Subthema")
        verbose_name_plural = _("Subthemen")

        ordering = ["parent__order", "parent__name", "order", "name"]

    def published_initiatives(self):
        return self.initiative_set.filter(published=True)


def topics_as_choices():
    topics = []
    for topic in Topic.objects.all():
        sub_topics = []
        for sub_topic in topic.subtopic_set.all():
            sub_topics.append([sub_topic.id, sub_topic.name])

        new_topic = [topic.name, sub_topics]
        topics.append(new_topic)

    return topics


def topics_as_choices_dict():
    topics = []
    for topic in Topic.objects.all():
        sub_topics = []
        for sub_topic in topic.subtopic_set.all():
            sub_topics.append({"id": sub_topic.id, "name": sub_topic.name})

        new_topic = {"id": topic.id, "name": topic.name, "sub_topics": sub_topics}
        topics.append(new_topic)

    return topics
