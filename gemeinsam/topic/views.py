# Django stuff
from django.shortcuts import get_object_or_404, render

# Local imports
from .models import Topic


# Create your views here.
def index(request):
    return render(request, "topic/index.html")


def detail(request, id):
    topic = get_object_or_404(Topic, pk=id)
    return render(request, "topic/detail.html", {"topic": topic})
