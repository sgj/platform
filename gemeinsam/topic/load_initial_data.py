#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Load initial topic data into database and media directory.

Silently ignore if data is already available
"""
# Standard Library
import os
import sys

# Django stuff
import django

from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File

proj_path = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]
sys.path.append(proj_path)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gemeinsam.settings")
django.setup()

fixture_dir = os.path.join(os.path.dirname(__file__), "fixtures")
media_dir = os.path.join(fixture_dir, "media")

fixture_file = os.path.join(fixture_dir, "initial-data.yml")

if __name__ == "__main__":
    models = set()
    for object_ in serializers.deserialize("yaml", open(fixture_file, "r")):
        obj = object_.object
        model = type(obj)

        # if the object is already in the database do nothing
        try:
            _ = model.objects.get(pk=obj.pk)
        except ObjectDoesNotExist:
            print(f"Saving {model.__name__}: {obj}")
            models.add(model)
            obj.save()
            if hasattr(obj, "icon"):
                obj.icon.save(
                    os.path.basename(obj.icon.name),
                    File(open(os.path.join(media_dir, obj.icon.name), "rb")),
                )

    # set the sequences to the last value
    models = list(models)
    if models:
        # Django stuff
        from django.core.management.color import no_style
        from django.db import connection

        print(
            (
                f"Reset the database id-sequence for "
                f"{' and '.join([m.__name__ for m in models])}"
            )
        )
        sequence_sql = connection.ops.sequence_reset_sql(no_style(), models)
        with connection.cursor() as cursor:
            for sql in sequence_sql:
                cursor.execute(sql)
