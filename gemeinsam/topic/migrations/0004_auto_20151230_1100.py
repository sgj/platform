# -*- coding: utf-8 -*-


from django.db import migrations, models
import stdimage.models


class Migration(migrations.Migration):

    dependencies = [
        ('topic', '0003_auto_20151117_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topic',
            name='icon',
            field=stdimage.models.StdImageField(upload_to=b'topic', verbose_name='Kleines Bild zu diesem Thema'),
        ),
    ]
