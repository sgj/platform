# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('topic', '0002_auto_20151116_1926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topic',
            name='icon',
            field=models.ImageField(upload_to=b'topic', verbose_name='Kleines Bild zu diesem Thema'),
        ),
    ]
