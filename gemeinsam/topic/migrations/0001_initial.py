# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SubTopic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Name des Subthemas', unique=True, max_length=50, verbose_name='Name')),
                ('description', models.TextField(help_text='Text, der dieses Subthema beschreibt', verbose_name='Erkl\xe4rung', blank=True)),
                ('order', models.SmallIntegerField(default=0, help_text='Diese Feld dient dazu, die Subthemen in der gew\xfcnschten Reihenfolge anzuzeigen. Je gr\xf6\xdfer dieser Wert ist, desto weiter hinten erscheint dieses Subthema', verbose_name='Sortierung')),
            ],
            options={
                'ordering': ['order', 'name'],
                'verbose_name': 'Subthema',
                'verbose_name_plural': 'Subthemen',
            },
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Dies ist der Name des Themas, der \xfcberall auf der Home-page erscheinen wird', unique=True, max_length=50, verbose_name='Name')),
                ('description', models.TextField(help_text='Text, der dieses Thema beschreibt', verbose_name='Erkl\xe4rung')),
                ('icon', models.ImageField(upload_to=b'', verbose_name='Kleines Bild zu diesem Thema')),
                ('order', models.SmallIntegerField(default=0, help_text='Diese Feld dient dazu, die Themen in der gew\xfcnschten Reihenfolge anzuzeigen. Je gr\xf6\xdfer dieser Wert ist, desto weiter hinten erscheint dieses Thema', verbose_name='Sortierung')),
            ],
            options={
                'ordering': ['order', 'name'],
                'verbose_name': 'Thema',
                'verbose_name_plural': 'Themen',
            },
        ),
        migrations.AddField(
            model_name='subtopic',
            name='parent',
            field=models.ForeignKey(verbose_name='\xdcbergeordnetes Thema', on_delete=models.CASCADE, to='topic.Topic'),
        ),
    ]
