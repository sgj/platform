# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('topic', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subtopic',
            options={'ordering': ['parent__order', 'order', 'name'], 'verbose_name': 'Subthema', 'verbose_name_plural': 'Subthemen'},
        ),
    ]
