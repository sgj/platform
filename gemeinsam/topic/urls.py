# Django stuff
from django.urls import path

# Local imports
from . import views

app_name = "topic"
urlpatterns = [
    path("", views.index, name="index"),
    path("<int:id>/", views.detail, name="detail"),
]
