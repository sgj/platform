# Django stuff
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

# Third party
from watson import search as watson


class TopicAppConfig(AppConfig):
    name = "topic"
    verbose_name = _("Themen")

    def ready(self):
        TopicModel = self.get_model("topic")
        watson.register(TopicModel.objects.all(), exclude=("icon", "order"))
