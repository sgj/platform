#!/bin/bash

WHOAMI=$SUDO_USER

if [ "$WHOAMI"xx == "xx" ]
then
    echo "The install script must be run with sudo"
    exit
fi

SCRIPT=$(readlink -f "$0")
SCRIPTDIR=$(dirname "${SCRIPT}")
BASEDIR=$(realpath "${SCRIPTDIR}"/..)

SOCKFILE=${BASEDIR}/run/gunicorn.sock
LOGDIR=${BASEDIR}/logs

mkdir -p ${LOGDIR}

# install nginx conf
sed -e "s#SOCKFILE#$SOCKFILE#g" \
    -e "s#LOGDIR#$LOGDIR#g" \
    -e "s#BASEDIR#$BASEDIR#g" nginx_gemeinsam.conf > /etc/nginx/sites-available/gemeinsam.conf
ln -fs /etc/nginx/sites-available/gemeinsam.conf /etc/nginx/sites-enabled/gemeinsam.conf

# install supervisor conf
sed -e "s#SCRIPTDIR#$SCRIPTDIR#g" \
    -e "s#WHOAMI#$WHOAMI#g" \
    -e "s#LOGDIR#$LOGDIR#g" supervisor_gemeinsam.conf > /etc/supervisor/conf.d/gemeinsam.conf


# collect statics
echo "Do not forget to run manage.py collectstatic with the correct environment"
#yes yes | ./manage.py collectstatic >/dev/null
