# -*- coding: utf-8 -*-
# Standard Library
import csv

# Django stuff
from django.contrib import admin
from django.http import HttpResponse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

# Local imports
from .models import Subscription


def export_active(modeladmin, request, queryset):
    query = modeladmin.model.objects.filter(is_active=True).order_by("email")
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = (
        "attachment; filename=adressen_newsletter_{:%F}.csv".format(now())
    )
    writer = csv.writer(response)
    for obj in query:
        writer.writerow([obj.email])
    return response


export_active.short_description = _(
    "Email Adressen für Newsletter exportieren "
    "(unabhängig von der aktuellen Auswahl)"
)
export_active.acts_on_all = True


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ["email", "is_active"]
    list_filter = ["is_active"]
    actions = [export_active]

    def changelist_view(self, request, extra_context=None):
        try:
            action = self.get_actions(request)[request.POST["action"]][0]
            action_acts_on_all = action.acts_on_all
        except (KeyError, AttributeError):
            action_acts_on_all = False

        if action_acts_on_all:
            post = request.POST.copy()
            post.setlist(
                admin.helpers.ACTION_CHECKBOX_NAME,
                self.model.objects.values_list("id", flat=True),
            )
            request.POST = post

        return admin.ModelAdmin.changelist_view(self, request, extra_context)
