# Django stuff

from django.apps import AppConfig


class NewsletterConfig(AppConfig):
    name = "newsletter"
