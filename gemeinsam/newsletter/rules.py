# -*- coding: utf-8 -*-


# Third party
from rules import add_perm, is_group_member

is_moderator = is_group_member("Moderation")

add_perm("newsletter", is_moderator)
add_perm("newsletter.add_subscription", is_moderator)
add_perm("newsletter.change_subscription", is_moderator)
add_perm("newsletter.delete_subscription", is_moderator)
