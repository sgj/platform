from django.conf import settings
from django.http import JsonResponse
from location.models import location_states_counties_dict
from topic.models import topics_as_choices_dict


def filters(request):
    topics = topics_as_choices_dict()
    states = location_states_counties_dict()

    region = getattr(settings, "LOCATION_REGION_DEFAULT", "")
    data = {"status": "ok", "topics": topics, "states": states, "region": region}

    response = JsonResponse(data, json_dumps_params={"ensure_ascii": False})
    return response
