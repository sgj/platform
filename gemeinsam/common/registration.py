# Third party
from captcha.fields import CaptchaField
from registration.forms import RegistrationFormUsernameLowercase


class RegistrationFormCaptcha(RegistrationFormUsernameLowercase):
    """Add a captcha to the registration."""

    captcha = CaptchaField()
