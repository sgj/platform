def add_link_target_blank(data):
    """
    add link target="_blank" to each html link
    """
    # Third party
    from bs4 import BeautifulSoup

    soup = BeautifulSoup(data, "html.parser")
    links = soup.find_all("a")
    for link in links:
        link["target"] = "_blank"
    return str(soup)
