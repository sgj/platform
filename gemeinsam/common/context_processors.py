# Django stuff
from django.conf import settings


def site(request):
    return {"SITE_URL": settings.SITE_URL, "MATOMO_SERVER": settings.MATOMO_SERVER}
