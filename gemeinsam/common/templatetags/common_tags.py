# Django stuff
from django.template import Library
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

register = Library()


@stringfilter
def parse_date(date_string):
    """
    Return a datetime corresponding to date_string, parsed according to format.

    For example, to re-display a date string in another format::

        {{ "01/01/1970"|parse_date|date:"F jS, Y" }}

    """
    # Third party
    from dateutil.parser import parse

    try:
        return parse(date_string)
    except ValueError:
        return None


register.filter(parse_date)


@register.filter()
def nbsp(value):
    return mark_safe("&nbsp;".join(value.split(" ")))


@register.filter()
def html2text(value):
    # Third party
    from bs4 import BeautifulSoup

    return BeautifulSoup(value, "html5lib").text.replace("\n", " ")


@register.filter()
def highlight(value, arg):
    # Standard Library
    import re

    words = arg.split()
    sentences = []
    for word in words:
        sentences += re.findall(rf"([^.]*{word}[^.]*)", value, re.IGNORECASE)

    # choose the first match
    try:
        sentence = sentences[0]
    except IndexError:
        sentence = value

    occurrences = []
    for word in words:
        occurrences += re.findall(rf"{word}", sentence, re.IGNORECASE)

    for occurrence in set(occurrences):
        sentence = sentence.replace(
            occurrence, f"<strong class='fw-bold'>{occurrence}</strong>"
        )

    return mark_safe(sentence)


@register.filter(name="add_attr")
def add_attribute(field, css):
    attrs = field.subwidgets[0].data[
        "attrs"
    ]  # Accessing the already declared attributes
    definition = css.split(",")

    for d in definition:
        if ":" not in d:
            attrs["class"] += f" {d}"  # Extending the class string
        else:
            key, val = d.split(":")
            attrs[key] += f"{val}"  # Extending the `key` string

    return field.as_widget(attrs=attrs)


@register.filter(name="add_invalid")
def add_invalid(field):
    """Add the is-invalid class if there are errors."""
    attrs = field.subwidgets[0].data["attrs"]
    if field.errors:
        attrs["class"] += " is-invalid"

    return field.as_widget(attrs=attrs)
