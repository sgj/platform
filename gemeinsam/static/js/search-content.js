(function () {
    function triggerEvent(el, type) {
	// IE9+ and other modern browsers
	if ('createEvent' in document) {
            var e = document.createEvent('HTMLEvents');
            e.initEvent(type, false, true);
            el.dispatchEvent(e);
	} else {
            // IE8
            var e = document.createEventObject();
            e.eventType = type;
            el.fireEvent('on' + e.eventType, e);
	}
    }

    const types = ["initiative", "event", "topic", "profiles"];
    types.forEach(function (item) {
	var button=document.querySelector("#btn-"+item);
	if (button != null) {
	    document.querySelector('[for=btn-'+item+'] span')
		.textContent = document.querySelectorAll('[data='+item+']').length;
	    button.addEventListener("change", function() {
		els = document.querySelectorAll("[data="+item+"]");
		if (this.checked) {
		    els.forEach((el) => { el.classList.remove("d-none"); })
		} else {
		    els.forEach((el) => { el.classList.add("d-none"); })
		}
		if (item === "event") {
		    // enable or disable the past-event
		    document.getElementById("past-event").disabled = !this.checked;
		    triggerEvent(document.getElementById("past-event"), "change");
		}
	    });
	}
    });

    // past events handling
    document.querySelector('[for=past-event] span')
	.textContent = document.querySelectorAll('.past-event').length;
    document.querySelector("#past-event").addEventListener("change", function() {
	var show_events = document.querySelector('#btn-event').checked
	els = document.querySelectorAll(".past-event");
	if (this.checked && show_events) {
	    els.forEach((el) => { el.classList.remove("d-none"); })
	} else {
	    els.forEach((el) => { el.classList.add("d-none"); })
	}
    });
    triggerEvent(document.getElementById("past-event"), "change");
})();
