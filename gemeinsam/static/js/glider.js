// set glider on div with glider class
if (document.querySelector('.glider')) {
    glider = new Glider(document.querySelector('.glider'),{
        slidesToShow: 1,
	slidesToScroll: 1,
	draggable: true,
	duration: 5,
        arrow: {
            prev: '.glider-prev',
            next: '.glider-next'
        },
        dots: '.dots',
	responsive: [
	    {
		breakpoint: 768,
		settings: {
		    slidesToShow: 3,
                    slidesToScroll: 3
                }
	    }
	]
    });
}
