$(window).on('map:init', function(e) {
    var dataurl = $('#map-info').attr('url');
    var lon = $('#map-info').attr('lon');
    var lat = $('#map-info').attr('lat');

    var map = e.originalEvent.detail.map;
    map.attributionControl.setPrefix(
	'<a href="http://leafletjs.com/" target="_blank" rel="noopener noreferrer">Leaflet</a>');

    var markers;
    var initiativeMarker = new L.BeautifyIcon.icon({
      icon: 'users',
      iconShape: 'marker',
    });

    var weekdayMarkers = [ ...Array(7).keys()].map( i =>
	new L.BeautifyIcon.icon({
	    iconShape: 'marker',
	    isAlphaNumericIcon: true,
	    text: moment.weekdaysShort(i+1).substring(0,2).toUpperCase(),
	})
    );

    var map_view = {
	lat: map.options.djoptions.center[0],
	lon: map.options.djoptions.center[1],
	zoom: map.options.djoptions.zoom,
	baselayer: map.options.djoptions.layers[0][0]
    };
    var stored_filters = {};
    var date_range = {
	name: 'agendaDay',
    };
    if (!_.isUndefined(localStorage['map_view'])) {
	map_view = JSON.parse(localStorage['map_view']);
    }
    if (!_.isUndefined(localStorage['filters'])) {
	stored_filters = JSON.parse(localStorage['filters']);
    }
    if (!_.isUndefined(localStorage['date_range'])) {
        date_range = JSON.parse(localStorage['date_range']);
	if (date_range.updated === undefined || moment(date_range.updated).add(1, 'd').isBefore()) {
	    date_range.start = new Date()
	};
    }
    if (date_range.name.endsWith("onth")) {
	date_range.name = 'agendaWeek';
    }

    // remember current view after a move
    map.on('moveend', function(e) {
	map_view.lat = map.getCenter().lat;
	map_view.lon = map.getCenter().lng;
	map_view.zoom = map.getZoom();
	window.localStorage['map_view'] = JSON.stringify(map_view);
    });

    map.on('baselayerchange', function(e) {
	map_view.baselayer = e.name;
	window.localStorage['map_view'] = JSON.stringify(map_view);
    });

    // if there is calendar control
    if ($('#calendar').length) {
	var calendar = $('#calendar').fullCalendar({
	    timeFormat: 'HH:mm',
	    defaultDate: date_range.start,
	    defaultView: date_range.name,
	    firstDay: 1,
	    header: {
		left: 'prev,next today',
		center: 'title',
		right: 'agendaWeek,agendaTodayPlusSix,agendaDay'
	    },
	    views: {
		agendaTodayPlusSix: {
		    type: 'agendaWeek',
		    buttonText: 'Woche ab Heute',
		    duration: {
			days: 7
		    }
		}
	    },
	    viewRender: function() {
		view =  $('#calendar').fullCalendar('getView');
		window.localStorage['date_range'] = JSON.stringify({
		    name: view.name,
		    start: view.intervalStart.format(),
		    end: view.intervalEnd.format(),
		    updated: new Date()
		});
		dr = {
		    start: view.intervalStart.format(),
		    end: view.intervalEnd.format()
		}
		$.ajax({
		    dataType: 'json',
		    type: 'POST',
		    url: dataurl,
		    data: $.extend({}, dr, stored_filters),
		    success: function(data) {
			update_map(data);
		    }
		});
	    }
	});
    } else {
	// make the first post with stored filters
	$.ajax({
	    dataType: 'json',
	    type: 'POST',
	    url: dataurl,
	    data: stored_filters,
	    success: function(data) {
		update_map(data);
	    }
	});
    }

    // subscribe to filter events
    $.subscribe('filters.update', function(evt, new_filters) {
	stored_filters = new_filters;
	$.ajax({
	    dataType: 'json',
	    type: 'POST',
	    url: dataurl,
	    data: $.extend({}, date_range, new_filters),
	    success: function(data) {
		update_map(data);
	    }
	})
    });

    function update_map(mydata) {
	if (markers) {
	    map.removeLayer(markers);
	}
	markers = L.markerClusterGroup({
	    showCoverageOnHover: false,
	    maxClusterRadius: 20
	});
	for (var i = 0; i<mydata.features.length; i++) {
	    var a = mydata.features[i].geometry.coordinates;
	    title = mydata.features[i].properties.popup_content;
	    weekday = mydata.features[i].properties.next_occurrence_weekday
	    marker = L.marker(new L.LatLng(a[1], a[0], {title: title}),
			      {icon: initiativeMarker});
	    if (weekday) {
		marker.setIcon(weekdayMarkers[parseInt(weekday)-1]);
	    }
	    marker.bindPopup(title);
	    markers.addLayer(marker);
	}
	map.addLayer(markers);
    };

    if (lat != '0' ) {
	map.setView(L.latLng(
	    parseFloat(lat),
	    parseFloat(lon)), 16);
    } else {
	// restore last view or try to get current position of user
	map.setView(L.latLng(
	    map_view.lat, map_view.lon), map_view.zoom);
    }

    // set stored baselayer
    $("input.leaflet-control-layers-selector").each(function(index) {
	if ($(this).siblings('span').text().trim() === map_view.baselayer) {
	    $(this).trigger('click');
	}
    });
});
