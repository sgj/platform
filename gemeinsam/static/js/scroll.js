window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.documentElement.scrollTop > 90) {
	$("#header-nav .nav-cell i").hide();
	$("#header-nav #logo img").height(80);
	$("#header-nav #logo img").css("margin-top", "-15%");
    } else if (document.documentElement.scrollTop < 1) {
	$("#header-nav .nav-cell i").show();
	$("#header-nav #logo img").height(180);
	$("#header-nav #logo img").css("margin-top", "-10%");
    }
}
