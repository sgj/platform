(function ($) {
    var template = $('#filterTemplate').html();

    var data = {
	topics: [],
	states: [],
	person: '',
	initiative: '',
	accessible: false,
	off_line: false,
	non_recurring: false,
	region: '',
	by_id: {
	    sub_topics: {}
	}
    };
    var vm = new Vue({
	el: '#filter',
	data: data,
	template: template,
	created: function () {

	    // load saved values from local storage, if available
	    var saved_filters_str = window.localStorage['filters'];
	    var saved_filters = {
		topics: undefined,
		counties: undefined
	    };

	    if (!_.isEmpty(saved_filters_str)) {
		saved_filters = JSON.parse(saved_filters_str);
	    }

	    // load default filter layout from server
	    this.$http.get('/filters/')
		.then(function (response) {
		    var topics = response.body.topics;
		    var states = response.body.states;
		    var region = response.body.region;
		    var all_subtopics_by_id = {};

		    // process topics
		    _.forEach(topics, function (topic) {
			// set default value of topic.selected
			topic.selected = false;
			topic.indeterminate = true;
			topic.hide = true;

			// set selected subtopics from saved values
			_.forEach(topic.sub_topics, function (subtopic) {
			    if (_.isUndefined(saved_filters.topics)) {
				subtopic.selected = true;
			    } else {
				subtopic.selected = _.includes(saved_filters.topics, subtopic.id);
			    }
			});

			// trigger check of the topic selected value based on selected subtopics
			vm.recheck_topic_selected_value(topic);

			// build index of subtopics by id for easier manipulation
			var subtopics_by_id = _.keyBy(topic.sub_topics, 'id');
			_.assign(all_subtopics_by_id, subtopics_by_id);

		    });
		    topics.number = topics.map(function(topic) { return topic.sub_topics.length }).reduce((acc, curr) => acc+curr, 0);

		    // process counties
		    _.forEach(states, function(state) {
			// default is not selected
			state.selected = false;
			state.indeterminate = true;
			state.hide = true;

			// set selected counties from saved values
			_.forEach(state.counties, function(county) {
			    if (_.isUndefined(saved_filters.counties)) {
				county.selected = true;
			    } else {
				county.selected = _.includes(saved_filters.counties, state.name + ':' + county.name);
			    }
			});

			// trigger check of state selected value based on selected counties
			vm.recheck_state_selected_value(state);
		    });
		    states.number = states.map(function(state) { return state.counties.length }).reduce((acc, curr) => acc+curr, 0);

		    // search strings
		    vm.initiative = _.isUndefined(saved_filters.initiative) ? '' : saved_filters.initiative;
		    vm.person = _.isUndefined(saved_filters.person) ? '' : saved_filters.person;

		    // search for accessible location
		    vm.accessible = _.isUndefined(saved_filters.accessibel) ? false : saved_filters.accessible;

		    // search for on-line events
		    vm.off_line = _.isUndefined(saved_filters.off_line) ? true : saved_filters.off_line;
		    // search for non-recurring events
		    vm.non_recurring = _.isUndefined(saved_filters.non_recurring) ? true : saved_filters.non_recurring;

		    // update view model with loaded values
		    vm.topics = topics;
		    vm.states = states;
		    vm.region = region;
		    vm.by_id.sub_topics = all_subtopics_by_id;
		});
	},
	mounted: function () {

	},
	directives: {
	    indeterminate: function(el, binding) {
		el.indeterminate = Boolean(binding.value);
	    }
	},
	methods: {
	    topics_selected: function() {
		selected = this.topics.map(function(topic) {
		    return topic.sub_topics.filter(
			x => x.selected).length
		}).reduce((acc, curr) => acc+curr, 0);
		if (selected == this.topics.number) {
		    return 0;
		} else {
		    return selected;
		}
	    },

	    recheck_topic_selected_value: function (topic) {
		all = topic.sub_topics.every(x => x.selected)
		none = topic.sub_topics.every(x => !x.selected)

		topic.selected = all && !none
		topic.indeterminate = ! (all || none);
	    },

	    topic_changed: function (topic) {
		topic.indeterminate = false;
		// apply new checked value to all sub_topics
		_.forEach(topic.sub_topics, function (x) {
		    x.selected = topic.selected
		});
	    },

	    subtopic_changed: function (topic, subtopic) {
		// check is parent topic needs checked value update
		this.recheck_topic_selected_value(topic);
	    },

	    states_selected: function() {
		selected = this.states.map(function(state) {
		    return state.counties.filter(
			x => x.selected).length
		}).reduce((acc, curr) => acc+curr, 0);
		if (selected == this.states.number) {
		    return 0;
		} else {
		    return selected;
		}
	    },

	    recheck_state_selected_value: function(state) {
		all = state.counties.every(x => x.selected)
		none = state.counties.every(x => !x.selected)

		state.selected = all && !none
		state.indeterminate = ! (all || none);
	    },

	    state_changed: function(state) {
		state.indeterminate = false;
		_.forEach(state.counties, function(x) {
		    x.selected = state.selected
		})
	    },

	    county_changed: function(state, county) {
		this.recheck_state_selected_value(state);
	    },

	    locations_selected: function() {
		return this.accessible | 0;
	    },

	    events_selected: function() {
		return this.off_line + this.non_recurring;
	    },

	    search_fields: function() {
		return Boolean(this.initiative) + Boolean(this.person);
	    },

	    trigger_filter_update: function () {
		var new_filters = this.current_filters;
		window.localStorage['filters'] = JSON.stringify(new_filters);
		$.publish('filters.update', new_filters);
	    },

	    reset_filters: function () {
		this.set_all('all', false);
		this.close_modal();
	    },

	    set_all: function (type, value) {
		if (_.isUndefined(value) || !_.isBoolean(value)) {
		    value = true;
		}

		var set_all = _.isUndefined(type) || type === 'all';

		if (type === 'topics' || set_all) {
		    _.forEach(vm.topics, function (topic) {
			// set value of topic.selected
			topic.selected = value;

			// set subtopics
			_.forEach(topic.sub_topics, function (subtopic) {
			    subtopic.selected = value;
			});
		    });
		}

		if (type === 'counties' || set_all) {
		    // set counties
		    _.forEach(vm.states, function(state) {
			state.selected = value;
			_.forEach(state.counties, function(county) {
			    county.selected = value;
			});
		    });
		}

		if (type === 'all' && vm.region !== '') {
		    // just set the filter on the predefined state
		    _.forEach(vm.states, function(state) {
			new_val = state.name === vm.region;
			state.selected = new_val;
			_.forEach(state.counties, function(county) {
			    county.selected = new_val;
			});
		    });
		}

		if (type === 'strings' || set_all) {
		    this.initiative = '';
		    this.person = '';
		    this.accessible = false;
		    this.off_line = false;
		    this.non_recurring = false;
		}
	    },
	    close_modal: function () {
		this.trigger_filter_update();
		$('#filter-modal').modal('hide');
	    }
	},
	computed: {
	    selected_subtopic_ids: function () {
		var selected_subtopics = _.filter(this.by_id.sub_topics, function (sub_topic) {
		    return sub_topic.selected
		});
		return _.map(selected_subtopics, 'id');
	    },
	    selected_counties: function () {
		var selected_counties = [];
		_.forEach(this.states, function(state) {
		    var selection = _.filter(state.counties, function(county) {
			return county.selected
		    });
		    _.forEach(selection, function(county) {
			selected_counties.push(state.name+':'+county.name);
		    });
		});
		return selected_counties;
	    },
	    current_filters: function () {
		return {
		    counties: this.selected_counties,
		    topics: this.selected_subtopic_ids,
		    initiative: this.initiative,
		    person: this.person,
		    accessible: this.accessible,
		    off_line: this.off_line,
		    non_recurring: this.non_recurring,
		    number: this.topics_selected() +
			this.states_selected() +
			this.locations_selected() +
			this.events_selected() +
			this.search_fields()
		}
	    }
	}
    });

})(jQuery);

var filters = {};
if (!_.isUndefined(localStorage['filters'])) {
    filters = JSON.parse(localStorage['filters']);
}

set_filter_indicator(filters);

$.subscribe('filters.update', function(evt, filters) {
    set_filter_indicator(filters);
});

function set_filter_indicator(filters) {
    $("#filters-set").toggleClass("d-none", filters.number==0);
}
