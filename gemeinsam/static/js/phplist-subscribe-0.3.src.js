$(document).ready(function() {
    var waitimg = new Image();
    waitimg.src = waitImage;
    function showerror(url, msg="Leider ist ein Fehler bei der Übertragung aufgetreten.") {
	$("#phplistsubscriberesult").html('<div class="alert alert-danger"><strong>'+msg+'</strong><br/>Bitte probiere es noch einmal oder melde dich unter diesem <a href="'+url+'" target="_blank" rel="noopener noreferrer">link</a> an.');
    };

    $("#phplistsubscribeform").submit(function() {
	var emailaddress = $("#emailaddress").val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var subscribeaddress = this.action;
	var ajaxaddress = subscribeaddress.replace(/subscribe/,'asubscribe');
	$('#phplistsubscriberesult').html('<img src="'+waitimg.src+'" width="'+waitimg.width+'" height="'+waitimg.height+'" border="0" alt="Please wait" title="powered by phpList, www.phplist.com" />');

	if(emailReg.test(emailaddress)) {
	    var jqxhr = $.ajax({
		type: 'POST',
		url: ajaxaddress,
		crossDomain: true,
		data: "email="+emailaddress,
		success: function(data, textStatus, jqXHR ) {
		    if (data.search(/FAIL/) >= 0) {
			url = subscribeaddress+"&email="+emailaddress;
			showerror(url);
		    } else {
			$('#phplistsubscriberesult').html("<div id='subscribemessage'></div>");
			$('#subscribemessage').html(data)
			    .hide()
			    .fadeIn(1500);
			$("#phplistsubscribeform").hide();
			$("#phplistnotsubscribe").hide();
			$("#continuesetup").show().fadeIn(2000);
			document.cookie = "phplistsubscribed=yes";
		    }
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    showerror(subscribeaddress+"&email="+emailaddress);
		}
	    });
	} else {
	    showerror(subscribeaddress+"&email="+emailaddress, 'Dies ist keine gültige e-Mail Adresse!');
	}
	return false;
    });

    $("#emailaddress").val(pleaseEnter);
    $("#emailaddress").focus(function() {
	var v = $("#emailaddress").val();
	if (v == pleaseEnter) {
	    $("#emailaddress").val("")
	}
    });
    $("#emailaddress").blur(function() {
	var v = $("#emailaddress").val();
	if (v == "") {
	    $("#emailaddress").val(pleaseEnter)
	}
    });
    var cookie = document.cookie;
    if (cookie.indexOf('phplistsubscribed=yes')>=0) {
	$("#phplistsubscribeform").html(thanksForSubscribing);
	$("#phplistnotsubscribe").hide();
	$("#continuesetup").show().fadeIn(2000);
    }
    $("#phplistnotsubscribe").click(function() {
	$("#phplistsubscribeform").html('<h3>Not subscribed</h3>');
	$("#phplistnotsubscribe").hide();
	$("#continuesetup").show().fadeIn(2000);
    })

});

if (pleaseEnter == undefined) {
    var pleaseEnter = "E-mail Adresse";
}
if (thanksForSubscribing == undefined) {
    var thanksForSubscribing = '<div class="subscribed">Danke für die Anmeldung. Bitte überprüfe deine e-mails und bestätige die Anmeldung.</div>';
}
if (waitImage == undefined) {
    var waitImage = '/static/img/spinner.gif';
}
