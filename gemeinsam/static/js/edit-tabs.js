let tab = new URL(location.href).searchParams.get("tab") || ''
const currentTab = document.querySelector('button[data-bs-target="#' + tab + '"]');
const curTab = new bootstrap.Tab(currentTab);
curTab.show();
