"""gemeinsam URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Standard Library
import os

# Django stuff
from django.conf import settings

# from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin, flatpages
from django.urls import include, path
from django.views.generic import RedirectView, TemplateView
from django.views.i18n import JavaScriptCatalog

# Third party
import home_page

# from newsletter.models import Subscription
# from newsletter_subscription.backend import ModelBackend
# from newsletter_subscription.urls import newsletter_subscriptions_urlpatterns
from pictures.conf import get_settings

# First party
from common import views

urlpatterns = [
    # start-page
    path("", home_page.view),
    path("admin/", admin.site.urls),
    path("ckeditor5/", include("django_ckeditor_5.urls")),
    path("pages/", include("django.contrib.flatpages.urls")),
    path("accounts/", include("registration.backends.default.urls")),
    path("accounts/profile/", include("profiles.urls")),
    path("topic/", include("topic.urls")),
    path("initiative/", include("initiative.urls")),
    path("faq/", include("faq.urls")),
    # favicon/robots
    path(
        "favicon.ico",
        RedirectView.as_view(
            url=os.path.join(settings.STATIC_URL, "img", "favicon.ico"), permanent=True
        ),
    ),
    path(
        r"robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    # calendar
    path("event/", include("event.urls")),
    # locations
    path("location/", include("location.urls")),
    # search page
    path("search/", include("watson.urls")),
    path("about-us/", flatpages.views.flatpage, {"url": "/about-us/"}, name="about-us"),
    path(
        "participate/",
        flatpages.views.flatpage,
        {"url": "/participate/"},
        name="participate",
    ),
    path("filters/", views.filters),
    # captcha
    path("captcha/", include("captcha.urls")),
]

urlpatterns += [
    path("jsi18n/", JavaScriptCatalog.as_view(), name="javascript-catalog"),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if get_settings().USE_PLACEHOLDERS:
    urlpatterns += [
        path("_pictures/", include("pictures.urls")),
    ]
