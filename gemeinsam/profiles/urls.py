# Django stuff
from django.urls import path

# Local imports
from . import views

app_name = "profile"
urlpatterns = [
    path("contacts/", views.index, name="index"),
    path("<int:pk>/", views.detail, name="detail"),
    path("", views.myself, name="myself"),
    path("edit/", views.edit, name="edit"),
    path("delete/", views.delete, name="delete"),
    path("events.ics", views.EventFeed(), name="my-events"),
]
