# Django stuff
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db.models import Count, Q
from django.db.models import Value as V
from django.db.models.functions import Concat
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, reverse

# Third party
from django_ical.utils import build_rrule_from_recurrences_rrule
from django_ical.views import ICalFeed

# First party
from event.models import Event
from initiative.models import Initiative
from location.models import Location

# Local imports
from .forms import ProfileForm, UserForm
from .models import Profile


def index(request):
    template_name = "profiles/index.html"

    # depending on login-status of request user
    # create the queryset
    if request.user.is_authenticated:
        qs = User.objects.all()
    else:
        qs = User.objects.filter(profile__public=True)

    # remove users that have not provided neither name nor picture
    qs = qs.exclude(first_name="", last_name="", profile__picture="")
    qs = qs.filter(profile__picture__isnull=False)

    # sort by full name and move the people who did not provide a name to
    # the end of the list
    user_list = qs.annotate(
        order_name=Concat("first_name", "last_name", V("z"))
    ).order_by("order_name")

    # list of all initiatives that have somehow a matrix appearance
    level = request.user.is_authenticated and 1 or 2

    # get all users that have a matrix id
    matrix_users = User.objects.filter(
        ~Q(profile__matrix_id__isnull=True) & Q(profile__matrix_privacy__gte=level)
    )

    ini_query = Q(published=True) & (
        ~Q(matrix_room="")
        | Q(admin__in=matrix_users)
        | Q(collaborators__in=matrix_users)
    )

    initiative_list = (
        Initiative.objects.filter(ini_query)
        .distinct()
        .select_related("admin")
        .prefetch_related("collaborators")
    )

    communication_tool = getattr(settings, "COMMUNICATION_TOOL", {"base_url": "#"})

    return render(
        request,
        template_name,
        {
            "user_list": user_list,
            "initiative_list": initiative_list,
            "communication_tool": communication_tool,
        },
    )


@login_required
def myself(request):
    user = request.user

    my_initiatives = [
        ini
        for ini in Initiative.objects.all()
        if user.has_perm("initiative.change_initiative", ini)
        or user.has_perm("initiative.add_event", ini)
    ]

    my_events = [
        event
        for event in Event.objects.all().order_by("initiative__name", "-start_time")
        if user.has_perm("event.change_event", event)
    ]

    my_locations = [
        location
        for location in Location.objects.annotate(
            num_ini=Count("ini_loc", distinct=True),
            num_ev=Count("ev_loc", distinct=True),
        ).order_by("city", "name", "street")
        if user.has_perm("location.change_location", location)
    ]

    return render(
        request,
        "profiles/myself.html",
        {
            "initiative_list": my_initiatives,
            "event_list": my_events,
            "location_list": my_locations,
            "moderation_mail": getattr(
                settings, "MODERATION_EMAIL", "moderation@gemeinsam.jetzt"
            ),
            "communication_tool": getattr(
                settings, "COMMUNICATION_TOOL", {"base_url": "#"}
            ),
        },
    )


def detail(request, pk):
    # Django stuff
    from django.db.models import Q

    user = get_object_or_404(User, pk=pk)
    profile, created = Profile.objects.get_or_create(user=user)

    if not request.user.is_authenticated and not profile.public:
        raise PermissionDenied

    # calculate what can be seen
    show_mail = (
        request.user == user
        or profile.mail_privacy == 2
        or (profile.mail_privacy == 1 and request.user.is_authenticated)
    )
    show_matrix = (
        request.user == user
        or profile.matrix_privacy == 2
        or (profile.matrix_privacy == 1 and request.user.is_authenticated)
    )
    show_topics = (
        request.user == user
        or profile.topics_privacy == 2
        or (profile.topics_privacy == 1 and request.user.is_authenticated)
    )
    show_regions = (
        request.user == user
        or profile.regions_privacy == 2
        or (profile.regions_privacy == 1 and request.user.is_authenticated)
    )

    # get all published initiatives where at least one permission
    # is set to True for the requested user or he/she is admin
    query = Q(published=True)
    query &= Q(admin=user) | Q(permission__user=user)
    initiatives = Initiative.objects.filter(query).distinct()

    return render(
        request,
        "profiles/detail.html",
        {
            "profile": profile,
            "initiative_list": initiatives,
            "show_mail": show_mail,
            "show_matrix": show_matrix,
            "show_topics": show_topics,
            "show_regions": show_regions,
            "communication_tool": getattr(
                settings, "COMMUNICATION_TOOL", {"base_url": "#"}
            ),
        },
    )


@login_required
def edit(request):
    profile, created = Profile.objects.get_or_create(user=request.user)
    if request.method == "POST":
        uf = UserForm(data=request.POST, instance=request.user)
        pf = ProfileForm(data=request.POST, files=request.FILES, instance=profile)

        if uf.is_valid() and pf.is_valid() and pf.is_multipart():
            uf.save()
            profile = pf.save()
            return HttpResponseRedirect(
                reverse("profile:detail", args=[profile.user.pk])
            )

    else:
        uf = UserForm(instance=request.user)
        pf = ProfileForm(instance=profile)

    return render(
        request,
        "profiles/edit.html",
        {
            "user_form": uf,
            "profile_form": pf,
            "communication_url": getattr(
                settings, "COMMUNICATION_TOOL", {"base_url": "#"}
            )["base_url"],
        },
    )


@login_required
def delete(request):
    my_initiatives = Initiative.objects.filter(admin=request.user)
    if request.POST:
        user = request.user
        user.delete()
        return HttpResponseRedirect("/accounts/logout")

    return render(request, "profiles/delete.html", {"initiative_list": my_initiatives})


class EventFeed(ICalFeed):
    """
    the entire event calendar as ical feed
    """

    # Standard Library
    from urllib.parse import urlparse

    # Django stuff
    from django.conf import settings
    from django.utils.translation import gettext as _
    from django.views.decorators.csrf import csrf_exempt

    basic_auth_realm = urlparse(settings.SITE_URL).hostname

    product_id = "-//{}//{}//{}".format(
        basic_auth_realm, _("Veranstaltungen"), settings.LANGUAGE_CODE.split("-")[0]
    ).upper()
    timezone = settings.TIME_ZONE
    file_name = "gemeinsam-jetzt.ics"

    @csrf_exempt
    def __call__(self, request, *args, **kwargs):
        self.user = request.user
        if request.user.is_authenticated:
            return super(EventFeed, self).__call__(request, *args, **kwargs)

        if "HTTP_AUTHORIZATION" in request.META:
            auth = request.META["HTTP_AUTHORIZATION"].split()
            if len(auth) == 2:
                # only basic auth is supported
                if auth[0].lower() == "basic":
                    # Standard Library
                    import base64

                    # Django stuff
                    from django.contrib.auth import authenticate, login

                    uname, passwd = base64.b64decode(auth[1]).decode().split(":", 1)
                    user = authenticate(username=uname, password=passwd)
                    if user is not None:
                        if user.is_active:
                            login(request, user)
                            request.user = user
                            self.user = user
                            # import ipdb
                            # ipdb.set_trace()
                            return super(EventFeed, self).__call__(
                                request, *args, **kwargs
                            )

        # missing auth header of failed authentication results in 401
        response = HttpResponse()
        response.status_code = 401
        response["WWW-Authenticate"] = 'Basic realm="%s"' % self.basic_auth_realm
        return response

    def items(self):
        query = Q()
        topics = self.user.profile.topics.all()
        regions = tuple(self.user.profile.regions)
        if topics:
            query &= Q(topics__in=topics)
        if regions:
            query &= Q(location__county__in=regions) | Q(on_line=True)

        events = Event.objects.filter(query).distinct()
        return events

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        # Third party
        from bs4 import BeautifulSoup

        soup = BeautifulSoup(item.description, "html.parser")
        return soup.get_text("\n")

    def item_guid(self, item):
        return item.uuid

    def item_location(self, item):
        ret = ""
        if item.location:
            ret = item.location.address_formatted()
        elif item.on_line:
            ret = "on-line"
        return ret

    def item_geolocation(self, item):
        if item.location:
            return item.location.coords.coords[::-1]

    def item_class(self, item):
        return "PUBLIC"

    def item_link(self, item):
        return item.get_absolute_url()

    def item_organizer(self, item):
        # Third party
        from icalendar import vCalAddress, vText

        if item.initiative.mail:
            organizer = vCalAddress("MAILTO:{}".format(item.initiative.mail))
        else:
            organizer = vCalAddress(item.initiative)

        organizer.params["cn"] = vText(
            item.initiative.abbr and item.initiative.abbr or item.initiative.name
        )
        return organizer

    def item_start_datetime(self, item):
        return item.start_time

    def item_end_datetime(self, item):
        return item.end_time

    def item_categories(self, item):
        return [x.__str__() for x in item.topics.all()]

    def item_rrule(self, item):
        """Adapt Event recurrence to Feed Entry rrule."""
        if item.recurrences:
            rules = []
            for rule in item.recurrences.rrules:
                rules.append(build_rrule_from_recurrences_rrule(rule))
            return rules

    def item_exrule(self, item):
        """Adapt Event recurrence to Feed Entry exrule."""
        if item.recurrences:
            rules = []
            for rule in item.recurrences.exrules:
                rules.append(build_rrule_from_recurrences_rrule(rule))
            return rules

    def item_rdate(self, item):
        """Adapt Event recurrence to Feed Entry rdate."""
        if item.recurrences:
            return item.recurrences.rdates

    def item_exdate(self, item):
        """Adapt Event recurrence to Feed Entry exdate."""
        if item.recurrences:
            return item.recurrences.exdates
