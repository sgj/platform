# Django stuff
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

# Third party
from watson import search as watson


class ProfileAppConfig(AppConfig):
    name = "profiles"
    verbose_name = _("Profile")

    def ready(self):
        ProfileModel = self.get_model("Profile")
        watson.register(
            ProfileModel.objects.all(),
            fields=(
                "user__first_name",
                "user__last_name",
            ),
        )
