# -*- coding: utf-8 -*-


from django.db import migrations, models
import stdimage.models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='picture',
            field=stdimage.models.StdImageField(upload_to=b'profiles', null=True, verbose_name='Bild', blank=True),
        ),
    ]
