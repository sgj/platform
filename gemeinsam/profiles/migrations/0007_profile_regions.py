# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-10-26 10:31


from django.db import migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0006_auto_20170518_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='regions',
            field=multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('Bezirk Oberwart', 'Bezirk Oberwart'), ('G\xfcssing', 'G\xfcssing'), ('Wolfsberg', 'Wolfsberg'), ('Bezirk Bruck-M\xfcrzzuschlag', 'Bezirk Bruck-M\xfcrzzuschlag'), ('Bezirk Hartberg-F\xfcrstenfeld', 'Bezirk Hartberg-F\xfcrstenfeld'), ('Deutschlandsberg', 'Deutschlandsberg'), ('Expositur Gr\xf6bming', 'Expositur Gr\xf6bming'), ('Graz', 'Graz'), ('Graz-Umgebung', 'Graz-Umgebung'), ('Leibnitz', 'Leibnitz'), ('S\xfcdoststeiermark', 'S\xfcdoststeiermark'), ('Voitsberg', 'Voitsberg'), ('Weiz', 'Weiz'), ('Landstra\xdfe', 'Landstra\xdfe')], max_length=1000, null=True, verbose_name='Bezirke'),
        ),
    ]
