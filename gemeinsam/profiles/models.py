# -*- coding: utf-8 -*-

# Django stuff
from django.contrib.auth.models import User
from django.db import models
from django.utils.functional import lazy
from django.utils.translation import gettext_lazy as _

# Third party
from multiselectfield import MultiSelectField
from stdimage import StdImageField

# First party
from topic.models import SubTopic


def get_county_choices():
    # First party
    from location.models import location_states_counties_dict

    return [
        choice
        for sublist in [
            [tuple([y["name"]] * 2) for y in x["counties"]]
            for x in location_states_counties_dict()
        ]
        for choice in sublist
    ]


class Profile(models.Model):
    YES_NO = ((True, _("Ja")), (False, _("Nein")))
    PUBLIC_AUTH_NO = (
        (2, _("Öffentlich")),
        (1, _("Nur für angemeldete Benutzer*innen")),
        (0, _("Niemals anzeigen")),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    picture = StdImageField(
        _("Avatar"),
        upload_to="profiles",
        null=True,
        blank=True,
        variations={"thumbnail": (100, 100, True), "mini": (24, 24, True)},
    )

    topics = models.ManyToManyField(
        SubTopic, related_name="topic+", verbose_name=_("Themen"), blank=True
    )

    regions = MultiSelectField(
        verbose_name=_("Bezirke"), choices=(), max_length=1000, null=True, blank=True
    )

    matrix_id = models.CharField(_("Matrix-ID"), max_length=50, null=True, blank=True)

    public = models.BooleanField(
        verbose_name=_("Öffentlich"),
        help_text=_("Teile des Profils sind öffentlich"),
        choices=YES_NO,
        default=False,
        blank=False,
    )

    slogan = models.CharField(
        _("Meine Botschaft"),
        max_length=160,
        null=True,
        blank=True,
        help_text=_("Meine Botschaft, mein Wahlspruch, Slogan, ... (max. 160 Zeichen)"),
    )

    # privacy
    mail_privacy = models.SmallIntegerField(
        verbose_name=_("Meine E-mail Adresse anzeigen"),
        choices=PUBLIC_AUTH_NO,
        default=1,
    )

    matrix_privacy = models.SmallIntegerField(
        verbose_name=_("Meine [matrix]-ID anzeigen"), choices=PUBLIC_AUTH_NO, default=1
    )

    topics_privacy = models.SmallIntegerField(
        verbose_name=_("Meine thematischen Interessen anzeigen"),
        choices=PUBLIC_AUTH_NO,
        default=1,
    )

    regions_privacy = models.SmallIntegerField(
        verbose_name=_("Meine Bezirke anzeigen"), choices=PUBLIC_AUTH_NO, default=1
    )

    class Meta:
        verbose_name = "Benutzerprofil"
        verbose_name_plural = "Benutzerprofile"

    def __init__(self, *args, **kwargs):
        super(Profile, self).__init__(*args, **kwargs)
        self._meta.get_field("regions").choices = lazy(get_county_choices, list)()

    def __str__(self):
        return self.user.get_full_name()

    def get_absolute_url(self):
        # Django stuff
        from django.urls import reverse

        return reverse("profile:detail", args=[str(self.user.pk)])
