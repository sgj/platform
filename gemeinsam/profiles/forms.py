# -*- coding: utf-8 -*-

# Django stuff
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

# First party
from location.models import location_states_counties_dict
from topic.models import topics_as_choices

# Local imports
from .models import Profile


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["username", "email", "first_name", "last_name"]

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields["email"].required = True
        for field in self.fields:
            self.fields[field].widget.attrs.update(
                {
                    "class": "form-control",
                    "placeholder": self.fields[field].help_text
                    and self.fields[field].help_text
                    or self.fields[field].label,
                }
            )


class ProfileForm(forms.ModelForm):
    deletion = forms.BooleanField(
        required=False, label=_("Unwiderruflich löschen - ich weiss, was ich tue")
    )

    class Meta:
        model = Profile
        fields = [
            "picture",
            "topics",
            "regions",
            "public",
            "matrix_id",
            "mail_privacy",
            "matrix_privacy",
            "topics_privacy",
            "regions_privacy",
            "slogan",
        ]
        widgets = {"regions": forms.SelectMultiple()}

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field not in ["deletion"]:
                self.fields[field].widget.attrs.update(
                    {
                        "class": "form-control",
                        "placeholder": self.fields[field].help_text
                        and self.fields[field].help_text
                        or self.fields[field].label,
                    }
                )

        self.fields["public"].initial = False

        # dealing with subtopic / topic optgroups
        self.fields["topics"].choices = topics_as_choices()
        self.fields["topics"].widget.attrs.update(
            {"class": "selectpicker form-control", "multiple": "", "title": _("Themen")}
        )

        # counties
        self.fields["regions"].choices = [
            [x["name"], [[y["name"]] * 2 for y in x["counties"]]]
            for x in location_states_counties_dict()
        ]
        self.fields["regions"].widget.attrs.update(
            {
                "class": "selectpicker form-control",
                "multiple": "",
                "title": _("Alle Bezirke"),
            }
        )
