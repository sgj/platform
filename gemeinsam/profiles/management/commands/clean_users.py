# -*- coding: utf-8 -*-
"""Management command to remove unused accounts."""

# Django stuff
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

# First party
from initiative.models import Initiative, Permission


class Command(BaseCommand):
    help = "Correct / unify counties"

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="test",
            default=False,
            help="do not actually delete anything",
        )

    def handle(self, *args, **options):
        # get users that are excluded from all operations
        excluded = set(Initiative.objects.values_list("admin", flat=True))
        excluded.update(
            set(
                Permission.objects.order_by("user")
                .values_list("user", flat=True)
                .distinct("user")
            )
        )

        # get all accounts that never logged in
        users = User.objects.filter(last_login=None).exclude(id__in=excluded)
        self.stdout.write(f"Remove {users.count()} accounts that never logged in.")
        if not options["test"]:
            users.delete()
