# Django stuff
from django.conf import settings
from django.template import Library

# Third party
from profiles.models import Profile

# First party
from initiative.models import Initiative

register = Library()

communication_tool = getattr(settings, "COMMUNICATION_TOOL", {"base_url": "#"})


@register.inclusion_tag("profiles/user-initiatives.html")
def user_initiatives(user):
    # Django stuff
    from django.db.models import Q

    query = Q(published=True)
    query &= Q(admin=user) | Q(permission__user=user)
    initiatives = Initiative.objects.filter(query).distinct()

    return {"initiative_list": initiatives}


@register.inclusion_tag("profiles/user-contact.html")
def user_contact(user, request_user):
    profile, created = Profile.objects.get_or_create(user=user)

    ret = {"communication_tool": communication_tool}

    if (
        request_user == user
        or profile.mail_privacy == 2
        or (profile.mail_privacy == 1 and request_user.is_authenticated)
    ):
        ret.update({"email": user.email})

    if (
        request_user == user
        or profile.matrix_privacy == 2
        or (profile.matrix_privacy == 1 and request_user.is_authenticated)
    ):
        ret.update({"matrix_id": profile.matrix_id})

    return ret
