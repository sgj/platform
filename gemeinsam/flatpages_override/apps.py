# Django stuff
from django.apps import AppConfig


class FlatpageOverrideConfig(AppConfig):
    name = "flatpages_override"
