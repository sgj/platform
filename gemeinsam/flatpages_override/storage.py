# Standard Library
import datetime

from pathlib import Path
from urllib.parse import urljoin

# Django stuff
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class FlatPageMediaStorage(FileSystemStorage):
    """Custom storage for flatpage media uploaded through CKEditor5."""

    today = datetime.date.today()
    location = Path(settings.MEDIA_ROOT).joinpath(
        "uploads", f"{today:%Y}", f"{today:%m}", f"{today:%d}"
    )
    base_url = urljoin(settings.MEDIA_URL, f"uploads/{today:%Y/%m/%d}/")
