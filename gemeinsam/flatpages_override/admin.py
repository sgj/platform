# Django stuff
from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld
from django.contrib.flatpages.models import FlatPage

# Third party
from django_ckeditor_5.widgets import CKEditor5Widget


class FlatpageForm(FlatpageFormOld):
    class Meta:
        model = FlatPage
        fields = "__all__"
        widgets = {
            "content": CKEditor5Widget(config_name="internal"),
        }


class FlatPageAdmin(FlatPageAdminOld):
    class Media:
        css = {"all": ("css/ckeditor.css",)}

    form = FlatpageForm


# We have to unregister the normal admin, and then reregister ours
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
