# Django stuff
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AppearenceConfig(AppConfig):
    name = "appearance"
    verbose_name = _("Erscheinungsbild")
