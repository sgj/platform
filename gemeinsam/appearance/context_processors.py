# Django stuff
from django.contrib.sites.shortcuts import get_current_site

# Local imports
from .models import Basic, Menu


def appearance(request):
    site = get_current_site(request)
    return {
        "appearance": Basic.objects.get_or_create(site=site)[0],
        "menu_above": Menu.objects.get_or_create(site=site, position=1)[0],
        "menu_below": Menu.objects.get_or_create(site=site, position=2)[0],
    }
