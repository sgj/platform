# Generated by Django 4.2.13 on 2024-08-09 16:18

# Django stuff
from django.db import migrations

# Third party
import stdimage.models


class Migration(migrations.Migration):
    dependencies = [
        ("appearance", "0008_alter_menuentry_text"),
    ]

    operations = [
        migrations.AlterField(
            model_name="basic",
            name="favicon",
            field=stdimage.models.StdImageField(
                blank=True,
                force_min_size=False,
                help_text="Favicon der Webseite",
                null=True,
                upload_to="appearance",
                variations={"favicon": (64, 64, True)},
                verbose_name="Favicon",
            ),
        ),
        migrations.AlterField(
            model_name="basic",
            name="logo",
            field=stdimage.models.StdImageField(
                blank=True,
                force_min_size=False,
                help_text="Logo der Webseite",
                null=True,
                upload_to="appearance",
                variations={"favicon": (64, 64, True), "standard": (640, 480)},
                verbose_name="Logo",
            ),
        ),
        migrations.AlterField(
            model_name="basic",
            name="start_page_image",
            field=stdimage.models.StdImageField(
                blank=True,
                force_min_size=False,
                help_text="Bild auf Startseite rechts unten",
                null=True,
                upload_to="appearance",
                variations={"standard": (640, 480)},
                verbose_name="Aufmacher",
            ),
        ),
        migrations.AlterField(
            model_name="basic",
            name="start_page_special_link_image",
            field=stdimage.models.StdImageField(
                blank=True,
                force_min_size=False,
                help_text="Bild für speziellen externen link auf Startseite rechts unten",
                null=True,
                upload_to="appearance",
                variations={"standard": (640, 480)},
                verbose_name="Bild für Spezial-link",
            ),
        ),
    ]
