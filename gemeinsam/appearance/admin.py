# -*- encoding: utf-8 -*-
# Django stuff
from django import forms
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

# Local imports
from .models import Basic, Menu, MenuEntry


class BasicAdminForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super(BasicAdminForm, self).clean()
        url = cleaned_data.get("start_page_special_link_url")
        img = cleaned_data.get("start_page_special_link_image")
        if url and not img:
            self.add_error("start_page_special_link_url", "Bild muss angeben werden")


@admin.register(Basic)
class BasicAdmin(admin.ModelAdmin):
    form = BasicAdminForm
    list_display = ("site", "title")
    list_display_links = ("site",)

    fieldsets = (
        (_("Allgemein"), {"fields": ("site", "title", "logo", "favicon")}),
        (_("Startseite"), {"fields": ("motto", "start_page_image")}),
        (
            _("Spezial-link auf Startseite"),
            {
                "fields": (
                    "start_page_special_link_image",
                    "start_page_special_link_url",
                )
            },
        ),
    )


class MenuEntryInline(admin.TabularInline):
    model = MenuEntry


@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = ("site", "position", "get_number")
    list_display_links = ("site",)

    inlines = [
        MenuEntryInline,
    ]

    def get_number(self, obj):
        return obj.menuentry_set.count()

    get_number.short_description = _("Einträge")
