# -*- encoding: utf-8 -*-

# Django stuff
from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import gettext_lazy as _

# Third party
from stdimage import StdImageField


class Basic(models.Model):
    site = models.OneToOneField(
        Site, primary_key=True, verbose_name=_("Webseite"), on_delete=models.CASCADE
    )
    title = models.CharField(
        _("Titel"), max_length=100, blank=True, help_text=_("Titel der Webseite")
    )
    logo = StdImageField(
        _("Logo"),
        upload_to="appearance",
        null=True,
        blank=True,
        variations={"standard": (640, 480), "favicon": (64, 64, True)},
        help_text=_("Logo der Webseite"),
    )
    favicon = StdImageField(
        _("Favicon"),
        upload_to="appearance",
        null=True,
        blank=True,
        variations={"favicon": (64, 64, True)},
        help_text=_("Favicon der Webseite"),
    )
    motto = models.CharField(
        _("Motto"),
        max_length=200,
        blank=True,
        help_text=_("Motto, welches auf der Startseite angezeigt wird"),
    )
    start_page_image = StdImageField(
        _("Aufmacher"),
        upload_to="appearance",
        null=True,
        blank=True,
        variations={"standard": (640, 480)},
        help_text=_("Bild auf Startseite rechts unten"),
    )
    start_page_special_link_image = StdImageField(
        _("Bild für Spezial-link"),
        upload_to="appearance",
        null=True,
        blank=True,
        variations={"standard": (640, 480)},
        help_text=_("Bild für speziellen externen link auf Startseite rechts unten"),
    )
    start_page_special_link_url = models.URLField(_("URL Spezial-link"), blank=True)
    update = models.DateField(_("Zuletzt aktualisiert"), auto_now=True)

    def __str__(self):
        return "{} {}".format(_("Basiseinstellungen für"), self.site)


class Menu(models.Model):
    MENU_POSITION = ((1, _("Fußzeile oben")), (2, _("Fußzeile unten")))
    site = models.ForeignKey(Site, verbose_name=_("Webseite"), on_delete=models.CASCADE)
    position = models.SmallIntegerField(_("Menü"), choices=MENU_POSITION, default=1)

    class Meta:
        unique_together = ("site", "position")

    def __str__(self):
        return "{} {} ({})".format(_("Menü in"), self.get_position_display(), self.site)


class MenuEntry(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    text = models.CharField(_("Text"), max_length=100, help_text=_("Angezeigter Text"))
    link = models.CharField(
        _("Url"),
        max_length=200,
        blank=True,
        help_text=_(
            "Links mit Protokoll öffnen in einem neuen Reiter, "
            "ohne Protokoll im gleichen. Wenn dieses Feld leer "
            "bleibt, wird nur der Text angezeigt"
        ),
    )
    order = models.SmallIntegerField(
        _("Reihung"),
        default=0,
        help_text=_(
            "Ordnung der einzelnen Menüpunkte: "
            "Negative Zahlen linkes Menu, positive Zahlen rechtes Menü"
        ),
    )

    class Meta:
        ordering = ["menu", "order"]

    def __str__(self):
        if self.link:
            return "{} ({})".format(self.text, self.link)
        else:
            return self.text
