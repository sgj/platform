# -*- coding: utf-8 -*-
# Standard Library
import re

# Django stuff
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

# Third party
from django_ckeditor_5.widgets import CKEditor5Widget

# First party
from common.utils import add_link_target_blank
from location.models import locations_as_choices
from topic.models import topics_as_choices

# Local imports
from .models import Initiative, Picture


class PictureForm(forms.ModelForm):
    class Meta:
        model = Picture
        fields = ("title", "photo")

    def __init__(self, *args, **kwargs):
        super(PictureForm, self).__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({"class": "form-control"})


class InitiativeForm(forms.ModelForm):
    deletion = forms.BooleanField(required=False, label=_("Unwiderruflich löschen"))

    error_css_class = "is-invalid"

    class Meta:
        model = Initiative
        # fields = '__all__'
        exclude = [
            "admin",
            "background",
            "coords",
            "slug",
            "published",
            "reason",
            "update",
            "collaborators",
            "moderation",
        ]

        widgets = {
            "aim": CKEditor5Widget(),
            "activity": CKEditor5Widget(),
            "contribution": CKEditor5Widget(),
            "note": CKEditor5Widget(),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super(InitiativeForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field not in list(self._meta.widgets.keys()) + ["deletion"]:
                self.fields[field].widget.attrs.update(
                    {
                        "class": "form-control",
                        "placeholder": self.fields[field].help_text
                        and self.fields[field].help_text
                        or self.fields[field].label,
                    }
                )

        # email field attribute
        self.fields["mail"].widget.attrs.update({"aria-describedby": "email-addon"})

        # matrix_room field attribute
        self.fields["matrix_room"].widget.attrs.update(
            {"placeholder": "#room:gemeinsam.jetzt"}
        )

        # dealing with locations
        try:
            id = getattr(self.instance.location, "id", None)
        except Initiative.location.RelatedObjectDoesNotExist:
            id = None

        locs = locations_as_choices(
            user=self.user,
            id=id,
            restriction=True,
        ) + [[_("Neuer Ort"), [[0, _("Auf Karte wählen")]]]]
        self.fields["location"].choices = locs
        self.fields["location"].widget.attrs.update(
            {
                "class": "selectpicker form-control",
                "data-live-search": "true",
            }
        )

        # dealing with subtopic / topic optgroups
        self.fields["topics"].choices = topics_as_choices()
        self.fields["topics"].widget.attrs.update(
            {
                "class": "selectpicker form-control",
                "multiple": "",
                "data-live-search": "true",
                "data-live-search-placeholder": _("Suche ..."),
                "data-max-options": "3",
                "data-max-options-text": _("Maximum von 3 Themen erreicht"),
                "data-title": _("Wähle bis zu 3 Themen"),
            }
        )

    def clean_matrix_room(self):
        data = self.cleaned_data.get("matrix_room", "")
        if data:
            pattern = re.compile(
                r"^#[a-zA-Z0-9-+_]+:"
                r"(?:[a-zA-Z0-9]"
                r"(?:[a-zA-Z0-9-_]{0,61}[A-Za-z0-9])?\.)"
                r"+[A-Za-z0-9][A-Za-z0-9-_]{0,61}"
                r"[A-Za-z]$"
            )
            if not pattern.match(data):
                raise ValidationError(
                    _("Bitte einen gültigen [matrix]-Raumnamen eingeben.")
                )
        return data

    def clean_aim(self):
        data = self.cleaned_data.get("aim", "")
        return add_link_target_blank(data)

    def clean_activity(self):
        data = self.cleaned_data.get("activity", "")
        return add_link_target_blank(data)

    def clean_contribution(self):
        data = self.cleaned_data.get("contribution", "")
        return add_link_target_blank(data)

    def clean_note(self):
        data = self.cleaned_data.get("note", "")
        return add_link_target_blank(data)
