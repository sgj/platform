
ad i18n %}
{% trans 'Hallo' %} {{ initiative.admin.first_name|default:initiative.admin.username }},

{% trans 'ein/e neue/r BenutzerIn möchte auf' %}
{{ site.name }} {% trans 'deiner Initiative' %} '{{ initiative }}' {% trans 'beitreten' %}.

{% trans "Wer" %}: {{ who.get_full_name|default:who.username }} {% if who.get_full_name %}({{ who }}){% endif %}

{% trans "Link zur Rechteverwaltung deiner Initiative" %}:
https://{{site.domain}}/initiative/permissions/{{ initiative.id }}

{% blocktrans with initiative.name as ini %}
Dort kannst du die Rechte der Mitglieder deiner Initiative verwalten.

Du kannst zum Beispiel festlegen, wer den Steckbrief von {{ ini }}
bearbeiten darf oder Termine deiner Initiative eintragen kann.

Bitte ignoriere diese mail nicht. Solange du die Rechte nicht festlegst,
wirst du in regelmäßigen Abständen automatisch daran erinnert.
{% endblocktrans %}

{% trans 'Dein' %} {{ site.name }} Team
