{% load i18n %}
{% blocktrans with name=initiative.admin.get_full_name|default:initiative.admin.username %}
Hallo {{ name }},


BITTE GIB UNS EIN SIGNAL

- Logge dich in DEIN Konto ein und gib den engagierten Menschen
  und der Plattform ein Update von deiner Initiative und Aktivitäten!

- Sollte sich nichts am Steckbrief oder den Terminen geändert
  haben, dann klicke innerhalb der nächsten 10 Tage auf den
  folgenden Link. Damit wird deine Initiative automatisch
  aktualisiert und wir, die Menschen und die Datenbank wissen:
  „Alles ist aktuell!“
{% endblocktrans %}
  https://{{ site.domain }}{% url 'initiative:update' token %}

{% blocktrans %}
SO EINFACH GEHT DAS

Bitte aktualisiere deine Initiative auf
{% endblocktrans %}
https://{{ site.domain }}{% url 'initiative:edit' initiative.pk %}
{% blocktrans %}
Bitte vergiss nicht, den Speichern-Button zu klicken.


WARUM ERHÄLTST DU DIESE MAIL

Du hast deine Initiative
{% endblocktrans %}

{{ initiative }}

{% blocktrans with site_name=site.name last_update=initiative.update|date:"Y-m-d" %}
zum letzten Mal am {{ last_update }} bearbeitet. Dies ist
bereits {{ days_update }} Tage her.


AN WEN KANNST DU DICH BEI FRAGEN WENDEN

Falls du irgendwelche Fragen dazu hast, wende dich einfach
an die Moderation, indem du auf diese mail antwortest.

Dein {{ site_name }} Team
{% endblocktrans %}
