# -*- coding: utf-8 -*-


# Django stuff
from django.core.exceptions import ObjectDoesNotExist

# Third party
from rules import add_perm, is_group_member, predicate


@predicate
def is_initiative_admin(user, initiative):
    return user == initiative.admin


is_moderator = is_group_member("Moderation")


def editor_factory(app):
    """
    factory function that creates predicates for the apps
    """

    @predicate
    def app_editor(user, initiative):
        try:
            initiative.permission_set.get(user=user, module=app)
            return True
        except ObjectDoesNotExist:
            return False

    return app_editor


initiative_editor = editor_factory(1)
calendar_editor = editor_factory(2)


add_perm("initiative", is_moderator)
add_perm("initiative.add_event", is_moderator | is_initiative_admin | calendar_editor)
add_perm("initiative.delete_initiative", is_moderator | is_initiative_admin)
add_perm(
    "initiative.change_initiative",
    is_moderator | is_initiative_admin | initiative_editor,
)
add_perm("initiative.add_permission", is_moderator)
add_perm("initiative.change_permission", is_moderator)
add_perm("initiative.delete_permission", is_moderator)
add_perm("initiative.add_picture", is_moderator)
add_perm("initiative.change_picture", is_moderator)
add_perm("initiative.delete_picture", is_moderator)
add_perm("initiative.perms_initiative", is_moderator | is_initiative_admin)
