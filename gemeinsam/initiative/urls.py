# Django stuff
from django.urls import path

# Local imports
from . import views
from .models import Initiative

app_name = "initiative"
urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("line/", views.IndexView.as_view(), name="index-line"),
    path("bycounty/", views.IndexViewByCounty.as_view(), name="bycounty"),
    path("bycounty/line", views.IndexViewByCounty.as_view(), name="bycounty-line"),
    path("bytopic/", views.IndexViewByTopic.as_view(), name="bytopic"),
    path("bytopic/line", views.IndexViewByTopic.as_view(), name="bytopic-line"),
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("add/", views.edit, name="add"),
    path("edit/<int:id>/", views.edit, name="edit"),
    path("update/<str:token>/", views.update, name="update"),
    path(
        "picture-upload/<int:ini>/",
        views.PictureUploadView.as_view(),
        name="picture-upload",
    ),
    path("picture-delete/", views.PictureUploadView.as_view(), name="picture-delete"),
    path("user_list/<int:id>/", views.user_list, name="user_list"),
    path("map/<int:pk>", views.map, name="map-zoom"),
    path("map/", views.map, name="map"),
    path(
        "data.geojson",
        views.InitiativeMapView.as_view(
            model=Initiative,
            properties=("popup_content", "background"),
            geometry_field="coords",
        ),
        name="data",
    ),
]
