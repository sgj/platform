# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0020_auto_20160129_1122'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='permission',
            options={'ordering': ['initiative', 'user', 'module']},
        ),
    ]
