# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('initiative', '0014_auto_20151221_1750'),
    ]

    operations = [
        migrations.CreateModel(
            name='Permission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_joined', models.DateField(auto_now_add=True, verbose_name='Beigetreten am')),
                ('can_edit', models.ManyToManyField(related_name='module', to='contenttypes.ContentType')),
            ],
        ),
        migrations.AlterField(
            model_name='initiative',
            name='background',
            field=models.SmallIntegerField(default=None, choices=[(None, 'unmoderiert'), (1, '\xf6ffentliche Einrichtung'), (2, 'konfessionell'), (3, 'partiepolitisch'), (4, 'gewinnorientiert')], blank=True, help_text='Hintergrund der Initiative (moderierter Status)', null=True, verbose_name='Hintergrund'),
        ),
        migrations.AddField(
            model_name='permission',
            name='initiative',
            field=models.ForeignKey(to='initiative.Initiative', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='permission',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
    ]
