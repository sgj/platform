# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0024_initiative_create'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='legal_form',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Rechtsform', choices=[(None, b''), (1, 'Verein'), (2, 'Unternehmen'), (3, '\xd6ffentliche Einrichtung'), (4, 'Genossenschaft')]),
        ),
    ]
