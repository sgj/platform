# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0028_auto_20160910_2322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='background',
            field=models.SmallIntegerField(default=None, choices=[(None, 'unmoderiert'), (1, '\xf6ffentliche Einrichtung'), (2, 'konfessionell'), (3, 'parteipolitisch'), (4, 'gewerblich')], blank=True, help_text='Hintergrund der Initiative (moderierter Status)', null=True, verbose_name='Hintergrund'),
        ),
    ]
