# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('initiative', '0015_auto_20151227_1741'),
    ]

    operations = [
        migrations.AddField(
            model_name='initiative',
            name='collaborators',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='MitarbeiterInnen', through='initiative.Permission'),
        ),
    ]
