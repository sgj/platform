# -*- coding: utf-8 -*-


from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Initiative',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Name der Initiative', max_length=100, verbose_name='Name')),
                ('abbr', models.CharField(help_text='Abk\xfcrzung der Initiative', max_length=10, verbose_name='Abk\xfcrzung', blank=True)),
                ('address', models.CharField(help_text='Je genauer die Angabe der Adresse, desto leichter lassen sich die geografischen Koordinaten zur Kartenanzeige finden', max_length=100, verbose_name='Adresse', blank=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, verbose_name='Geographische Koordinaten', blank=True)),
            ],
        ),
    ]
