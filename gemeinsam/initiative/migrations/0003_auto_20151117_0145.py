# -*- coding: utf-8 -*-


from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0002_auto_20151117_0136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, verbose_name='Geographische Koordinaten', blank=True),
        ),
    ]
