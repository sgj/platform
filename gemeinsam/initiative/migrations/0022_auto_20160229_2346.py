# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0021_auto_20160130_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='topics',
            field=models.ManyToManyField(related_name='topic', verbose_name='Themen', to='topic.SubTopic'),
        ),
    ]
