# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0026_auto_20160603_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='legal_form',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Organisationsform', choices=[(None, 'Nicht angegeben'), (1, 'Verein'), (2, 'Unternehmen'), (3, '\xd6ffentliche Einrichtung'), (4, 'Genossenschaft'), (5, 'B\xfcndnis')]),
        ),
    ]
