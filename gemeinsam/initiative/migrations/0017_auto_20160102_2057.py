# -*- coding: utf-8 -*-


from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0016_initiative_collaborators'),
    ]

    operations = [
        migrations.AddField(
            model_name='permission',
            name='module',
            field=models.SmallIntegerField(default=1, choices=[(1, 'Steckbrief'), (2, 'Kalender')]),
        ),
        migrations.AddField(
            model_name='permission',
            name='permission',
            field=models.NullBooleanField(default=None),
        ),
        migrations.AddField(
            model_name='permission',
            name='updated',
            field=models.DateField(default=datetime.datetime(2016, 1, 2, 19, 57, 42, 469367, tzinfo=utc), verbose_name='Aktualiert am', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='permission',
            unique_together=set([('user', 'initiative', 'module')]),
        ),
        migrations.RemoveField(
            model_name='permission',
            name='can_edit',
        ),
        migrations.RemoveField(
            model_name='permission',
            name='date_joined',
        ),
    ]
