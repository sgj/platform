# -*- coding: utf-8 -*-


from django.db import migrations, models
import datetime
import stdimage.models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0013_auto_20151215_0907'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='initiative',
            name='trust',
        ),
        migrations.AddField(
            model_name='initiative',
            name='background',
            field=models.SmallIntegerField(default=None, choices=[(None, b''), (1, '\xf6ffentliche Einrichtung'), (2, 'konfessionell'), (3, 'partiepolitisch'), (4, 'gewinnorientiert')], blank=True, help_text='Hintergrund der Initiative (moderierter Status)', null=True, verbose_name='Hintergrund'),
        ),
        migrations.AddField(
            model_name='initiative',
            name='update',
            field=models.DateField(default=datetime.datetime(2015, 12, 21, 16, 50, 14, 355756, tzinfo=utc), verbose_name='Zuletzt aktualisiert', auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='initiative',
            name='legal_form',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Rechtsform', choices=[(None, b''), (1, 'Verein'), (2, 'Unternehmen'), (3, '\xd6ffentliche Einrichtung')]),
        ),
        migrations.AlterField(
            model_name='picture',
            name='photo',
            field=stdimage.models.StdImageField(upload_to=b'initiative/pictures', verbose_name='Bilddatei'),
        ),
    ]
