# -*- coding: utf-8 -*-


from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0023_auto_20160229_2348'),
    ]

    operations = [
        migrations.AddField(
            model_name='initiative',
            name='create',
            field=models.DateField(default=datetime.datetime(2016, 5, 3, 9, 11, 38, 881212, tzinfo=utc), verbose_name='Angelegt', auto_now_add=True),
            preserve_default=False,
        ),
    ]
