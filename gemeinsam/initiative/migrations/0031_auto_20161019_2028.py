# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0030_initiative_moderation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='background',
            field=models.SmallIntegerField(default=None, choices=[(None, '----'), (1, '\xf6ffentliche Einrichtung'), (2, 'konfessionell'), (3, 'parteipolitisch'), (4, 'gewerblich')], blank=True, help_text='Hintergrund der Initiative (moderierter Status)', null=True, verbose_name='Hintergrund'),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='moderation',
            field=models.BooleanField(default=False, help_text='Moderation von Veranstaltungen, ...', verbose_name='Moderation', choices=[(True, 'Ja'), (False, 'Nein')]),
        ),
    ]
