# -*- coding: utf-8 -*-


from django.db import migrations, models
import stdimage.models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0012_picture_initiative'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='logo',
            field=stdimage.models.StdImageField(upload_to=b'initiative/logos', null=True, verbose_name='Logo der Initiative', blank=True),
        ),
        migrations.AlterField(
            model_name='picture',
            name='title',
            field=models.CharField(max_length=100, verbose_name='Bildunterschrift', blank=True),
        ),
    ]
