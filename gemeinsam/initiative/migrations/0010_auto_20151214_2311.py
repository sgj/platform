# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0009_auto_20151205_2013'),
    ]

    operations = [
        migrations.AddField(
            model_name='initiative',
            name='published',
            field=models.BooleanField(default=False, help_text='Steckbrief \xf6ffentlich zug\xe4nglich', verbose_name='Ver\xf6ffentlicht', choices=[(True, 'Ja'), (False, 'Nein')]),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='address',
            field=models.CharField(max_length=100, verbose_name='Adresse', blank=True),
        ),
    ]
