# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0029_auto_20161007_2226'),
    ]

    operations = [
        migrations.AddField(
            model_name='initiative',
            name='moderation',
            field=models.BooleanField(default=True, help_text='Moderation von Veranstaltungen, ...', verbose_name='Moderation', choices=[(True, 'Ja'), (False, 'Nein')]),
        ),
    ]
