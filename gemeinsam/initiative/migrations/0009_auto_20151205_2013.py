# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0008_auto_20151117_1654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='topics',
            field=models.ManyToManyField(related_name='_topics_+', verbose_name='Themen', to='topic.SubTopic'),
        ),
    ]
