# -*- coding: utf-8 -*-


from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3, null=True, verbose_name='Geographische Koordinaten', blank=True),
        ),
    ]
