# -*- coding: utf-8 -*-


from django.db import migrations, models
import stdimage.models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0010_auto_20151214_2311'),
    ]

    operations = [
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='Unterschrift', blank=True)),
                ('photo', stdimage.models.StdImageField(upload_to=b'initiative', verbose_name='Bilddatei')),
                ('order', models.SmallIntegerField(default=0, verbose_name='Sortierung')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': 'Foto',
                'verbose_name_plural': 'Fotos',
            },
        ),
    ]
