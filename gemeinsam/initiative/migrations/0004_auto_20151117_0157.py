# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0003_auto_20151117_0145'),
    ]

    operations = [
        migrations.AddField(
            model_name='initiative',
            name='city',
            field=models.CharField(max_length=50, verbose_name='Ort', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='street',
            field=models.CharField(max_length=100, verbose_name='Stra\xdfe', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='zipcode',
            field=models.CharField(max_length=10, verbose_name='PLZ', blank=True),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='address',
            field=models.CharField(max_length=200, verbose_name='Adresse', blank=True),
        ),
    ]
