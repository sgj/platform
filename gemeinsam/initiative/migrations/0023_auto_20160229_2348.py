# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0022_auto_20160229_2346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='topics',
            field=models.ManyToManyField(to='topic.SubTopic', verbose_name='Themen'),
        ),
    ]
