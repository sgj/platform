# -*- coding: utf-8 -*-


from django.db import migrations, models
import django_bleach.models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0027_auto_20160910_2254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='activity',
            field=django_bleach.models.BleachField(help_text='Wie wir dorthin kommen wollen', verbose_name='Aktivit\xe4ten'),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='aim',
            field=django_bleach.models.BleachField(help_text='Was wir erreichen wollen', verbose_name='Ziel'),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='contribution',
            field=django_bleach.models.BleachField(help_text='Wie du dich einbringen kannst', verbose_name='Beteiligung'),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='note',
            field=django_bleach.models.BleachField(help_text='Was wir sonst noch sagen wollen', verbose_name='Anmerkungen', blank=True),
        ),
    ]
