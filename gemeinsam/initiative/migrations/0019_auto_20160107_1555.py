# -*- coding: utf-8 -*-


from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0018_auto_20160104_2005'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='initiative',
            name='address',
        ),
        migrations.AlterField(
            model_name='initiative',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(default='POINT(15.4333 47.0667)', srid=4326, verbose_name='Geographische Koordinaten'),
            preserve_default=False,
        ),
    ]
