# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('topic', '0003_auto_20151117_1540'),
        ('initiative', '0006_initiative_county'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='initiative',
            options={'verbose_name': 'Initiative', 'verbose_name_plural': 'Initiativen'},
        ),
        migrations.AddField(
            model_name='initiative',
            name='activity',
            field=models.TextField(default='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', help_text='Wie wir dorthin kommen wollen', verbose_name='Aktivit\xe4ten'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='initiative',
            name='admin',
            field=models.ForeignKey(related_name='admin+', on_delete=models.PROTECT, default=1, to=settings.AUTH_USER_MODEL, help_text='Administrator der Initiative'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='initiative',
            name='aim',
            field=models.TextField(default='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', help_text='Was wir erreichen wollen', verbose_name='Ziel'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='initiative',
            name='contact_person',
            field=models.CharField(help_text='Name der Ansprechperson', max_length=50, verbose_name='Kontaktperson', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='contribution',
            field=models.TextField(default='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', help_text='Wie du dich einbringen kannst', verbose_name='Beteiligung'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='initiative',
            name='legel_form',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Rechtsform', choices=[(None, b''), (1, 'Verein')]),
        ),
        migrations.AddField(
            model_name='initiative',
            name='logo',
            field=models.ImageField(upload_to=b'initiative/logos', null=True, verbose_name='Logo der Initiative', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='mail',
            field=models.EmailField(max_length=254, verbose_name='Email', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='note',
            field=models.TextField(help_text='Was wir sonst noch sagen wollen', verbose_name='Anmerkungen', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='phone',
            field=models.CharField(max_length=40, verbose_name='Telefon', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='size',
            field=models.IntegerField(help_text='Anzahl der aktiven Mitglieder', null=True, verbose_name='Gr\xf6\xdfe', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='topics',
            field=models.ManyToManyField(related_name='_topics_+', to='topic.SubTopic'),
        ),
        migrations.AddField(
            model_name='initiative',
            name='trust',
            field=models.SmallIntegerField(default=100, help_text='Was d\xfcrfen Initiativen ohne Moderation auf der Website ver\xf6ffentlichen', verbose_name='Vertrauen', choices=[(100, 'Komplett'), (50, 'Steckbrief'), (40, 'Kalendereintrag')]),
        ),
        migrations.AddField(
            model_name='initiative',
            name='website',
            field=models.URLField(verbose_name='Website', blank=True),
        ),
    ]
