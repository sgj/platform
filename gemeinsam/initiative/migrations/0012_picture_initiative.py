# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0011_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='picture',
            name='initiative',
            field=models.ForeignKey(default=1, to='initiative.Initiative', on_delete=models.CASCADE),
            preserve_default=False,
        ),
    ]
