# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0004_auto_20151117_0157'),
    ]

    operations = [
        migrations.AddField(
            model_name='initiative',
            name='country',
            field=models.CharField(max_length=50, verbose_name='Land', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='house_number',
            field=models.CharField(max_length=50, verbose_name='Hausnummer', blank=True),
        ),
        migrations.AddField(
            model_name='initiative',
            name='state',
            field=models.CharField(max_length=50, verbose_name='Bundesland', blank=True),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='address',
            field=models.CharField(help_text='Je genauer die Angabe der Adresse, desto leichter lassen sich die geografischen Koordinaten zur Kartenanzeige finden', max_length=100, verbose_name='Adresse', blank=True),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='street',
            field=models.CharField(max_length=50, verbose_name='Stra\xdfe', blank=True),
        ),
        migrations.AlterField(
            model_name='initiative',
            name='zipcode',
            field=models.CharField(max_length=50, verbose_name='PLZ', blank=True),
        ),
    ]
