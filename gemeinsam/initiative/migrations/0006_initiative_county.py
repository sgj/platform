# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0005_auto_20151117_1540'),
    ]

    operations = [
        migrations.AddField(
            model_name='initiative',
            name='county',
            field=models.CharField(max_length=50, verbose_name='Bezirk', blank=True),
        ),
    ]
