# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0019_auto_20160107_1555'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='initiative',
            options={'ordering': ['name'], 'verbose_name': 'Initiative', 'verbose_name_plural': 'Initiativen'},
        ),
        migrations.AlterModelOptions(
            name='permission',
            options={'ordering': ['initiative']},
        ),
    ]
