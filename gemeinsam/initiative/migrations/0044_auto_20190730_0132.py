# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-07-30 01:32


from django.db import migrations
import stdimage.models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0043_auto_20190214_2318'),
    ]

    operations = [
        migrations.AlterField(
            model_name='initiative',
            name='logo',
            field=stdimage.models.StdImageField(blank=True, null=True, upload_to='initiative/logos', verbose_name='Logo der Initiative'),
        ),
        migrations.AlterField(
            model_name='picture',
            name='photo',
            field=stdimage.models.StdImageField(upload_to='initiative/pictures', verbose_name='Bilddatei'),
        ),
    ]
