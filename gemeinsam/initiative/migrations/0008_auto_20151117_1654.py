# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('initiative', '0007_auto_20151117_1651'),
    ]

    operations = [
        migrations.RenameField(
            model_name='initiative',
            old_name='legel_form',
            new_name='legal_form',
        ),
    ]
