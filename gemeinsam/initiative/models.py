# -*- coding: utf-8 -*-
# Standard Library
import datetime

from functools import partial
from io import BytesIO

# Django stuff
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.core.files.base import ContentFile
from django.db.models.signals import post_init, post_save
from django.template.loader import render_to_string
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

# Third party
from django_bleach.models import BleachField
from phonenumber_field.modelfields import PhoneNumberField
from pictures.models import PictureField
from PIL import Image
from stdimage import StdImageField
from stdimage.utils import render_variations

# First party
from location.models import Location
from topic.models import SubTopic


class Initiative(models.Model):
    LEGAL_FORMS = (
        (None, _("Nicht angegeben")),
        (1, _("Verein")),
        (2, _("Unternehmen")),
        (3, _("Öffentliche Einrichtung")),
        (4, _("Genossenschaft")),
        (5, _("Bündnis")),
    )
    BACKGROUND = (
        (None, _("----")),
        (1, _("öffentliche Einrichtung")),
        (2, _("konfessionell")),
        (3, _("parteipolitisch")),
        (4, _("gewerblich")),
    )
    YES_NO = ((True, _("Ja")), (False, _("Nein")))

    # general
    name = models.CharField(
        _("Name"), max_length=100, help_text=_("Name der Initiative")
    )
    slug = models.SlugField(_("Slug"), max_length=110, unique=True)
    abbr = models.CharField(
        _("Abkürzung"),
        max_length=10,
        blank=True,
        help_text=_("Abkürzung der Initiative"),
    )
    logo = StdImageField(
        _("Logo der Initiative"),
        upload_to="initiative/logos",
        null=True,
        blank=True,
        variations={
            "thumbnail": (100, 100, True),
            "square": (300, 300, True),
            "large": (640, 480),
            "medium": (480, 360),
        },
    )

    # description
    aim = BleachField(verbose_name=_("Ziel"), help_text=_("Was wir erreichen wollen"))
    activity = BleachField(
        verbose_name=_("Aktivitäten"), help_text=_("Wie wir dorthin kommen wollen")
    )
    contribution = BleachField(
        verbose_name=_("Beteiligung"), help_text=_("Wie du dich einbringen kannst")
    )
    note = BleachField(
        verbose_name=_("Anmerkungen"),
        help_text=_("Was wir sonst noch sagen wollen"),
        blank=True,
    )

    # contact
    website = models.URLField(_("Website"), blank=True)
    mail = models.EmailField(_("Email"), blank=True)
    phone = PhoneNumberField(_("Telefon"), max_length=40, blank=True)
    matrix_room = models.CharField(
        _("Öffentlicher [matrix] Raum"), max_length=100, blank=True
    )
    contact_person = models.CharField(
        _("Kontaktperson"),
        max_length=50,
        blank=True,
        help_text=_("Name der Ansprechperson"),
    )

    # address - fk to location and coords will be reduntantly stored here
    # because of geodjango that needs to have a geometric field
    location = models.ForeignKey(
        Location,
        related_name="ini_loc",
        on_delete=models.PROTECT,
        verbose_name=_("Ort"),
    )
    coords = models.PointField(
        _("Geographische Koordinaten"), srid=4326, dim=2, blank=True, null=True
    )

    # miscellaneous
    legal_form = models.SmallIntegerField(
        _("Organisationsform"), choices=LEGAL_FORMS, blank=True, null=True
    )
    size = models.IntegerField(
        _("Größe"), help_text=_("Anzahl der aktiven Mitglieder"), blank=True, null=True
    )

    # administrative
    admin = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="admin+",
        help_text=_("Administrator der Initiative"),
    )

    background = models.SmallIntegerField(
        _("Hintergrund"),
        choices=BACKGROUND,
        blank=True,
        null=True,
        default=None,
        help_text=_("Hintergrund der Initiative (moderierter Status)"),
    )
    moderation = models.BooleanField(
        verbose_name=_("Moderation"),
        choices=YES_NO,
        default=False,
        help_text=_("Moderation von Veranstaltungen, ..."),
    )

    # collaborators
    collaborators = models.ManyToManyField(
        User, through="Permission", verbose_name=_("MitarbeiterInnen")
    )

    # published
    published = models.BooleanField(
        _("Veröffentlicht"),
        help_text=_("Steckbrief öffentlich zugänglich"),
        choices=YES_NO,
        default=False,
    )
    reason = models.TextField(
        _("Grund für Nichtveröffentlichung"), blank=True, default=_("Neue Initiative")
    )

    create = models.DateField(_("Angelegt"), auto_now_add=True)
    update = models.DateField(_("Zuletzt aktualisiert"), auto_now=True)

    # topics
    topics = models.ManyToManyField(SubTopic, verbose_name=_("Themen"), blank=True)

    def __str__(self):
        return self.name

    def __init__(self, *args, **kwargs):
        super(Initiative, self).__init__(*args, **kwargs)

        for field in ["aim", "activity", "contribution", "note"]:
            verbose_name = "get_{}_verbose_name".format(field)
            help_text = "get_{}_help_text".format(field)

            curried_verbose_name = partial(self._get_verbose_name, field)
            curried_help_text = partial(self._get_help_text, field)

            setattr(self, verbose_name, curried_verbose_name)
            setattr(self, help_text, curried_help_text)

    @classmethod
    def from_db(cls, db, field_names, values):
        instance = super(Initiative, cls).from_db(db, field_names, values)
        instance._loaded_values = dict(list(zip(field_names, values)))
        return instance

    def save(self, *args, **kwargs):
        self.coords = self.location.coords
        self.slug = self._get_unique_slug()
        if self.published:
            self.reason = ""
        super(Initiative, self).save(*args, **kwargs)

    def _get_unique_slug(self):
        slug = slugify(self.name)
        unique_slug = slug
        num = 1
        while Initiative.objects.filter(slug=unique_slug).exists():
            unique_slug = f"{slug}-{num}"
            num += 1
        return unique_slug

    def _get_help_text(self, field_name):
        for field in self._meta.fields:
            if field.name == field_name:
                return field.help_text

    def _get_verbose_name(self, field_name):
        for field in self._meta.fields:
            if field.name == field_name:
                return field.verbose_name

    def first_letter(self):
        return self.name[0].upper()

    def get_absolute_url(self):
        return "/initiative/{}".format(self.pk)

    @property
    def popup_content(self):
        return render_to_string("initiative/map_tooltip.html", {"ini": self})

    class Meta:
        ordering = ["name"]

        verbose_name = _("Initiative")
        verbose_name_plural = _("Initiativen")


def email_notify(sender, instance, created, **kwargs):
    # Django stuff
    from django.conf import settings
    from django.contrib.sites.models import Site
    from django.core.mail import EmailMultiAlternatives
    from django.template import loader

    site = Site.objects.get_current()
    c = {"initiative": instance, "site": site}

    if created:
        subject = "[{}] {} - {}".format(site.name, _("Neue Initiative"), instance)

        # mail to moderation
        t = loader.get_template("initiative/new_initiative.txt")
        msg = EmailMultiAlternatives(
            subject, t.render(c), settings.SERVER_EMAIL, [settings.MODERATION_EMAIL]
        )
        msg.send()

        # mail from moderation to admin of the initiative
        t = loader.get_template("initiative/new_initiative_user.txt")
        msg = EmailMultiAlternatives(
            subject, t.render(c), settings.MODERATION_EMAIL, [instance.admin.email]
        )
        msg.send()

    else:
        # check if published state changed to True
        if not instance.previous_published_state and instance.published:
            subject = "[{}] {} - {}".format(
                site.name, _("Initiative veröffentlicht!"), instance
            )

            # mail from moderation to admin of the initiative
            # depending on the background
            t = loader.get_template("initiative/new_initiative_published.txt")
            msg = EmailMultiAlternatives(
                subject, t.render(c), settings.MODERATION_EMAIL, [instance.admin.email]
            )
            msg.send()

            # also publish events (if ini is not moderated)
            if not instance.moderation:
                for event in instance.events.all():
                    event.published = instance.published
                    event.save(update_ini=False)
            else:
                if instance.events.count() > 0:
                    # notify moderation about unpublished events
                    subject = "[{}] {}".format(site.name, instance)

                    t = loader.get_template("initiative/moderated_events.txt")
                    msg = EmailMultiAlternatives(
                        subject,
                        t.render(c),
                        settings.SERVER_EMAIL,
                        [settings.MODERATION_EMAIL],
                    )
                    msg.send()


post_save.connect(email_notify, sender=Initiative)


def check_published_state(sender, instance, **kwargs):
    instance.previous_published_state = instance.published


post_init.connect(check_published_state, sender=Initiative)


def resize_and_autorotate(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format
            # png or gif do not have _getexif
            exif = getattr(image, "_getexif", lambda: None)()

            # if image has exif data about orientation, let's rotate it
            orientation_key = 274  # cf ExifTags
            if exif and orientation_key in exif:
                orientation = exif[orientation_key]

                rotate_values = {
                    3: Image.ROTATE_180,
                    6: Image.ROTATE_270,
                    8: Image.ROTATE_90,
                }

                if orientation in rotate_values:
                    image = image.transpose(rotate_values[orientation])

            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                storage.delete(file_name)
                storage.save(file_name, f)

    # render stdimage variations
    render_variations(file_name, variations, replace=True, storage=storage)

    return False  # prevent default rendering


class ReminderSent(models.Model):
    initiative = models.OneToOneField(Initiative, on_delete=models.CASCADE)
    sent = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} -> {}".format(self.sent, self.initiative)


class Picture(models.Model):
    title = models.CharField(_("Bildunterschrift"), max_length=100, blank=True)
    photo_width = models.PositiveIntegerField(null=True, editable=False)
    photo_height = models.PositiveIntegerField(null=True, editable=False)
    photo = PictureField(
        _("Bilddatei"),
        upload_to="initiative/pictures",
        width_field="photo_width",
        height_field="photo_height",
    )
    order = models.SmallIntegerField(_("Sortierung"), default=0)
    initiative = models.ForeignKey(Initiative, on_delete=models.CASCADE)

    class Meta:
        ordering = ["order"]
        verbose_name = _("Foto")
        verbose_name_plural = _("Fotos")


class Permission(models.Model):
    MODULES = (
        (1, _("Steckbrief")),
        (2, _("Veranstaltungen")),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    initiative = models.ForeignKey(Initiative, on_delete=models.CASCADE)
    module = models.SmallIntegerField(choices=MODULES, default=1)

    class Meta:
        ordering = ["initiative", "user", "module"]
        unique_together = ("user", "initiative", "module")

    def __str__(self):
        return _("{} -- {}: {}").format(
            self.user, self.initiative, self.get_module_display()
        )


class InitiativeOnFront(models.Model):
    initiative = models.OneToOneField(Initiative, on_delete=models.CASCADE)
    last_date = models.DateField(
        _("Zuletzt angezeigt"), default=datetime.date(1970, 1, 1)
    )
    count = models.IntegerField(_("Anzahl der Anzeigen"), default=0)

    class Meta:
        ordering = ["count", "last_date"]

    def __str__(self):
        return f"{self.initiative} -- ({self.last_date} / {self.count})"
