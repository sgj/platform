# Standard Library
import datetime

# Django stuff
from django.conf import settings
from django.contrib.sites.models import Site
from django.core import signing
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand, CommandError
from django.template import loader

# Third party
from dateutil.parser import parse, ParserError

# First party
from initiative.models import Initiative, ReminderSent


class Command(BaseCommand):
    help = "Send out keep-alive mails to initiatives/moderation"

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="test",
            default=False,
            help="do not send out emails but print the actions on console",
        )
        parser.add_argument(
            "--date",
            dest="date",
            type=str,
            help="simulated run date (that can somehow be parsed)"
            " will be ingnored in non dry-run",
        )

    def handle(self, *args, **options):
        date = datetime.date.today()
        if options["test"]:
            if options["date"]:
                try:
                    date = parse(options["date"]).date()
                except ParserError:
                    raise CommandError(f"{options['date']} is not a valid date")

        # load templates and set common context
        subject = loader.get_template("initiative/keep_alive_subject.txt")
        text = loader.get_template("initiative/keep_alive_mail.txt")
        html = loader.get_template("initiative/keep_alive_mail.html")

        from_addr = settings.MODERATION_EMAIL
        context = {"site": Site.objects.get_current()}

        # loop over all initiatives that were not updated recently
        initiatives = Initiative.objects.filter(
            published=True,
            update__lt=date
            - datetime.timedelta(settings.KEEP_ALIVE_MAIL["first_warning"]),
        )

        for initiative in initiatives:
            # check if a reminder has been sent recently
            rs, created = ReminderSent.objects.get_or_create(initiative=initiative)
            if (
                not created
                and (date - rs.sent.date()).days
                < settings.KEEP_ALIVE_MAIL["warning_frequency"]
            ):
                continue

            # otherwise sent the keep alive mail and update the ReminderSent
            context.update(
                dict(
                    initiative=initiative,
                    days_update=(date - initiative.update).days,
                    token=signing.dumps(
                        {"update": str(initiative.update), "pk": initiative.pk}
                    ),
                )
            )

            mail_subject = subject.render(context).strip()
            mail_body = text.render(context)
            mail_html = html.render(context)
            recipients = [initiative.admin.email]

            # check if mail should also go to the moderation
            if (date - initiative.update).days > settings.KEEP_ALIVE_MAIL[
                "to_moderation"
            ]:
                recipients.append(settings.MODERATION_EMAIL)

            # output either by mail or on console
            if options["test"]:
                self.stdout.write("Email sent by {}".format(from_addr))
                self.stdout.write("To: {}".format([x.__str__() for x in recipients]))
                self.stdout.write("Subject: {}".format(mail_subject))
                self.stdout.write("{}".format(mail_body))
                self.stdout.write("\n")

                # in dry-run delete the reminder if it was created
                if created:
                    rs.delete()

            else:
                # send to all recipients as mass mail
                msg = EmailMultiAlternatives(
                    mail_subject, mail_body, from_addr, recipients
                )
                if mail_html:
                    msg.attach_alternative(mail_html, "text/html")
                msg.send()
                rs.save()
