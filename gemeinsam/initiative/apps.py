# Django stuff
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

# Third party
from watson import search as watson


class InitiativeAppConfig(AppConfig):
    name = "initiative"
    verbose_name = _("Initiativen")

    def ready(self):
        InitiativeModel = self.get_model("initiative")
        watson.register(
            InitiativeModel.objects.filter(published=True),
            fields=(
                "name",
                "abbr",
                "aim",
                "activity",
                "contribution",
                "note",
                "website",
                "mail",
                "contact_person",
                "location__city",
                "location__county",
                "get_legal_form_display",
                "get_background_display",
            ),
        )
