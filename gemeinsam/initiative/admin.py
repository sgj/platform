# -*- coding: utf-8 -*-
# Standard Library
import csv

# Django stuff
from django import forms
from django.contrib import admin
from django.http import HttpResponse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

# Third party
from django_ckeditor_5.widgets import CKEditor5Widget

# Local imports
from .models import Initiative, Permission, Picture


def export_emails(modeladmin, request, queryset):
    query = modeladmin.model.objects.filter(published=True)
    emails = list()
    for ini in query:
        emails.extend([ini.admin.email, ini.mail])
    emails = [[mail] for mail in sorted([_f for _f in list(set(emails)) if _f])]

    # build response object
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = (
        "attachment; filename=adressen_koopletter_{:%F}.csv".format(now())
    )
    writer = csv.writer(response)
    writer.writerows(emails)

    return response


export_emails.short_description = _(
    "Email Adressen für Koop-Letter exportieren "
    "(unabhängig von der aktuellen Auswahl)"
)
export_emails.acts_on_all = True


class TopicFilter(admin.SimpleListFilter):
    title = _("Themen")
    parameter_name = "topics"

    def lookups(self, request, model_admin):
        topics_list = [t.topics.all() for t in model_admin.model.objects.all()]
        topics = set([item for sublist in topics_list for item in sublist])
        return sorted([(t.id, t.name) for t in topics], key=lambda x: x[1])

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(topics__id__exact=self.value())
        else:
            return queryset


class PictureInline(admin.TabularInline):
    model = Picture


class CollaboratorsInline(admin.TabularInline):
    model = Permission


class InitiativeAdminForm(forms.ModelForm):
    class Meta:
        model = Initiative
        fields = "__all__"
        widgets = {
            "aim": CKEditor5Widget(config_name="default"),
            "activity": CKEditor5Widget(config_name="default"),
            "contribution": CKEditor5Widget(config_name="default"),
            "note": CKEditor5Widget(config_name="default"),
        }


@admin.register(Initiative)
class InitiativeAdmin(admin.ModelAdmin):
    list_display = ("name", "contact_person", "admin", "update")
    list_display_links = ("name",)
    list_filter = (TopicFilter, "published", "background", "moderation", "legal_form")

    actions = [export_emails]
    search_fields = ("name", "abbr")

    readonly_fields = ("slug", "update", "coords")

    inlines = [PictureInline, CollaboratorsInline]

    fieldsets = (
        (_("Allgemein"), {"fields": ("name", "abbr", "logo", "slug")}),
        (_("Beschreibung"), {"fields": ("aim", "activity", "contribution", "note")}),
        (_("Kontakt"), {"fields": ("contact_person", "website", "mail", "phone")}),
        (_("Adresse"), {"fields": ("location", "coords")}),
        (_("Sonstiges"), {"fields": ("legal_form", "size")}),
        (
            _("Intern"),
            {
                "fields": (
                    "admin",
                    ("background", "moderation"),
                    ("published", "reason"),
                    "update",
                )
            },
        ),
        (_("Themen"), {"fields": ("topics",)}),
    )

    form = InitiativeAdminForm

    def changelist_view(self, request, extra_context=None):
        try:
            action = self.get_actions(request)[request.POST["action"]][0]
            action_acts_on_all = action.acts_on_all
        except (KeyError, AttributeError):
            action_acts_on_all = False

        if action_acts_on_all:
            post = request.POST.copy()
            post.setlist(
                admin.helpers.ACTION_CHECKBOX_NAME,
                self.model.objects.values_list("id", flat=True),
            )
            request.POST = post

        return admin.ModelAdmin.changelist_view(self, request, extra_context)
