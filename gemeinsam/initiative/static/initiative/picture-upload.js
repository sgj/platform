$(function () {
    function show_hide() {
	if (document.querySelectorAll("#sortable li").length === 0) {
	    document.querySelector("#picture-sort").classList.add("d-none");
	} else {
	    document.querySelector("#picture-sort").classList.remove("d-none");
	}
    }

    // first check if the picture sorted shall be shown
    show_hide();

    $("#sortable").sortable({
	group: "list-group",
	placeholder: '<li class="list-group-item placeholder"></li>',
	handle: 'i.fa-arrows'
    });

    $(".js-upload-photos").click(function () {
	$("#fileupload").click();
    });

    $(document).on("click", "button.delete", function() {
	$.ajax({
	    type: 'POST',
	    url: $(this).data('url'),
	    data: { pk: $(this).data('id') },
	    headers: {
		'X-METHODOVERRIDE': 'DELETE',
		'X-CSRFToken': $(this).data('token')
	    },
	    success: function(data) {
		if (data.is_valid) {
		    $('button.delete[data-id="'+data.pk+'"]').parents('li').remove();
		}
		show_hide();
	    }
	});
    });

    $("#fileupload").fileupload({
	dataType: 'json',
	sequentialUploads: true,
	start: function (e) {
	    $("#modal-progress").modal("show");
	},
	stop: function (e) {
	    $("#modal-progress").modal("hide");
	},
	progressall: function (e, data) {
	    var progress = parseInt(data.loaded / data.total * 100, 10);
	    var strProgress = progress + "%";
	    $(".progress-bar").css({"width": strProgress});
	    $(".progress-bar").text(strProgress);
	},
	done: function (e, data) {
	    if (data.result.is_valid) {
		$("#sortable").append(
		    '<li class="list-group-item">\
                       <div class="d-flex justify-content-between">\
                         <div class="align-self-center">\
                           <i class="fa fa-arrows" aria-hidden="true"></i>\
                         </div>\
                         <div class="input-group ms-2">\
                           <span class="input-group-prepend">\
                             <img class="img-responsive" width="32px" height="32px" style="width: 32px; height: 32px; object-fit: cover;" src="'+ data.result.url + '">\
                           </span>\
                           <input type="hidden" value="'+data.result.id+'" name="sorting[]">\
                           <input type="text" style="background-color: #f8f9fa;" class="form-control form-control-sm" value="" name="title-'+ data.result.id + '" placeholder="' + data.result.placeholder + '">\
                           <span class="input-group-append">\
                             <button type="button" data-url="' + data.result.delete_url + '" data-id="' + data.result.id + '" data-token="' + data.result.token + '" class="delete btn btn-sm btn-danger me-auto">' + data.result.delete_button + '</button>\
                           </span>\
                         </div>\
                       </div>\
                     </li>');
		show_hide();
	    }
	}
    });

});
