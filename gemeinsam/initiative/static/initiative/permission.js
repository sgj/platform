document.addEventListener("DOMContentLoaded", (event) => {
    $("#user-select").autoComplete();
    $("#user-select").on('autocomplete.select', function(evt, item) {
	// create list item and clear search field
	var newline=$("#template").children().first().clone();
	newline.find('input').each(function() {
	    $(this).attr("name", "permissions[]");
	    $(this).val(item.value+$(this).val());
     	});
	newline.find(".user").text(item.text);
	newline.appendTo("#permissions");
	$("#user-select").autoComplete('clear');
    });
});
