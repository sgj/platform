var filters = {};
if (!_.isUndefined(localStorage['filters'])) {
    filters = JSON.parse(localStorage['filters']);
}
update_list(filters);

$.subscribe('filters.update', function(evt, filters) {
    update_list(filters)
});

function update_list(filters) {
    $(".ini-card").removeClass("on");

    // by topics
    if (filters.topics.length) {
	$.each(filters.topics, function(idx, val) {
	    $(".ini-card[data-topics*="+val+"]").addClass('on');
	});
    } else {
	$(".ini-card").addClass("on");
    }

    // by counties
    if (filters.counties.length) {
	$(".ini-card.on").each(function(idx) {
	    if ($.inArray($(this).attr('data-county'), filters.counties)<0) {
		$(this).removeClass("on");
	    }
	});
    }

    // by ini name or abbr
    if (filters.initiative) {
	$(".ini-card.on").each(function(idx) {
	    if ($(this).attr('data-names').indexOf(filters.initiative.toLowerCase())<0) {
		$(this).removeClass("on");
	    }
	});
    }

    // by persons
    if (filters.person) {
	$(".ini-card.on").each(function(idx) {
	    if ($(this).attr('data-persons').indexOf(filters.person.toLowerCase())<0) {
		$(this).removeClass("on");
	    }
	});
    }

    // by accessibilty
    if (filters.accessible) {
	$(".ini-card.on").each(function(idx) {
	    if ($(this).attr('data-accessible') != 'True') {
		$(this).removeClass("on");
	    }
	});
    }

    // hide and show
    $("section.grouped-by").hide();
    $("section.grouped-by:has(.ini-card.on)").show();

    $(".ini-card").hide();
    $(".ini-card.on").show();

    if ($(".ini-card.on").length) {
	$("#no-entry").hide();
    } else {
	$("#no-entry").show();
    }

    // set borders for list-groups
    // in grouped-by
    if (document.querySelector(".grouped-by") !== null) {
	$(".grouped-by:visible").each(function(idx, item) {
	    length = $(item).find(".list-group-item:visible").length;
	    console.log(length);
	    $(item).find(".list-group-item:visible").each(function(index) {
		if(index == 0) {
		    $(this).css("border-top", "1px solid rgba(0, 0, 0, 0.125)");
		    $(this).css("border-top-left-radius", "inherit");
		    $(this).css("border-top-right-radius", "inherit");
		}
		if(index == length - 1){
		    $(this).css("border-bottom-left-radius", "inherit");
		    $(this).css("border-bottom-right-radius", "inherit");
		}
	    })
	})
    } else {
	// plain
	length = $(".list-group-item:visible").length;
	$(".list-group-item:visible").each(function(index) {
	    if(index == 0) {
		$(this).css("border-top", "1px solid rgba(0, 0, 0, 0.125)");
		$(this).css("border-top-left-radius", "inherit");
		$(this).css("border-top-right-radius", "inherit");
	    }
	    if(index == length - 1){
		$(this).css("border-bottom-left-radius", "inherit");
		$(this).css("border-bottom-right-radius", "inherit");
	    }
	})
    }

    // update anchors
    $("#anchors").empty();
    $("section.grouped-by:has(.ini-card.on)").each(function(idx, element){
	text = $(element).children("h3").text();
	$("#anchors").append('<li class="list-inline-item"><a href="#'+element.id+'">'+text+'</a>');
    });
}
