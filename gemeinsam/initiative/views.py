# -*- encoding: utf-8 -*-


# Standard Library
import datetime

from itertools import groupby
from typing import Optional

# Django stuff
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core import signing
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db.models import Prefetch, Q
from django.http import HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.middleware.csrf import get_token
from django.shortcuts import get_object_or_404, render
from django.urls import resolve, reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views import View, generic
from django.views.decorators.csrf import csrf_exempt

# Third party
from djgeojson.views import GeoJSONLayerView

# First party
from location.apps import GEOCODE2MODELFIELD, MAP_STRINGS
from location.forms import LocationForm
from topic.models import SubTopic

# Local imports
from .forms import InitiativeForm, PictureForm
from .models import Initiative, Permission, Picture


def ajax_required(f):
    """AJAX request required decorator use it in your views.

    @ajax_required
    def my_view(request):
        ....

    """

    def wrap(request, *args, **kwargs):
        if not request.headers.get("x-requested-with") == "XMLHttpRequest":
            return HttpResponseBadRequest()
        return f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def _filter_by_user(user, filtered=True):
    qs = Initiative.objects.prefetch_related(
        Prefetch("topics", queryset=SubTopic.objects.select_related("parent"))
    )
    if not filtered:
        return qs

    # else check if user may see the initiative
    if user and user.is_superuser:
        return qs
    else:
        query = Q(published=True) & ~Q(abbr="n/a")
        if user and user.is_authenticated:
            query |= Q(admin=user)
        return qs.filter(query)


@csrf_exempt
@ajax_required
def user_list(request, id):
    q = request.GET.get("q", "")
    name_query = (
        Q(first_name__icontains=q)
        | Q(last_name__icontains=q)
        | Q(username__icontains=q)
    )
    registration_query = Q(registrationprofile__activated=True) | Q(
        registrationprofile__isnull=True
    )

    try:
        initiative = Initiative.objects.get(pk=id)
    except Initiative.DoesNotExist:
        return JsonResponse([dict(value=0, text="")], safe=False)

    exclude = [initiative.admin.pk] + list(
        initiative.permission_set.all().values_list("user", flat=True)
    )
    user_list = User.objects.filter(registration_query, name_query).exclude(
        pk__in=exclude
    )
    ret = [dict(value=x.id, text=x.get_full_name() or x.username) for x in user_list]
    return JsonResponse(ret, safe=False)


class IndexView(generic.ListView):
    template_name = "initiative/index.html"
    context_object_name = "initiative_list"

    def get_queryset(self):
        """Return all published or own initiatives"""
        return _filter_by_user(self.request.user)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        url_split = resolve(self.request.path_info).url_name.split("-")
        context.update(
            {
                "group": url_split[0],
                "list": url_split[-1] if len(url_split) > 1 else "",
                "communication_tool": getattr(
                    settings, "COMMUNICATION_TOOL", {"base_url": "#"}
                ),
            }
        )
        return context


class IndexViewByCounty(generic.ListView):
    template_name = "initiative/bycounty.html"
    context_object_name = "initiative_list"

    def get_queryset(self):
        """Return all published or own initiatives sorted by county"""
        return _filter_by_user(self.request.user).order_by("location__county", "name")

    def get_context_data(self, **kwargs):
        context = super(IndexViewByCounty, self).get_context_data(**kwargs)
        url_split = resolve(self.request.path_info).url_name.split("-")
        context.update(
            {
                "group": url_split[0],
                "list": url_split[-1] if len(url_split) > 1 else "",
                "communication_tool": getattr(
                    settings, "COMMUNICATION_TOOL", {"base_url": "#"}
                ),
            }
        )
        return context


class IndexViewByTopic(generic.ListView):
    template_name = "initiative/bytopic.html"
    context_object_name = "topic_initiatives_list"

    def get_queryset(self):
        """Return a list of topics/ini_list tuples"""
        ret = []
        for st in SubTopic.objects.all():
            inis = _filter_by_user(self.request.user).filter(topics=st)
            if inis:
                ret.append([st, inis])
        return ret

    def get_context_data(self, **kwargs):
        context = super(IndexViewByTopic, self).get_context_data(**kwargs)
        url_split = resolve(self.request.path_info).url_name.split("-")
        context.update(
            {
                "group": url_split[0],
                "list": url_split[-1] if len(url_split) > 1 else "",
                "communication_tool": getattr(
                    settings, "COMMUNICATION_TOOL", {"base_url": "#"}
                ),
            }
        )
        return context


class DetailView(generic.DetailView):
    context_object_name = "ini"
    template_name = "initiative/detail.html"
    model = Initiative

    def get_queryset(self):
        # queryset = super(DetailView, self).get_queryset()
        return _filter_by_user(self.request.user, False)

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        next_events = sorted(
            [
                _f
                for _f in [
                    x.next_occurrence()
                    for x in self.get_object().events.filter(published=True)
                ]
                if _f
            ],
            key=lambda x: x.start_time,
        )

        context["next_events"] = next_events
        context["moderation_mail"] = getattr(
            settings, "MODERATION_EMAIL", "moderation@gemeinsam.jetzt"
        )
        context["communication_tool"] = getattr(
            settings, "COMMUNICATION_TOOL", {"base_url": "#"}
        )
        return context


# ajax views for adding and deleting pictures
class PictureUploadView(View):
    def delete(self, request):
        # check if user has permission to delete the image
        data = {"is_valid": False}
        pk = request.DELETE["pk"]
        try:
            picture = Picture.objects.get(pk=pk)
            initiative = picture.initiative
            if request.user.is_authenticated and request.user.has_perm(
                "initiative.change_initiative", initiative
            ):
                picture.delete()
                data = {"is_valid": True, "pk": pk}
        except ObjectDoesNotExist:
            pass

        return JsonResponse(data)

    def post(self, request, ini):
        # return false if
        # 1. ini does not exist
        # 2. user has no change permission
        # 3. form is not valid
        data = {"is_valid": False}
        try:
            initiative = Initiative.objects.get(pk=ini)
            if request.user.is_authenticated and request.user.has_perm(
                "initiative.change_initiative", initiative
            ):
                form = PictureForm(
                    self.request.POST,
                    self.request.FILES,
                    instance=Picture(initiative=initiative),
                )

                if form.is_valid():
                    picture = form.save()
                    data = {
                        "is_valid": True,
                        "id": picture.pk,
                        "placeholder": _("Titel für dein neues Bild"),
                        "delete_url": reverse("initiative:picture-delete"),
                        "delete_button": _("Löschen"),
                        "url": picture.photo.url,
                        "token": get_token(request),
                    }
        except ObjectDoesNotExist:
            pass

        return JsonResponse(data)


# adding a initiative can be done by every logged-in user
# thus check the change permission within the view-function
@login_required
def edit(request, id=None):
    if id:
        initiative = get_object_or_404(Initiative, pk=id)
        # check, if user is allowed to edit
        if not request.user.has_perm("initiative.change_initiative", initiative):
            raise PermissionDenied

    else:
        initiative = Initiative(admin=request.user)

    location_form = LocationForm(prefix="loc")

    if request.method == "POST":
        data = request.POST.copy()

        # first check for deletion
        if request.POST.get("deletion"):
            if request.user == initiative.admin:
                initiative.delete()
                return HttpResponseRedirect("/")
            else:
                raise PermissionDenied

        if request.POST.get("location") == "0":
            location_form = LocationForm(data=data, prefix="loc")

            if location_form.is_valid():
                new_location = location_form.save(commit=False)
                new_location.creator = request.user
                new_location.save()
                data["location"] = "{}".format(new_location.id)

        # save picture order and titles
        sorting = request.POST.getlist("sorting[]")
        for order, id in enumerate(sorting):
            pic = Picture.objects.get(pk=id)
            pic.order = order
            pic.title = request.POST.get("title-{}".format(id))
            pic.save()

        # save permissions
        if request.user == initiative.admin:
            try:
                initiative.permission_set.all().delete()
            except ValueError:
                pass
            Permission.objects.bulk_create(
                [
                    Permission(
                        initiative=initiative, user_id=int(user_id), module=int(module)
                    )
                    for (user_id, module) in (
                        x.split("-") for x in request.POST.getlist("permissions[]")
                    )
                ]
            )

        form = InitiativeForm(data=data, files=request.FILES, instance=initiative)

        if form.is_valid() and form.is_multipart():
            initiative = form.save()
            return HttpResponseRedirect(
                reverse("initiative:detail", args=[initiative.pk])
            )

    else:
        form = InitiativeForm(instance=initiative, user=request.user)

    map_strings = dict(**MAP_STRINGS)
    region_force = {}
    if hasattr(settings, "LOCATION_REGION_FORCE"):
        region_force = settings.LOCATION_REGION_FORCE
        region = settings.LOCATION_REGION_FORCE["name"]
        map_strings["search"] = _("Suche in {}").format(region)
        map_strings["error"] = _("Ort liegt nicht in: {}".format(region))

    if initiative.pk:
        permission_list = [
            (x[0], [y.module for y in x[1]])
            for x in groupby(initiative.permission_set.all(), key=lambda x: x.user)
        ]
    else:
        permission_list = []

    return render(
        request,
        "initiative/edit.html",
        {
            "form": form,
            "location_form": location_form,
            "permission_list": permission_list,
            "module_list": Permission._meta.get_field("module").choices,
            "map_strings": map_strings,
            "geocode2modelfield": GEOCODE2MODELFIELD,
            "settings": {
                "MODERATION_EMAIL": settings.MODERATION_EMAIL,
                "PELIAS_API_KEY": settings.PELIAS_API_KEY,
                "PELIAS_URL": settings.PELIAS_URL,
            },
            "region_force": region_force,
        },
    )


def update(request, token):
    """Update an initiative if token is valid."""
    initiative: Optional[Initiative] = None
    msg: Optional[str] = None

    try:
        original = signing.loads(token, max_age=datetime.timedelta(days=10))

    except signing.SignatureExpired:
        msg = _(
            "Dein Token ist bereits abgelaufen. Warte auf ein neues Token oder "
            "bearbeite deine Initiative händisch. Die Anleitung dazy findest "
            "du in der E-Mail."
        )

    except signing.BadSignature:
        msg = _("Dies ist kein gültiges Token!")

    else:
        try:
            initiative = Initiative.objects.get(pk=original["pk"])
            initiative.save()
        except Initiative.DoesNotExist:
            msg = _("Deine Initiative wurde in der Zwischenzeit leider gelöscht.")

    return render(
        request, "initiative/update.html", {"initiative": initiative, "error": msg}
    )


@method_decorator(csrf_exempt, name="dispatch")
class InitiativeMapView(GeoJSONLayerView):
    def post(self, request, *args, **kwargs):
        # Standard Library
        import json

        self.context = {
            "topic_ids": request.POST.getlist("topics[]", []),
            "county_names": [
                x.split(":")[-1] for x in request.POST.getlist("counties[]", [])
            ],
            "initiative": request.POST.get("initiative", ""),
            "person": request.POST.get("person", ""),
            "accessible": json.loads(request.POST.get("accessible", "false")),
        }
        return super(InitiativeMapView, self).render_to_response(self.context)

    def get_queryset(self):
        # queryset = super(InitiativeMapView, self).get_queryset()
        ret = _filter_by_user(self.request.user)

        # apply filters
        if self.context["topic_ids"]:
            ret = ret.filter(topics__in=self.context["topic_ids"]).distinct()

        if self.context["county_names"]:
            ret = ret.filter(location__county__in=self.context["county_names"])

        if self.context["initiative"]:
            ret = ret.filter(name__icontains=self.context["initiative"]) | ret.filter(
                abbr__icontains=self.context["initiative"]
            )

        if self.context["person"]:
            ret = (
                ret.filter(admin__first_name__icontains=self.context["person"])
                | ret.filter(admin__last_name__icontains=self.context["person"])
                | ret.filter(contact_person__icontains=self.context["person"])
            )

        if self.context["accessible"]:
            ret = ret.filter(location__accessible=True)

        return ret


def map(request, pk=None):
    lat, lon = 0, 0
    if pk:
        ini = get_object_or_404(Initiative, pk=pk)
        lat, lon = ini.coords.y, ini.coords.x

    return render(request, "initiative/map.html", {"lat": str(lat), "lon": str(lon)})
