# Standard Library
import copy

# Django stuff
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.models import Value as V
from django.db.models.functions import Concat
from django.template import Library

register = Library()

communication_tool = getattr(settings, "COMMUNICATION_TOOL", {"base_url": "#"})


@register.inclusion_tag("initiative/initiative-users.html")
def initiative_matrix_users(initiative, request_user):
    user_ids = [initiative.admin.id] + list(
        initiative.collaborators.distinct().values_list("id", flat=True)
    )

    # list of all initiatives that have somehow a matrix appearance
    level = request_user.is_authenticated and 1 or 2

    # get all users that have a matrix id
    qs = User.objects.filter(
        ~Q(profile__matrix_id__isnull=True)
        & Q(profile__matrix_privacy__gte=level)
        & Q(id__in=user_ids)
    )

    qs = qs.exclude(first_name="", last_name="")
    user_list = qs.annotate(
        order_name=Concat("first_name", "last_name", V("z"))
    ).order_by("order_name")

    return {"communication_tool": communication_tool, "user_list": user_list}


@register.inclusion_tag("initiative/initiative-contact.html")
def initiative_matrix_contact(initiative):
    ret = {"communication_tool": communication_tool, "ini": initiative}

    return ret
