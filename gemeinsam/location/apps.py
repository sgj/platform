# Django stuff
from django.apps import AppConfig
from django.conf import settings
from django.utils.translation import gettext_lazy as _

# mapping between location model and osm-places
GEOCODE2MODELFIELD = {
    "city": ["localadmin", "locality", "city", "town", "village", "hamlet", "state"],
    "street": ["street", "road", "pedestrian"],
    "house_number": ["housenumber", "house_number"],
    "zipcode": ["postalcode", "postcode"],
    "county": ["county", "city_district", "suburb"],
    "state": ["region", "state"],
    "country": ["country"],
    "name": ["name", "house", "house_name", "building", "cafe"],
    "label": ["label"],
}

MAP_STRINGS = {
    "nothing_found": _("Nichts gefunden :-("),
    "search": _("Suche"),
    "reset": _("Zurücksetzen"),
    "country_code": getattr(settings, "LOCATION_COUNTRY_CODE", "").lower(),
    "language_code": getattr(settings, "LANGUAGE_CODE", "").split("-")[0].lower(),
}


class LocationConfig(AppConfig):
    name = "location"
    verbose_name = _("Ort")
    verbose_name_plural = _("Orte")
