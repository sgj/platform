# -*- coding: utf-8 -*-
# Django stuff
from django.conf import settings

# Third party
from geopy.geocoders import Nominatim, Pelias

# Local imports
from .apps import GEOCODE2MODELFIELD


def _get_host_from_url(url):
    url_list = url.split("://")
    i = (0, 1)[len(url_list) > 1]
    return url_list[i].split("?")[0].split("/")[0].split(":")[0].lower()


def _get_city_district(latlng):
    """Get city districts from Nominatim."""
    user_agent = _get_host_from_url(settings.SITE_URL)
    geocoder = Nominatim(user_agent=user_agent)

    geo = geocoder.reverse(latlng, language=settings.LANGUAGE_CODE).raw
    ret = ""
    for key in GEOCODE2MODELFIELD["county"]:
        if key in geo["address"]:
            ret = geo["address"][key]
            break

    return ret


def geocode_reverse(latlng):
    """
    Get geocoded address from latitude / longitude tuples. This
    address is normalized to the keys in GEOCODE2MODELFIELD dict

    Args:

    * latlng: tuple

    Returns:

        Dictionary with normalized postal entries
    """
    if settings.PELIAS_URL:
        pelias_domain = _get_host_from_url(settings.PELIAS_URL)
        geocoder = Pelias(pelias_domain, api_key=settings.PELIAS_API_KEY)
        address_key = "properties"
        label_key = None
    else:
        user_agent = _get_host_from_url(settings.SITE_URL)
        geocoder = Nominatim(user_agent=user_agent)
        address_key = "address"
        label_key = "display_name"

    geo = geocoder.reverse(latlng, language=settings.LANGUAGE_CODE).raw
    address = geo[address_key]
    if label_key:
        address.update({"label": geo[label_key]})

    ret = dict((key, "") for key in list(GEOCODE2MODELFIELD.keys()))

    if address:
        for k, v in GEOCODE2MODELFIELD.items():
            for item in v:
                attr = address.get(item, None)
                if attr:
                    ret[k] = attr
                    break

    # pelias does not provide city districts
    if not ret["county"]:
        ret["county"] = _get_city_district(latlng)

    return ret
