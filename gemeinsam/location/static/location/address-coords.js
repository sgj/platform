document.addEventListener("DOMContentLoaded", (event) => {

    var map_strings = JSON.parse(document.getElementById('map-strings').textContent);
    var geocode2modelfield = JSON.parse(document.getElementById('geocode2modelfield').textContent);
    var settings = JSON.parse(document.getElementById('settings').textContent);
    var region_force = JSON.parse(document.getElementById('region-force').textContent);
    var url = JSON.parse(document.getElementById('get-address-url').textContent);

    var map;

    $.ajaxSetup({
	headers: { "X-CSRFToken": $("input[type='hidden'][name='csrfmiddlewaretoken']").val() }
    });

    $(window).on('map:init', function(e) {

	map = e.originalEvent.detail.map;
	var geometryField = null;
	var geocoder;

	function update_map_and_fields(e, latlng, address, text) {
	    // check if input is valid, i.e. if found location is in desired region
	    if ( typeof(region_force) === 'object' && address[region_force.field] != region_force.name ) {
		L.popup().setLatLng(latlng).setContent(
		    '<ul class="fa-ul"><li><i class="fa-li fa fa-fw fa-times-circle-o" aria-hidden="true"></i>'+
			map_strings.error+'</li></ul>'
		).openOn(map);
		// clear fields
		$("input[id^='id_loc-']").each(function() {
		    $(this).val('');
		});

	    } else {
		// set marker and zoom to it
		e.layer = new L.marker(latlng)
		    .bindPopup(
			'<ul class="fa-ul"><li><i class="fa-li fa fa-fw fa-map-marker" aria-hidden="true"></i>'
			    +address.label+'</li></ul>')
		    .addTo(map)
		    .openPopup();
		geometryField.onCreated(e);
		map.setView(latlng, 17);

		// if county is missing make api call to python routine
		if (address.county === undefined) {
		    $.ajax({
			type: 'POST',
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			url: url,
			data: JSON.stringify(latlng),
			success: function(result) {
			    $.each(result, function(key, value) {
				$("input[id='id_loc-"+key+"']").val(value);
			    });
			}
		    });
		} else {
		    // fill fields from already retrieved data
		    $.each(geocode2modelfield, function(id, fields){
			$("input[id='id-loc_"+id+"']").val('');
	    		$.each(fields, function(i, field) {
	    		    if (address[field]) {
	    			$("input[id='id_loc-"+id+"']").val(address[field]);
	    			return false;
	    		    }
	    		})
		    });
		}
	    }
	}

	if (settings.PELIAS_URL != '') {
	    // pelias geocoder
	    geocoderOptions = {
		url: settings.PELIAS_URL,
		autocomplete: true,
		fullWidth: true,
		position: 'bottomleft',
		expanded: true,
		focus: true,
		placeholder: map_strings.search,
		textStrings: {
		    INPUT_PLACEHOLDER: map_strings.search,
		    RESET_TITLE_ATTRIBUTE: map_strings.reset,
		    NO_RESULTS: map_strings.nothing_found
		},
		params: {
		    'boundary.country': map_strings.country_code,
		    lang: map_strings.language_code,
		    size: 5,
		    sources: ["osm", "oa", "gn", "wof"]
		}
	    }
	    geocoder = L.control.geocoder(settings.PELIAS_API_KEY, geocoderOptions);
	    // map.attributionControl.addAttribution(
	    //     'Geocoding: <a target="_blank" rel="noopener noreferrer"'+
	    // 	'href="https://geocode.earth">geocode.earth</a>, '+
	    // 	'<a target="_blank" rel="noopener noreferrer" '+
	    // 	'href="https://openstreetmap.org/copyright">OpenStreetMap</a>,'+
	    // 	'and <a target="_blank" rel="noopener noreferrer" '+
            //         'href="https://geocode.earth/guidelines">others</a>.');

	    geocoder.on('select', function(e) {
		latlng = e.latlng;
		address = e.feature.properties;
		update_map_and_fields(e, latlng, address);
	    });
	} else {
	    // search bar using nominatim
	    geocoderOptions = {
		collapsed: false,
		position: 'bottomleft',
		placeholder: map_strings.search,
		errorMessage: map_strings.nothing_found,
		geocoder: new L.Control.Geocoder.Nominatim({
		    geocodingQueryParams: {
			countrycodes: map_strings.country_code,
			'accept-language': map_strings.language_code,
			addressdetails: 1
		    }}),
		defaultMarkGeocode: false
	    }

	    geocoder = L.Control.geocoder(geocoderOptions);
	    map.attributionControl.addAttribution('Geocoding: <a target="_blank" rel="noopener noreferrer" href="https://nominatim.openstreetmap.org/">nominatim</a>');

	    geocoder.on('markgeocode', function(e) {
		latlng = e.geocode.center;
		address = e.geocode.properties.address;
		address.label = e.geocode.name;

		update_map_and_fields(e, latlng, address);
	    });
	}

	// add the geocoder to the map
	geocoder.addTo(map);


	// stuff when interacting with the leaflet map
	// when marker is set by user try to find the address
	map.on('draw:created draw:edited', function (e) {
	    if (e.type === "draw:edited") {
		layer = e.layers.getLayers()[0]
	    } else {
		layer = e.layer;
	    }
	    latlng = layer.getLatLng();
	    if ( typeof(region_force) === 'object' ) {
		latlng.region = region_force.name
	    }
	    map.setView(latlng, 17);
	    $.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		url: url,
		data: JSON.stringify(latlng),
		success: function(result) {
		    layer.bindPopup('<ul class="fa-ul"><li><i class="fa-li fa fa-fw fa-map-marker" aria-hidden="true"></i>'+result['label']+'</li></ul>').addTo(map).openPopup();
		    $.each(result, function(key, value) {
			$("input[id='id_loc-"+key+"']").val(value);
		    });
		},
		error: function(data) {
                    geometryField.onDeleted(e)
		    L.popup().setLatLng(latlng).setContent(
			'<ul class="fa-ul"><li><i class="fa-li fa fa-fw fa-times-circle-o" aria-hidden="true"></i>'+
			    data.responseJSON.status+'</li></ul>'
		    ).openOn(map);
		    // clear fields
		    $("input[id^='id_loc-']").each(function() {
			$(this).val('');
		    });
		}
	    });
	});

	// get the editable layer
	map.on('map:loadfield', function(e) {
	    geometryField = e.field;
	});
    });

    $("#id_location").change(function(){
	if ($(this).val() === "0") {
	    $("#location_form").collapse('show');
	    map.invalidateSize();
	} else {
	    $("#location_form").collapse("hide");
	}
    });

    // set event on bootstrap tab change to call the change function
    $('button[data-bs-toggle="tab"]').on('shown.bs.tab', function(e) {
	var target = $(e.target).attr("data-bs-target");
	if (target == '#location') {
	    $("#id_location").change();
	}
    });
});
