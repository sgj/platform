# Django stuff
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

# Third party
from leaflet.admin import LeafletGeoAdmin

# Local imports
from .models import Location

# Register your models here.


@admin.register(Location)
class LocationAdmin(LeafletGeoAdmin):
    list_display = ("name", "address_formatted", "coords_formatted")
    list_display_links = ("name", "address_formatted", "coords_formatted")
    list_filter = ("city", "county", "creator")
    search_fields = ("name",)
    readonly_fields = (
        "county",
        "state",
        "country",
    )
    fieldsets = (
        (None, {"fields": ("name", "creator")}),
        (
            _("Adresse"),
            {
                "fields": (
                    ("street", "house_number"),
                    ("zipcode", "city"),
                    ("county", "state", "country"),
                ),
            },
        ),
        (_("Zusatz"), {"fields": ("note",)}),
        (_("Karte"), {"fields": ("coords",)}),
    )
