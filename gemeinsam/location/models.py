# -*- coding: utf-8 -*-


# Django stuff
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _trans
from django.utils.translation import gettext_lazy as _


class Location(models.Model):
    name = models.CharField(_("Name"), max_length=100, blank=True)
    city = models.CharField(_("Ort"), max_length=50, blank=True)
    zipcode = models.CharField(_("PLZ"), max_length=50, blank=True)
    street = models.CharField(_("Straße"), max_length=50, blank=True)
    house_number = models.CharField(_("Hausnr."), max_length=50, blank=True)
    county = models.CharField(_("Bezirk"), max_length=50, blank=True)
    state = models.CharField(_("Bundesland"), max_length=50, blank=True)
    country = models.CharField(_("Land"), max_length=50, blank=True)
    note = models.CharField(_("Bemerkung"), max_length=128, blank=True)
    accessible = models.BooleanField(_("Barrierefreier Zugang"), default=False)
    coords = models.PointField(_("Geographische Koordinaten"), srid=4326, dim=2)
    creator = models.ForeignKey(
        User,
        related_name="creator+",
        on_delete=models.CASCADE,
        verbose_name=_("Ersteller*in"),
    )

    class Meta:
        verbose_name = _("Ort")
        verbose_name_plural = _("Orte")
        ordering = ("name", "city", "street")

    def __str__(self):
        address_str = self.address_formatted(for_choice=True)
        if self.name:
            return f"{self.name} ({address_str})"
        else:
            return address_str

    def coords_formatted(self):
        return "{:.4f}, {:.4f}".format(*self.coords.tuple)

    coords_formatted.short_description = _("Koordinaten")

    def address_formatted(self, for_choice=False):
        street_part = " ".join([_f for _f in [self.street, self.house_number] if _f])
        city_part = " ".join([_f for _f in [self.zipcode, self.city] if _f])
        accessible = ""
        if self.accessible:
            accessible = (
                for_choice
                and _trans("barrierefrei")
                or "<i class='fa fa-wheelchair' aria-hidden='true'></i>"
            )
        return mark_safe(
            ", ".join(
                [_f for _f in [self.name, street_part, city_part, accessible] if _f]
            )
        )

    address_formatted.short_description = _("Adresse")


def locations_as_choices(user=None, id=None, restriction=False):
    # Django stuff
    from django.db.models import Q

    query = Q()
    if user:
        query |= Q(creator=user)
    if id:
        query |= Q(pk=id)

    if restriction and hasattr(settings, "LOCATION_REGION_FORCE"):
        field, region = [settings.LOCATION_REGION_FORCE[x] for x in ["field", "name"]]
        query &= Q(**{field: region})

    locations = {}
    for location in Location.objects.filter(query):
        locations.setdefault(location.city, []).append(
            [location.pk, location.__str__()]
        )

    locations_list = [[k, v] for k, v in list(locations.items())]
    return sorted(locations_list, key=lambda x: x[0])


def location_states_counties_dict():
    states = []

    tmp = {}
    for result in (
        Location.objects.all()
        .order_by("state", "county")
        .distinct("state", "county")
        .values("state", "county")
    ):
        tmp.setdefault(result["state"], []).append(result["county"])

    for state in sorted(tmp.keys()):
        states.append({"name": state, "counties": [{"name": x} for x in tmp[state]]})

    return states
