# -*- coding: utf-8 -*-

# Django stuff
from django.contrib.auth.models import User
from django.test import TestCase

# Local imports
from .models import Location, locations_as_choices
from .utils import geocode_reverse


# Create your tests here.
class LocationTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user("nobody")

        # create a correct address
        place_in_graz = (47.0715688, 15.4210459)
        fields = geocode_reverse(place_in_graz)
        fields["coords"] = "POINT({} {})".format(*place_in_graz[::-1])
        fields["creator"] = user
        fields["name"] = "anne64"
        del fields["label"]
        Location.objects.create(**fields)

        # and one where coords and city do not match
        Location.objects.create(
            name="Corrupt address",
            creator=user,
            city="Leoben",
            coords="POINT(15.4210459 47.0715688)",
        )

    def test_geolocator(self):
        """
        Check that country, state and county is retrieved correctly from
        the geocoder
        """
        anne64 = Location.objects.get(name="anne64")
        self.assertEqual(anne64.country, "Österreich")
        self.assertEqual(anne64.state, "Steiermark")
        self.assertEqual(anne64.city, "Graz")

    def test_freesave(self):
        """
        Test that address and coords are independent in the model
        """
        loc = Location.objects.get(name="Corrupt address")
        self.assertEqual(loc.city, "Leoben")

    def test_location_choices(self):
        """
        Check number and cities of locations
        """
        loc_choices = locations_as_choices()
        self.assertEqual(len(loc_choices), 2)
        cities_string = " ".join([loc[0] for loc in loc_choices])
        self.assertEqual(cities_string, "Graz Leoben")
