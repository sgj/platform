# -*- coding: utf-8 -*-
"""
management command to merge duplicate locations that have been entered
accidently by users
"""

# Django stuff
from django.core.management.base import BaseCommand
from django.db.models import Count, ProtectedError

# First party
from event.models import Event
from initiative.models import Initiative
from location.models import Location


class Command(BaseCommand):
    help = "Merge same locations"

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="test",
            default=False,
            help="do not actually merge but show what would be done",
        )
        parser.add_argument(
            "--radius",
            type=float,
            default=5,
            help="set the radius in meters within locations" " are considered as same",
        )

    def handle(self, *args, **options):
        # find all duplicates
        fields = [
            field.name
            for field in Location._meta.fields
            if field.name not in ["id", "coords"]
        ]

        duplicates = (
            Location.objects.values(*fields)
            .annotate(Count("id"))
            .order_by()
            .filter(id__count__gt=1)
        )

        for duplicate in duplicates:
            _ = duplicate.pop("id__count")
            same_loc = Location.objects.filter(**duplicate)

            fl = same_loc[0]

            all_ids = [loc.id for loc in same_loc]
            to_id = all_ids[0]
            from_ids = []
            # check for distance smaller than 10m
            # (if its to large something else is wrong)
            for id in all_ids[1:]:
                if (
                    Location.objects.get(pk=1).coords.distance(fl.coords)
                    < options["radius"]
                ):
                    from_ids.append(id)

            if not from_ids:
                # nothing left to merge
                continue

            self.stdout.write("Merging {} locations to:\n".format(len(from_ids) + 1))

            for field in fields + ["coords"]:
                val = getattr(fl, field)
                if val:
                    self.stdout.write("  {:14s}{}\n".format(field, val))

            if not options["test"]:
                # update events and initiatiaves
                for model in [Event, Initiative]:
                    for object in model.objects.filter(location__id__in=from_ids):
                        object.location_id = to_id
                        object.save()

                try:
                    # deletion of unused locations
                    Location.objects.filter(id__in=from_ids).delete()
                except ProtectedError as e:
                    self.stderr.write(e.args[0])
