# -*- coding: utf-8 -*-
"""Management command to correct (unify) the counties."""

# Django stuff
from django.core.management.base import BaseCommand

# First party
from location.models import Location
from location.utils import geocode_reverse


class Command(BaseCommand):
    help = "Correct / unify counties"

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="test",
            default=False,
            help="do not actually correct",
        )

    def handle(self, *args, **options):
        # get all distinct counties
        all_counties = [
            c
            for c in Location.objects.order_by("county")
            .values_list("county", flat=True)
            .distinct("county")
        ]

        for county in all_counties:
            locations = Location.objects.filter(county=county)
            new_county = geocode_reverse(
                (locations[0].coords.y, locations[0].coords.x)
            )["county"]
            if new_county != county:
                self.stdout.write(f"{county} ==> {new_county}\n")
                if not options["test"]:
                    locations.update(county=new_county)
