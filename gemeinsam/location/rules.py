# -*- coding: utf-8 -*-


# Third party
from rules import add_perm, is_group_member, predicate


@predicate
def is_location_creator(user, location):
    return user == location.creator


is_moderator = is_group_member("Moderation")

add_perm("location", is_moderator)
add_perm("location.add_location", is_moderator | is_location_creator)
add_perm("location.change_location", is_moderator | is_location_creator)
add_perm("location.delete_location", is_moderator | is_location_creator)
