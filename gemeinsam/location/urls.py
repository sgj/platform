# Django stuff
from django.urls import path

# Local imports
from . import views

app_name = "location"
urlpatterns = [
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("get_address/", views.get_address, name="get_address"),
    path("add/", views.edit, name="add"),
    path("edit/<int:id>/", views.edit, name="edit"),
]
