# -*- coding: utf-8 -*-


# Standard Library
import json

# Django stuff
# 'globals'
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views import generic

from geopy.exc import GeocoderServiceError

# Local imports
from .apps import GEOCODE2MODELFIELD, MAP_STRINGS
from .forms import LocationForm
from .models import Location
from .utils import geocode_reverse

# Create your views here.


class DetailView(generic.DetailView):
    context_object_name = "location"
    template_name = "location/detail.html"
    model = Location


def get_address(request):
    """
    Returns a json-response with postal address data for
    geographical coordingates. It uses the geopy project
    as geocoder interface.
    """
    if (
        request.headers.get("x-requested-with") == "XMLHttpRequest"
        and request.method == "POST"
    ):
        data = json.loads(request.body)
        status_code = 200
        try:
            address = geocode_reverse((data["lat"], data["lng"]))
        except GeocoderServiceError:
            status_code = 404
            address = {
                "status": _(
                    "Geocoder im Moment nicht erreichbar. Bitte versuche es später nocheinmal"
                )
            }

        if "region" in data and hasattr(settings, "LOCATION_REGION_FORCE"):
            field, region = [
                settings.LOCATION_REGION_FORCE[x] for x in ["field", "name"]
            ]

            if field not in address or address[field] != region:
                status_code = 404
                address["status"] = _("Ort liegt nicht in: {}".format(region))

        address["code"] = status_code
        return JsonResponse(data=address)


@login_required
def edit(request, id=None):
    if id:
        location = get_object_or_404(Location, pk=id)

        # check permission
        if not request.user.has_perm("location.change_location", location):
            raise PermissionDenied

    else:
        location = Location(creator=request.user)

    if request.method == "POST":
        # check if user wants to delete this location
        if request.POST.get("loc-deletion"):
            location.delete()
            return HttpResponseRedirect("/")

        form = LocationForm(request.POST, prefix="loc", instance=location)

        if form.is_valid():
            location = form.save()
            return HttpResponseRedirect(reverse("location:detail", args=[location.pk]))

    else:
        form = LocationForm(instance=location, prefix="loc")

    return render(
        request,
        "location/edit.html",
        {
            "form": form,
            "map_strings": MAP_STRINGS,
            "geocode2modelfield": GEOCODE2MODELFIELD,
            "settings": {
                "MODERATION_EMAIL": settings.MODERATION_EMAIL,
                "PELIAS_API_KEY": settings.PELIAS_API_KEY,
                "PELIAS_URL": settings.PELIAS_URL,
            },
        },
    )
