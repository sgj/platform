# -*- coding: utf-8 -*-
# Django stuff
from django import forms
from django.utils.translation import gettext_lazy as _

# Third party
from leaflet.forms.widgets import LeafletWidget

# Local imports
from .models import Location


class LocationForm(forms.ModelForm):
    deletion = forms.BooleanField(required=False, label=_("Unwiderruflich löschen"))

    class Meta:
        model = Location
        exclude = ["creator"]
        widgets = {
            "coords": LeafletWidget(),
        }

    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            if field not in ["deletion", "accessible"]:
                self.fields[field].widget.attrs.update(
                    {"class": "form-control", "placeholder": self.fields[field].label}
                )
                if field in [
                    "street",
                    "house_number",
                    "city",
                    "zipcode",
                    "country",
                    "county",
                    "state",
                ]:
                    self.fields[field].widget.attrs["readonly"] = True
