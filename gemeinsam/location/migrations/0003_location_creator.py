# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-20 23:52


from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('location', '0002_auto_20170120_2140'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='creator',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
