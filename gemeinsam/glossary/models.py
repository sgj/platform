# -*- coding: utf-8 -*-
# Django stuff
from django.db import models
from django.utils.translation import gettext_lazy as _

# Third party
from django_bleach.models import BleachField


# Create your models here.
class Entry(models.Model):
    term = models.CharField(
        _("Begriff"), max_length=50, help_text=_("Der zu erklärende Begriff")
    )
    definition = BleachField(verbose_name=_("Erklärung"), help_text=_("Die Erklärung"))
    published = models.BooleanField(_("Veröffentlicht"), default=False)

    def __str__(self):
        return self.term

    def get_absolute_url(self):
        return "/glossary/{}".format(self.pk)

    def definition_cut(self):
        if len(self.definition) >= 100:
            return self.definition[:100] + "..."
        else:
            return self.definition

    definition_cut.short_description = _("Begriffserklärung")

    class Meta:
        verbose_name = _("Glossareintrag")
        verbose_name_plural = _("Glossareinträge")

        ordering = ["term"]


class Weblink(models.Model):
    description = models.CharField(
        _("Beschreibung des links"),
        max_length=50,
        blank=True,
        help_text=_("kurze Beschreibung zu der web-Ressource"),
    )
    link = models.URLField(_("Web-Referenz"), max_length=500, help_text=_("Web URL"))
    entry = models.ForeignKey(Entry)

    def __str__(self):
        return self.link

    class Meta:
        verbose_name = _("Webressource")
        verbose_name_plural = _("Webressourcen")
