# Django stuff
from django.test import TestCase

# Local imports
from .models import Entry


# Create your tests here.
class EntryMethodTests(TestCase):
    def test_100_definition_cut(self):
        """
        definition cut shoul return a string of length 103 if the
        original definition has more or equal 100 characters
        """
        entry = Entry(term="Test entry", definition="a" * 100)
        self.assertEqual(entry.definition_cut(), "a" * 100 + "...")

    def test_less_than_100_definition_cut(self):
        """
        should return entry.definition
        """
        entry = Entry(term="Test entry", definition="abc")
        self.assertEqual(entry.definition_cut(), "abc")
