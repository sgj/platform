# Django stuff
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

# Third party
from watson import search as watson


class GlossaryAppConfig(AppConfig):
    name = "glossary"
    verbose_name = _("Glossar")

    def ready(self):
        EntryModel = self.get_model("Entry")
        watson.register(EntryModel.objects.filter(published=True))
