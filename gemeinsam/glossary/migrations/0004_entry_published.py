# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0003_auto_20151210_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='published',
            field=models.BooleanField(default=False, verbose_name='Ver\xf6ffentlicht'),
        ),
    ]
