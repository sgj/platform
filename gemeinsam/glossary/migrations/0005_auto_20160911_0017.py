# -*- coding: utf-8 -*-


from django.db import migrations, models
import django_bleach.models


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0004_entry_published'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='definition',
            field=django_bleach.models.BleachField(help_text='Die Erkl\xe4rung', verbose_name='Erkl\xe4rung'),
        ),
    ]
