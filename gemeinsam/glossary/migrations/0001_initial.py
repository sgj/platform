# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('term', models.CharField(help_text='Der zu erkl\xe4rende Begriff', max_length=50, verbose_name='Begriff')),
                ('definition', models.TextField(help_text='Die Erkl\xe4rung', verbose_name='Erkl\xe4rung')),
            ],
            options={
                'ordering': ['term'],
                'verbose_name': 'Glossareintrag',
                'verbose_name_plural': 'Glossareintr\xe4ge',
            },
        ),
    ]
