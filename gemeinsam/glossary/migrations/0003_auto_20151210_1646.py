# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0002_entry_weblink'),
    ]

    operations = [
        migrations.CreateModel(
            name='Weblink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(help_text='kurze Beschreibung zu der web-Ressource', max_length=50, verbose_name='Beschreibung des links', blank=True)),
                ('link', models.URLField(help_text='Web URL', max_length=500, verbose_name='Web-Referenz')),
            ],
            options={
                'verbose_name': 'Webressource',
                'verbose_name_plural': 'Webressourcen',
            },
        ),
        migrations.RemoveField(
            model_name='entry',
            name='weblink',
        ),
        migrations.AddField(
            model_name='weblink',
            name='entry',
            field=models.ForeignKey(to='glossary.Entry'),
        ),
    ]
