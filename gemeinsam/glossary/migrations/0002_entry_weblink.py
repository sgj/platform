# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='weblink',
            field=models.URLField(help_text='Link zur Definition im web', max_length=500, null=True, verbose_name='Web-Referenz', blank=True),
        ),
    ]
