# Django stuff
from django.contrib import admin

# Local imports
from .models import Entry, Weblink


class WeblinkInline(admin.TabularInline):
    model = Weblink


class EntryAdmin(admin.ModelAdmin):
    fields = ["term", "definition", "published"]
    list_display = ("term", "definition_cut", "published")
    search_fields = ["term", "definition"]
    list_filter = [
        "published",
    ]
    inlines = [WeblinkInline]


admin.site.register(Entry, EntryAdmin)
