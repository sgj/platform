# Django stuff
from django.views import generic

# Local imports
from .models import Entry


class IndexView(generic.ListView):
    template_name = "glossary/index.html"
    context_object_name = "entry_list"

    def get_queryset(self):
        """Return all glossary entries."""
        return Entry.objects.filter(published=True)


class DetailView(generic.DetailView):
    model = Entry
    template_name = "glossary/detail.html"
