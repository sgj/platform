# Django stuff
from django.urls import path

# Local imports
from . import views

app_name = "faq"
urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("new/", views.new, name="new"),
]
