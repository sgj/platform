$(document).ready(function() {
    $("#submit").attr('disabled', true);
    $('#new-question').keyup(function(){
        if($(this).val().length !=0)
            $('#submit').attr('disabled', false).removeClass("btn-disabled").addClass("btn-success");
        else
            $('#submit').attr('disabled', true).addClass("btn-disabled").removeClass("btn-success");
    })
});
