# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name')),
                ('order', models.SmallIntegerField(default=0, verbose_name='Sortierung')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': 'FAQ Kategorie',
                'verbose_name_plural': 'FAQ Kategorien',
            },
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=100, verbose_name='Frage')),
                ('answer', models.TextField(verbose_name='Antwort', blank=True)),
                ('published', models.BooleanField(default=False, verbose_name='Ver\xf6ffentlicht')),
                ('order', models.SmallIntegerField(default=0, verbose_name='Sortierung')),
                ('category', models.ForeignKey(to='faq.Category', on_delete=models.CASCADE, null=True)),
            ],
            options={
                'ordering': ['order', 'question'],
                'verbose_name': 'FAQ Eintrag',
                'verbose_name_plural': 'FAQ Eintr\xe4ge',
            },
        ),
    ]
