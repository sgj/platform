# Django stuff
from django.contrib import admin

# Local imports
from .models import Category, Entry

# Register your models here.


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ["name", "order"]


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ["question", "category", "published", "answered"]
    search_fields = ["question", "answer"]
    list_filter = ["category", "published"]

    fields = ["question", "answer", "category", "order", "published"]
