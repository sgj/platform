# -*- coding: utf-8 -*-
# Django stuff
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
class Category(models.Model):
    name = models.CharField(_("Name"), max_length=50)
    order = models.SmallIntegerField(_("Sortierung"), default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("FAQ Kategorie")
        verbose_name_plural = _("FAQ Kategorien")

        ordering = ["order", "name"]


class Entry(models.Model):
    question = models.CharField(_("Frage"), max_length=100)
    answer = models.TextField(_("Antwort"), blank=True)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, null=True, blank=True
    )
    published = models.BooleanField(_("Veröffentlicht"), default=False)
    order = models.SmallIntegerField(_("Sortierung"), default=0)

    def answered(self):
        return self.answer != ""

    answered.short_description = _("Beantwortet?")

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = _("FAQ Eintrag")
        verbose_name_plural = _("FAQ Einträge")

        ordering = ["category", "order", "question"]
