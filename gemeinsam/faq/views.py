# Django stuff
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

# Local imports
from .models import Entry


# Create your views here.
class IndexView(generic.ListView):
    template_name = "faq/index.html"
    context_object_name = "entry_list"

    def get_queryset(self):
        return Entry.objects.filter(
            answer__isnull=False, published=True, category__isnull=False
        )


def new(request):
    if request.method == "POST":
        question = request.POST.get("new")
        if question:
            entry = Entry(question=question)
            entry.save()
            return render(request, "faq/question_entered.html")
        else:
            return render(request, "faq/question_invalid.html")

    else:
        return HttpResponseRedirect("/faq")
