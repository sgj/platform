# Django stuff
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class FaqAppConfig(AppConfig):
    name = "faq"
    verbose_name = _("FAQ")
