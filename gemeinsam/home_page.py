# Standard Library
import datetime as dt

from random import randint, sample

# Django stuff
from django.conf import settings
from django.db.models import Count
from django.shortcuts import render
from django.utils.timezone import now

# First party
from event.models import Event
from initiative.models import Initiative, InitiativeOnFront


def _get_ini_list(count=3):
    today = now().date()
    inis_on_front = InitiativeOnFront.objects.filter(last_date=today)
    if inis_on_front.count() < count:
        # update the InitiativeOnFront table
        InitiativeOnFront.objects.bulk_create(
            [
                InitiativeOnFront(initiative=ini)
                for ini in Initiative.objects.filter(
                    published=True, initiativeonfront__isnull=True
                )
            ]
        )
        # remove all not published initiatives
        InitiativeOnFront.objects.filter(initiative__published=False).delete()

        # select count initiatives and update initiativeonfront
        # this is done in a random way favoring initiatives
        # with low count and low last_date in the InitiativeOnFront model
        aggregates = (
            InitiativeOnFront.objects.values("count", "last_date")
            .order_by("count", "last_date")
            .annotate(number=Count("count"))
        )
        count_list, date_list = [], []
        number = 0
        for agg in aggregates:
            number += agg["number"]
            count_list.append(agg["count"])
            date_list.append(agg["last_date"])
            if number >= count * 2:
                break

        indexes = [randint(0, number - 1) for _ in range(count)]

        inis_on_front = [
            InitiativeOnFront.objects.filter(
                count__in=count_list, last_date__in=date_list
            )[i]
            for i in indexes
        ]

        # update the selected initiativves
        for ini_on_front in inis_on_front:
            ini_on_front.last_date = today
            ini_on_front.count += 1
            ini_on_front.save()

    return Initiative.objects.filter(
        id__in=sample(list(inis_on_front.values_list("initiative", flat=True)), count)
    )


def view(request):
    # ini_days = getattr(settings, "DAYS_ON_START_INITIATIVE", 14)
    event_days = getattr(settings, "DAYS_ON_START_EVENT", 7)
    min_events = getattr(settings, "MINIMUM_EVENTS", 5)
    max_events = getattr(settings, "MAXIMUM_EVENTS", 10)

    try:
        ini_list = _get_ini_list()
    except Exception:
        ini_list = None

    for days_ahead in range(event_days):
        ev_list = Event.objects.filter(
            published=True,
            occurrence__start_time__range=(
                now(),
                now() + dt.timedelta(days=days_ahead),
            ),
        ).order_by("occurrence__start_time")

        # remove duplicates
        seen = set()
        seen_add = seen.add
        ev_list = [x for x in ev_list if not (x in seen or seen_add(x))]

        if len(ev_list) > min_events:
            break

    context = {"initiative_list": ini_list, "event_list": ev_list[:max_events]}
    return render(request, "home.html", context)
